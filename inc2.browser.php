<? 
/*  
 * File Browser 
 * 
 * Lists files in script directory, creating 
 * a browsable file tree. 
 * 
 * many options are available for easy tweaking in the $SETTINGS array 
 * 
 * @copyright :  2005 
 * @author :  Gabriel Dunne 
 * @email : gdunne@quilime.com 
 * @url :  http://www.quilime.com 
 * 
 * 
 * @param string $root : Directory to list 
 *                       Examples: 
 *                       '/'  : drive root 
 *                       './' : script root 
 *                       './Users/Me/Pictures/gallery1/' : a folder 
 */ 
class FileBrowser { 
   
  //  settings 
  var $SETTING = array(); 
     
  // if there's a custom index, this will get set 
  var $customIndex, $root, $path, $files; 

  var $totalFiles, $totalFolders; 

  function FileBrowser($root = '/var/www/vhosts/agricortes.pt/docs/downs/') { 
     
    $this->totalFiles = 0; 
    $this->totalFolders = 0; 

    // allow the users to browse folders 
    $this->SETTINGS['browse'] = true; 

    // the current index file  
    $this->SETTINGS['filename'] = 'index.php'; 
     
    // track downloads 
    $this->SETTINGS['trackdls'] = true; 
     
    // enable uploading 
    $this->SETTINGS['upload'] = false; 
    $this->SETTINGS['uploaddir'] = 'uploads/';     

    // download config file 
    $this->SETTINGS['dlconfig'] = ''; 
     
    // title in the HTML header 
    $this->SETTINGS['title'] = 'bin'; 
     
    // html DOM id 
    $this->SETTINGS['id'] = 'fileBrowser'; 

    // show footer  
    $this->SETTINGS['footer'] = true; 
     
    // show header 
    $this->SETTINGS['header'] = true; 
     
    // show sorting header 
    $this->SETTINGS['sort'] = true; 
     
    // show/hide columns 
    $this->SETTINGS['lineNumbers']     = true; 
    $this->SETTINGS['showFileSize']    = true; 
    $this->SETTINGS['showFileModDate'] = true; 
    $this->SETTINGS['showFileType']    = true; 
     
    // calculate folder sizes (increases processing time) 
    $this->SETTINGS['calcFolderSizes'] = false; 
     
    // display MIME type, or "simple" file type  
    // (MIME type increases processing time) 
    $this->SETTINGS['simpleType'] = true; 
     
    // open linked files in new windows     
    $this->SETTINGS['linkNewWin'] = false; 
     
        // if a folder contains a file listed in this array,  
        //it'll automatically use that file instead. 
    $this->SETTINGS['folderIndexes'] = array( 
                         //'index.html', 
                         //'index.php' 
                                                                                            ); 
    // sort folders on top of files 
    $this->SETTINGS['separateFolders'] = true; 
     
    // natural sort files, as opposed to regular sort (files with capital 
    // letters get sorted first) 
    $this->SETTINGS['naturalSort'] = true; 
     
    // show hidden files (files with a dot as the first char) 
    $this->SETTINGS['showHiddenFiles'] = false; 
     
    // date format. see the url  
    // http://us3.php.net/manual/en/function.date.php 
    // for more information 
    $this->SETTINGS['dateFormat'] = 'F d, Y g:i A'; 
         
         
        $this->root = $root; 
         
    // get path if browsing a tree 
    $this->path = ( $this->SETTINGS['browse'] && isset($_GET['p']) ) ? $_GET['p']: ""; 

    // get sorting vars from URL, if nothing is set, sort by N [file Name] 
    // I'm doing this apache 1.2 style. Add whatever sort mode you need if you add more columnes, 
    $this->SETTINGS['sortMode'] = (isset($_GET['N']) ? 'N' :            // sort by name  
                  (isset($_GET['S']) ? 'S' :            // sort by size         
                  (isset($_GET['T']) ? 'T' :            // sort by file type 
                  (isset($_GET['M']) ? 'M' : 'N' ))));  // sort by modify time 
     
    // get sortascending or descending             
    $this->SETTINGS['sortOrder'] =  
      isset($_GET[$this->SETTINGS['sortMode']]) ?  
      $_GET[$this->SETTINGS['sortMode']] : 'A';  

     
    // create array of files in tree 
    $this->files = $this->makeFileArray($this->root, $this->path); 

         
        // use index file if contains 
        if(isset($this->customIndex)){ 
            $newindex = 'http://'.$_SERVER['HTTP_HOST'].'/'.substr($this->customIndex, '2'); 
            header("Location: $newindex"); 
        }  
          
              
    // get size of arrays before sort 
    //$this->totalFolders = sizeof($files['folders']); 
    //$this->totalFiles   = sizeof($files['files']); 
     
    // sort files 
    $this->files = $this->sortFiles($this->files);         

  } 
   
  /* 
   * list directory 
   * 
   * list the directory in html 
   * 
   * @param $root : the root of the folder to navigate 
   * 
   */ 
  function listDir() { 

    // container div 
    echo '<div id="'.$this->SETTINGS['id'].'">'; 
     
    // header 
    echo $this->SETTINGS['header'] ? $this->headerInfo($this->root, $this->path, $totalFolders, $totalFiles) : '';         
     
    // upload form     
    echo $this->SETTINGS['upload'] ?  
      $this->uploadForm($this->SETTINGS['uploaddir']) : '';     
     
    // the file list table 
    $this->fileList($this->$root, $this->$path, $this->$files);  
     
    // end of container div 
    echo '</div>';     
  } 
     
     
  /*  
   * upload form  
   * @param path : the path to the form  
   */ 
  function uploadForm($path) { 
    $server = 'http://media.quilime.com/upload/?config='.$this->SETTINGS['dlconfig']; 
    $uload  = '<div id="uploadform">'; 
    $uload .= '<form><input type="button" value="Upload Files" onClick="window.location=\''.$server.'\'"> </form>'; 
    $uload .= '</div>'; 
    return $uload; 
  }     
   
  /* Create array out of files 
   * 
   * @param   string $root  : path root  
   * @param   string $path  : folder to list 
   * @return     string array  : Array of files and folders inside the current  
   */ 
  function makeFileArray($root, $path)   { 

    if (!function_exists('mime_content_type'))       { 
      /* MIME Content Type 
       * 
       * @param   string $file : the extension 
       * @return     string      : the files mime type 
       * @note                 : alternate function written for UNIX  
       *                         environments when PHP function may  
       *                         not be available. 
       */ 
      function mime_content_type($file)         {       
    $file = escapeshellarg($file); 
    $type = `file -bi $file`; 
    $expl = explode(";", $type);     
    return $expl[0]; 
      } 
    } 
         
     
    // if the path is legit 
    if ($this->verifyPath($path)) { 
       
      $path        = $root.$path; 
      $fileArray   = array(); 
      $folderArray = array(); 
       
      if ($handle = opendir($path)) { 
             
     
     
    while (false !== ($file = readdir($handle))) { 
      if ($file != '.' && $file != '..') { 
         
        // show/hide hidden files 
        if (!$this->SETTINGS['showHiddenFiles'] && substr($file,0,1) == '.') { 
          continue;  
        } 
         
        // is a folder 
        if(is_dir($path.$file)) {  
           
          // store elements of the folder 
          $folderInfo  = array(); 
           
          $folderInfo['name']    = $file; 
          $folderInfo['mtime']   = filemtime($path.$file); 
          $folderInfo['type']    = 'Folder'; 
          $folderInfo['size']    = $this->SETTINGS['calcFolderSizes'] ? $this->folderSize($path.$file) : '-';  
          $folderInfo['rowType'] = 'fr'; 
          $this->totalFolders++; 
          $folderArray[] = $folderInfo; 
        }  
        // is a file 
        else {  
           
          // if it's the index, skip 
          if($path.$file == $root.$this->SETTINGS['filename'] || $path.$file == $root.$this->SETTINGS['filename'].'~'){ 
        continue; 
          } 
           
          // if this file is in the index array, flag it 
          if(in_array($file, $this->SETTINGS['folderIndexes'])){ 
        $this->customIndex = $path.$file; 
          } 
           
          // store elements of the file 
          $fileInfo    = array(); 
           
          $fileInfo['name']    = $file; 
          $fileInfo['mtime']   = filemtime($path.$file); 
          $fileInfo['type']    = $this->SETTINGS['simpleType'] ? $this->getExtension($path.$file) : mime_content_type($path.$file); 
          $fileInfo['size']    = filesize($path.$file); 
          $fileInfo['rowType'] = 'fl'; 
          $fileArray[] = $fileInfo; 
          $this->totalFiles++; 
        } 
      } 
    } 
    closedir($handle);     
      }     
       
      // everything gets returned 
      $dirArray = array('folders'=> $folderArray, 'files'=> $fileArray);     
      return $dirArray; 
    }  
    else { 
      echo 'Not a valid directory!'; 
      return false; 
      exit;         
    } 
  }     
   
  /* check path for sneakiness , such as (../)'s 
   * 
   * @param   string $path : The path to parse 
   * @return     bool         
   */ 
  function verifyPath($path) { 
    if (preg_match("/\.\.\//", $path)) return false; 
    else return true; 
  } 
   
  /* Get the file extension from a filename 
   * 
   * @param     string $filename : filename from which to get extension 
   * @return     string : the extension 
   */ 
  function getExtension($filename) { 
    $justfile = explode("/", $filename); 
    $justfile = $justfile[(sizeof($justfile)-1)]; 
    $expl = explode(".", $justfile); 
    if(sizeof($expl)>1 && $expl[sizeof($expl)-1]) { 
      return $expl[sizeof($expl)-1];    
    } 
    else { 
      return '?'; 
    } 
  } 
   
  /* Get the parent directory of a path 
   * 
   * @param   string $path : the working dir 
   * @return     string : the parent directory of the path 
   */ 
  function parentDir($path) { 
    $expl = explode("/", substr($path, 0, -1)); 
    return  substr($path, 0, -strlen($expl[(sizeof($expl)-1)].'/'));    
  } 
   
  /* Format Byte to Human-Readable Format 
   * 
   * @param   int $bytes : the byte size 
   * @return     string : the ledgable result 
   */ 
  function formatSize($bytes) { 

    if(is_integer($bytes) && $bytes > 0) { 
      $formats = array("%d bytes", "%.1f kb", "%.1f mb", "%.1f gb", "%.1f tb"); 
      $logsize = min( (int)(log($bytes) / log(1024)), count($formats)-1); 
      return sprintf($formats[$logsize], $bytes/pow(1024, $logsize)); 
    } 

    // it's a folder without calculating size 
    else if(!is_integer($bytes) && $bytes == '-') { 
      return '-'; 
    } 
    else { 
      return '0 bytes'; 
    } 
  } 
   
  /* Calculate the size of a folder recursivly 
   * 
   * @param   string $bytes : the byte size 
   * @return     string : the ledgable result 
   */     
  function folderSize($path) { 
    $size = 0; 
    if ($handle = opendir($path)) { 
      while (($file = readdir($handle)) !== false) { 
    if ($file != '.' && $file != '..') { 
      if(is_dir($path.'/'.$file)) { 
        $size += $this->folderSize($path.'/'.$file); 
      }  
      else { 
        $size += filesize($path.'/'.$file); 
      } 
    } 
      } 
    } 
    return $size; 
  } 
   
  /* Header Info for current path 
   * 
   * @param    string $root : root directory 
   * @param    string $path : working directory 
   * @param    int $totalFolders : total folders in working dir 
   * @param   int $totalFiles : total files in working dir 
   * @return     string : HTML header <div>      
   */     
  function headerInfo($root, $path, $totalFolders, $totalFiles) { 
    $slash   = '&nbsp;/&nbsp;'; 
    $header  = '<div class="header"> 
                <div class="breadcrumbs"> 
                <a href="'.$_SERVER['PHP_SELF'].'">'.$this->SETTINGS['title'].'</a>'; 
     
    // explode path into links 
    $pathParts = explode("/", $path);  
     
    // path counter 
    $pathCT = 0;  
     
    // path parts for breadcrumbs 
    foreach($pathParts as $pt) { 
      $header .= $slash;  
      $header .= '<a href="?p='; 
      for($i=0;$i<=$pathCT;$i++) { 
            $header .= $pathParts[$i].'/';  
      } 
      $header .= '">'.$pt.'</a>';  
      $pathCT++; 
    } 
         
    $header .= '</div><div>'; 
     
    $header .= $this->totalFolders.' Folders'; 
    $header .= ', '; 
    $header .= $this->totalFiles .' Files'; 
    $header .= '</div></div>'."\n"; 
     
    return $header; 
  } 
   
   
  /* Sort files 
   * 
   * @param     string array $files    : files/folders in the current tree 
   * @param    string $sortMode : the current sorted column 
   * @param    string $sortOrder : the current sort order. 'A' of 'D' 
   * @return    string : the resulting sort 
   */ 
  function sortFiles($files) {         
     
    // sort folders on top 
    if($this->SETTINGS['separateFolders']) { 
       
      $sortedFolders = $this->orderByColumn($files['folders'], '2');     
      $sortedFiles   = $this->orderByColumn($files['files'], '1'); 
       
      // sort files depending on sort order 
      if($this->SETTINGS['sortOrder'] == 'A') { 
    ksort($sortedFolders); 
    ksort($sortedFiles); 
    $result = array_merge($sortedFolders, $sortedFiles); 
      } 
      else { 
    krsort($sortedFolders); 
    krsort($sortedFiles); 
    $result = array_merge($sortedFiles, $sortedFolders); 
      } 
    } 
     
    // sort folders and files together 
    else { 
       
      $files = array_merge($files['folders'], $files['files']); 
      $result = $this->orderByColumn($files,'1'); 
       
      // sort files depending on sort order 
      $this->SETTINGS['sortOrder'] == 'A' ? ksort($result):krsort($result); 
    } 
    return $result; 
  } 
   
  /* Order By Column 
   *  
   * @param    string array $input : the array to sort 
   * @param    string $type : the type of array. 1 = files, 2 = folders 
   */ 
  function orderByColumn($input, $type) { 
     
    $column = $this->SETTINGS['sortMode']; 
     
    $result = array(); 
     
    // available sort columns 
    $columnList = array('N'=>'name',  
            'S'=>'size',  
            'T'=>'type',  
            'M'=>'mtime'); 
     
    // row count  
    // each array key gets $rowcount and $type  
    // concatinated to account for duplicate array keys 
    $rowcount = 0; 
     
    // create new array with sort mode as the key 
    foreach($input as $key=>$value) { 
      // natural sort - make array keys lowercase 
      if($this->SETTINGS['naturalSort']) { 
    $col = $value[$columnList[$column]]; 
    $res = strtolower($col).'.'.$rowcount.$type; 
    $result[$res] = $value; 
      } 
      // regular sort - uppercase values get sorted on top 
      else  { 
    $res = $value[$columnList[$column]].'.'.$rowcount.$type; 
    $result[$res] = $value; 
      } 
      $rowcount++; 
    } 
    return $result; 
  } 
   
   
  /* List Files 
   * 
   * @param   string $path         : working dir 
   * @param   string $files        : the files contined therin 
   * @param   mixed array $options : user options 
   * @return     none 
   */ 
  function fileList() { 
     
    // remove the './' from the path 
    $root = substr($this->root, '2'); 
     
    // start of HTML file table     
    echo '<table class="filelist" cellspacing="0" border="0">'; 
     
    // sorting row 
    echo $this->SETTINGS['sort'] ? $this->row('sort', null, $this->path) : ''; 
     
    // parent directory row (if inside a path) 
    echo $this->path ? $this->row('parent', null, $this->path) : ''; 
     
    // total number of files 
    $rowcount  = 1;  
     
    // total byte size of the current tree 
    $totalsize = 0;     
     
    // rows of files 
    foreach($this->files as $file) { 
      echo $this->row($file['rowType'], $this->root, $this->path, $rowcount, $file); 
      $rowcount++;  
      $totalsize += $file['size']; 
    } 
     
    $this->SETTINGS['totalSize'] = $this->formatSize($totalsize); 
     
    // footer row 
    echo $this->SETTINGS['footer'] ? $this->row('footer') : ''; 
     
    // end of table 
    echo '</table>'; 
  } 
   
    /* file / folder row 
     * 
     * @param   string $type : either 'fr' or 'fl', representing a file row 
     *                         or a folder row. 
     * @param   string $path : working path 
     * @param   int $rowcount : current count of the row for line numbering 
     * @param   string $file : the file to be rendered on the row 
     * @return     string : HTML table row 
     * 
     * notes: still need to edit code to wrap to 73 chars, hard to read at 
     * the moment. 
     * 
     * 
     * 
     */     
  function row($type, $root=null, $path=null, $rowcount=null, $file=null) { 
    // alternating row styles 
    $rnum = $rowcount ? ($rowcount%2 == 0 ? ' r2' : ' r1') : null; 
         
    // start row string variable to be returned 
    $row = "\n".'<tr class="'.$type.$rnum.'">'."\n";  
     
    switch($type) { 
     
    // file / folder row 
    case 'fl' :   
    case 'fr' :  
       
      // line number 
      $row .= $this->SETTINGS['lineNumbers'] ? '<td class="ln">'.$rowcount.'</td>' : ''; 
       
      // filename 
      $row .= '<td class="nm">'; 
      $row .= '<a '; 
      $row .= $this->SETTINGS['linkNewWin'] && $type== 'fl' ? 'target="_blank" ' : '';  
      $row .= 'href="'; 
      $row .= $this->SETTINGS['browse'] && $type == 'fr' ? '?p='.$path.$file['name'].'/' : $root.$path.$file['name']; 
      $row .= '" '; 
      $row .= '>'; 
      $row .= $file['name'].'</a></td>'; 
       
      // file size 
      $row .= $this->SETTINGS['showFileSize'] ? '<td class="sz">'.$this->formatSize($file['size']).'</td>' : ''; 
       
      // file type 
      $row .= $this->SETTINGS['showFileType'] ? '<td class="tp">'.$file['type'].'</td>' : '';  
       
      // date 
      $row .= $this->SETTINGS['showFileModDate'] ? '<td class="dt">'.date($this->SETTINGS['dateFormat'], $file['mtime']).'</td>' : ''; 
       
      break; 
       
    // sorting header     
    case 'sort' : 
             
      // sort order. Setting ascending or descending for sorting links 
      $N = ($this->SETTINGS['sortMode'] == 'N') ? ($this->SETTINGS['sortOrder'] == 'A' ? 'D' : 'A') : 'A'; 
      $S = ($this->SETTINGS['sortMode'] == 'S') ? ($this->SETTINGS['sortOrder'] == 'A' ? 'D' : 'A') : 'A'; 
      $T = ($this->SETTINGS['sortMode'] == 'T') ? ($this->SETTINGS['sortOrder'] == 'A' ? 'D' : 'A') : 'A'; 
      $M = ($this->SETTINGS['sortMode'] == 'M') ? ($this->SETTINGS['sortOrder'] == 'A' ? 'D' : 'A') : 'A'; 

      $row .= $this->SETTINGS['lineNumbers'] ? '<td class="ln">&nbsp;</td>' : '';  

      $row .= '<td><a href="?N='.$N.'&amp;p='.$path.'">Name</a></td>'; 

      $row .= $this->SETTINGS['showFileSize'] ?'<td class="sz"><a href="?S='.$S.'&amp;p='.$path.'">Size</a></td>' : ''; 
      $row .= $this->SETTINGS['showFileType'] ? '<td class="tp"><a href="?T='.$T.'&amp;p='.$path.'">Type</a></td>' : ''; 
      $row .= $this->SETTINGS['showFileModDate'] ? '<td class="dt"><a href="?M='.$M.'&amp;p='.$path.'">Last Modified</a></td>' : ''; 
      break; 
       
    // parent directory row     
    case 'parent' :  
      $row .= $this->SETTINGS['lineNumbers'] ? '<td class="ln">&uarr;</td>' : ''; 
      $row .= '<td class="nm"><a href="?p='.$this->parentDir($path).'">'; 
      $row .= 'Parent Directory'; 
      $row .= '</a></td>'; 
      $row .= $this->SETTINGS['showFileSize'] ? '<td class="sz">&nbsp;</td>' : ''; 
      $row .= $this->SETTINGS['showFileType'] ? '<td class="tp">&nbsp;</td>' : ''; 
      $row .= $this->SETTINGS['showFileModDate'] ? '<td class="dt">&nbsp;</td>' : ''; 
      break; 
       
    // footer row 
    case 'footer' :  
      $row .= $this->SETTINGS['lineNumbers'] ? '<td class="ln">&nbsp;</td>' : ''; 
      $row .= '<td class="nm">&nbsp;</td>'; 
      $row .= $this->SETTINGS['showFileSize'] ? '<td class="sz">'.$this->SETTINGS['totalSize'].'</td>' : ''; 
      $row .= $this->SETTINGS['showFileType'] ? '<td class="tp">&nbsp;</td>' : ''; 
      $row .= $this->SETTINGS['showFileModDate'] ? '<td class="dt">&nbsp;</td>' : ''; 
      break; 
    } 
     
    $row .= '</tr>'; 
    return $row; 
  }     


  /* 
   *  javascript 
   * 
   * 
   */ 
  function js(){ 
  ?> 
 <script> 
    
    // Link Tracker referenced from GLEN JONES 
    // For details: http://www.glennjones.net/ 

    window.onload = function() { 
      initLinkTracks.initList(); 
    }; 

 var LinkTracker = { 

   initLinkTracks : function(){ 
     if (!document.getElementsByTagName) return false; 
     links = document.getElementsByTagName('a') 
     for (var i = 0; i < links.length; i++) { 
       addEvent(links[i], 'mousedown', recordClick, false); 
       // If a link does not have any id it is given one 
       if (! links[i].getAttribute('id') ) 
       links[i].setAttribute('id',"link_" + i) 
     } 
   } 

   recordClick : function() { 
     alert("test"); 
   } 

 } 
  
  
 </script>  
  <? 
  } 
} 

// redirect if uploaded 
if(isset($_GET['tmp_sid'])){ 
  $dir = explode("/",$_SERVER['PHP_SELF']); 
  $server =  "http://media.quilime.com/".$dir[1]."/?p=uploads/"; 
  header("Location: ".$server); 
} 

?> 
