<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

        <?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span8','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span8','maxlength'=>100)); ?>
        
        <div class='control-group'>
        <?php echo $form->labelEx($model,'atividade_id',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
            echo $form->dropDownList($model,'atividade_id',AreaAtividade::model()->getAllName(),array('class'=>'span4','prompt'=>'Escolha a área de atividade'));
        ?>
        </div></div>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'categoria',array('class'=>'control-label')); ?>   
            <div class='controls'>
        <?php /*echo $form->dropDownList($model,'categoria',
                array('OPERATION MANUALS'=>'OPERATION MANUALS',
                      'PRICE LIST'=>'PRICE LIST',
                      'SERVICE MANUALS'=>'SERVICE MANUALS',  
                      'SPARE PARTS CATALOGUES'=>'SPARE PARTS CATALOGUES',  
                      'SPARE PARTS PRICE LIST'=>'SPARE PARTS PRICE LIST', 
                      'SPARE PARTS'=>'SPARE PARTS',
                    
                    ),
                array('class'=>'span4'));*/ ?>
        <?php echo $form->dropDownList($model,'categoria', CHtml::listData(CategoriaDownload::model()->findAll(), 'id', 'titulo_pt')); ?>        
        </div></div>        

	<?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'src',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'src');
        echo"</div></div>";
        ?>
        
        <?php
            if(!$model->isNewRecord)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>

	

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
