<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_es')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('src')); ?>:</b>
	<?php echo CHtml::encode($data->src); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoria')); ?>:</b>
	<?php echo CHtml::encode($data->categoria); ?>
	<br />


</div>