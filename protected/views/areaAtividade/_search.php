<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nome_pt',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'nome_en',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'nome_es',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'imagem_big',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'imagem_small',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
