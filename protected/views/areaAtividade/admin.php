<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Áreas de Atividade',
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/AreaAtividade/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Áreas de Atividade";

?>
        

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'area-atividade-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'nome_pt',
		//'nome_en',
		'nome_es',
		//'imagem_big',
		//'imagem_small',
		'ordem',
		
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/AreaAtividade/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/AreaAtividade/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
));
$this->endWidget();
?>

</div>
</div>        
