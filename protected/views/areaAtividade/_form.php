<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'area-atividade-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nome_pt',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nome_es',array('class'=>'span5','maxlength'=>100)); ?>

        <?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'imagem_big',array('class'=>'control-label'));
        echo"<div class='controls'>";
        $this->widget('CMultiFileUpload', array(
                'name' => 'images',
                'accept' => 'jpeg|jpg|gif|png', // useful for verifying files
                'duplicate' => 'Duplicate file!', // useful, i think
                'denied' => 'Invalid file type', // useful, i think
            ));
        echo"</div></div>";
        ?>
        <?php
            $imagens=AreaAtividadeImg::model()->findAllByAttributes(array('area_atividade_id'=>$model->id)); 
            if(!$model->isNewRecord && $imagens!=null)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent2', array('model'=>$model,'imagens'=>$imagens));
            echo '</div>';
            }
        ?>

       

	<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span1')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
