<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_item_portfolio')); ?>:</b>
	<?php echo CHtml::encode($data->id_item_portfolio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file')); ?>:</b>
	<?php echo CHtml::encode($data->file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_pt')); ?>:</b>
	<?php echo CHtml::encode($data->nome_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_es')); ?>:</b>
	<?php echo CHtml::encode($data->nome_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_en')); ?>:</b>
	<?php echo CHtml::encode($data->nome_en); ?>
	<br />


</div>