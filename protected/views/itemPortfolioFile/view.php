<?php
$this->breadcrumbs=array(
	'Item Portfolio Files'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ItemPortfolioFile','url'=>array('index')),
array('label'=>'Create ItemPortfolioFile','url'=>array('create')),
array('label'=>'Update ItemPortfolioFile','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ItemPortfolioFile','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ItemPortfolioFile','url'=>array('admin')),
);
?>

<h1>View ItemPortfolioFile #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_item_portfolio',
		'file',
		'nome_pt',
		'nome_es',
		'nome_en',
),
)); ?>
