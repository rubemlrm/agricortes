<?php
$this->breadcrumbs=array(
	'Item Portfolio Files',
);

$this->menu=array(
array('label'=>'Create ItemPortfolioFile','url'=>array('create')),
array('label'=>'Manage ItemPortfolioFile','url'=>array('admin')),
);
?>

<h1>Item Portfolio Files</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
