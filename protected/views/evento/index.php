<?php
$this->pageTitle=Yii::app()->name . ' - '.mb_strtoupper(Content::model()->getContentReturn('eventos',Yii::app()->language),"UTF-8");
$titulo="titulo_".Yii::app()->language;
$conteudo="conteudo_".Yii::app()->language;
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <?php
        $menu=Menu::model()->findByAttributes(array("link"=>"evento"));
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$menu->imagem.'" width="100%">';
        ?>
        
    </div>
</div>
<div class="container branco">
    <div class="texto">
                
            <?php
            $model=$dataProvider->getData();
            
            foreach($model as $m)
            {
                if($m->link!=null)
                    $link=$m->link;
                else
                    $link="#";
                echo'<div class="row">
                    <div class="col-md-offset-2 col-md-5">
                        <h5><a href="'.$link.'" target="_blank"><i class="fa fa-calendar"></i>&nbsp;'.mb_strtoupper($m->$titulo,"UTF-8").' '.$m->data.' a '.$m->data_fim.'</a></h5>
                        '.$m->$conteudo.'
                    </div>
                    <div class="col-md-3">
                        <img src="'.Yii::app()->baseUrl.'/uploads/'.$m->imagem.'" style="max-width: 400px;">
                    </div>
                </div>
                <br>
                <br>';
            }
            ?>
            
               
            </div>
        </div>
    </div>
</div>

<br>
<br>