<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Eventos',
    'headerIcon' => 'icon-globe',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/evento/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Eventos";

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'noticia-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                'titulo_pt',
		'data',
                'data_fim',
		/*'titulo_en',
		'titulo_fr',
		'titulo_es',
		'conteudo_pt',
		
		'conteudo_en',
		'conteudo_fr',
		'conteudo_es',
		'data',
		'imagem',
		*/
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                       
                                        'update' => array
                                        (
                                            //'url'=>'Yii::app()->getController()->createUrl("/itemPortfolio/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            //'url'=>'Yii::app()->getController()->createUrl("/itemPortfolio/delete",array("id"=>$data->id))',

                                        ),
					),
		),    
),
));
$this->endWidget();
?>

        
</div>
</div>