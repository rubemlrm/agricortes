<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'evento-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span8','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span8','maxlength'=>100)); ?>

        <?php echo $form->textFieldRow($model,'link',array('class'=>'span12','maxlength'=>100)); ?>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'conteudo_pt',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->textArea($model, 'conteudo_pt', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-pt','placeholder'=>''));
      
        ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace( 'some-textarea-pt' );
        </script>
        
        
        <div class='control-group'>
        <?php echo $form->labelEx($model,'conteudo_es',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->textArea($model, 'conteudo_es', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-es','placeholder'=>'')); ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace( 'some-textarea-es' );
        </script>


        <div class='control-group'>
        <?php echo $form->labelEx($model,'data',array('class'=>'control-label')); ?>   
        <div class='controls'>   
         <?php 
         
         echo"<div class='input-prepend'><span class='add-on'><i class='icon-calendar'></i></span>";
         $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'attribute'=>'data',
                        'value'=>$model->data, 
                        'options'=>array(
                        'dateFormat' => 'yy-mm-dd',
                        'altFormat' => 'yy-mm-dd', // show to user format    
                        'showAnim'=>'fold',
                        ),
                        'htmlOptions'=>array(
                        'style'=>'height:20px;',
                        'class'=>'span10',
                        'prepend'=>'<i class="icon-calendar">>/i>',    
                        ),
                        ));
         echo"</div>";
         ?>
        </div></div>
        
        <div class='control-group'>
        <?php echo $form->labelEx($model,'data_fim',array('class'=>'control-label')); ?>   
        <div class='controls'>   
         <?php 
         
         echo"<div class='input-prepend'><span class='add-on'><i class='icon-calendar'></i></span>";
         $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'attribute'=>'data_fim',
                        'value'=>$model->data_fim, 
                        'options'=>array(
                        'dateFormat' => 'yy-mm-dd',
                        'altFormat' => 'yy-mm-dd', // show to user format    
                        'showAnim'=>'fold',
                        ),
                        'htmlOptions'=>array(
                        'style'=>'height:20px;',
                        'class'=>'span10',
                        'prepend'=>'<i class="icon-calendar">>/i>',    
                        ),
                        ));
         echo"</div>";
         ?>
        </div></div>

	<?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'imagem',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'imagem');
        echo"</div></div>";
        
        ?>
        <?php
            if(!$model->isNewRecord)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>

        