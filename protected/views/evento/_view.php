<?php
$conteudo="conteudo_".Yii::app()->language;
$titulo="titulo_".Yii::app()->language;
?>
<article class="post">
    <img src="uploads/<?php echo $data->imagem; ?>" alt="//" />
    <header>
        <h3><?php echo CHtml::encode($data->$titulo); ?></h3>
    </header>
    
    <p><?php
    $string = strip_tags($data->$conteudo);

    if (strlen($string) > 300) {
        $stringCut = substr($string, 0, 300);

    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
}
    
    echo $string;
    ?></p>
    <?php echo CHtml::link(Content::model()->getContentReturn('ler-mais',Yii::app()->language).' <i class="entypo-right-open" ></i>',array('view','id'=>$data->id),array('class'=>'readmore')); ?>
    <footer>
        <span><i class="entypo-calendar" ></i><?php echo CHtml::encode($data->data); ?> </span>
    </footer>
</article>