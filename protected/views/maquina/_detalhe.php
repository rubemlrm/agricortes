<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

<div class='control-group'>
        <?php echo $form->labelEx($model,'descricao_pt',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->textArea($model, 'descricao_pt', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-pt','placeholder'=>''));
      
        ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace('some-textarea-pt', {
                "extraPlugins": "imagebrowser",
                "imageBrowser_listUrl": "<?php
                $url=Website::model()->findByPk(1);
                if($url!=null)
                    echo $url->url;
                ?>/uploads/image_list.js"
            });
        </script>
        
<div class='control-group'>
        <?php echo $form->labelEx($model,'descricao_es',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->textArea($model, 'descricao_es', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-es','placeholder'=>''));
      
        ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace('some-textarea-es', {
                "extraPlugins": "imagebrowser",
                "imageBrowser_listUrl": "<?php
                $url=Website::model()->findByPk(1);
                if($url!=null)
                    echo $url->url;
                ?>/uploads/image_list.js"
            });
        </script>        