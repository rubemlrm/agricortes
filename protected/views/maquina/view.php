<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Artigo do Portfólio '.$model->titulo_pt,
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar', 'url'=>Yii::app()->baseUrl.'/maquina/update/'.$model->id),    
    array('label'=>'Editar Imagens', 'url'=>Yii::app()->baseUrl.'/maquina/updateimg/'.$model->id),    
    array('label'=>'Gerir Artigos do Portfólio', 'url'=>Yii::app()->baseUrl.'/maquina/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Artigo do Portfólio ".$model->titulo_pt;

?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
                array(
                    'name'=>'categoria_id',
                    'type'=>'raw',
                    'value'=>Categoria::model()->findByPk($model->categoria_id)->nome_pt,
                ),
                array(
                    'name'=>'imagem_capa',
                    'type'=>'raw',
                    'value'=>CHtml::image(Yii::app()->request->baseUrl.'/uploads/'.$model->imagem_capa,'imagem_capa',array("width"=>100,'class'=>'img-polaroid')),
                    'visible'=>($model->imagem_capa==="" || $model->imagem_capa==null) ? false : true,
                ),
		'titulo_pt',
		'titulo_en',
                'titulo_fr',
                'titulo_es',
		'subtitulo_pt',
		'subtitulo_en',
                'subtitulo_fr',
                'subtitulo_es',
                array(
                    'name'=>'descricao_pt',
                    'type'=>'raw',
                    'value'=>$model->descricao_pt,
                ),
                array(
                    'name'=>'descricao_en',
                    'type'=>'raw',
                    'value'=>$model->descricao_en,
                ),
                array(
                    'name'=>'descricao_fr',
                    'type'=>'raw',
                    'value'=>$model->descricao_fr,
                ),
                array(
                    'name'=>'descricao_es',
                    'type'=>'raw',
                    'value'=>$model->descricao_es,
                ),
    
		'ordem',
),
));
?>
       
    <?php
    $galeria=ItemPortfolioImg::model()->getByItem($model->id);
    
    if($galeria!=null)
    {
        echo'<div class="row-fluid">
                <div class="span12 well">';
    $conta=0;
    foreach($galeria as $g)
    {
        $conta++;
        if($conta==1||(($conta-1)%4==0))
                echo'<ul class="thumbnails">';
            
        echo'<li class="span3">
        <a href="#" class="thumbnail">
        <img src="'.Yii::app()->request->baseUrl.'/uploads/'.$g->imagem.'" alt="">
        </a>
        </li>';   
        
        if($conta%4==0)
            echo'</ul>';
    }
     echo'</div></div>';
        
    }
    ?>
    

<?php
$this->endWidget();
?>

</div>
</div>


