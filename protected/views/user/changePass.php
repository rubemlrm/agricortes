<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Alterar Password do Utilizador '.$model->username,
    'headerIcon' => 'icon-lock',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar Dados', 'url'=>Yii::app()->baseUrl.'/user/update/'.$model->id),     
    array('label'=>'Gerir Utilizadores', 'url'=>Yii::app()->baseUrl.'/user/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Alterar Password do Utilizador ".$model->username;

?>

<?php echo $this->renderPartial('_pass', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>
