<div class="row-fluid">
    <div class="span12">

<?php

    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Dados do Utilizador '.$model->username,
    'headerIcon' => 'icon-pencil',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Alterar Password', 'url'=>Yii::app()->baseUrl.'/user/changePass/'.$model->id),      
    array('label'=>'Gerir Utilizadores', 'url'=>Yii::app()->baseUrl.'/user/admin'),
        
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Dados do Utilizador ".$model->username;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>