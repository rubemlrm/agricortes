<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Utilizadores',
    'headerIcon' => 'icon-user',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/user/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Utilizadores";

$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	'Manage',
);


?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		
		'username',
		'email',
                'firstname',
               
                array(
                  'name'=>'roles',
                  'type'=>'raw',
                  'value'=> '$data->roles=="admin" ? CHtml::openTag("span",array("class"=>"label label-warning"))."$data->roles".CHtml::closeTag("span") : CHtml::openTag("span",array("class"=>"label label-info"))."$data->roles".CHtml::closeTag("span")',
                ),
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {delete}',
			'buttons'=>array(
                                        'view' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/user/view",array("id"=>$data->id))',

                                        ),
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/user/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/user/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>

