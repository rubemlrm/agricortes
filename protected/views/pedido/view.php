<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Pedidos',
    'headerIcon' => 'icon-question-sign',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Voltar', 'url'=>Yii::app()->baseUrl.'/pedido/admin'),    
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Pedidos";

?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'id',
                'data',
                'assunto',
		'nome',
		'empresa',
		'telemovel',
                'email',
		'cod_postal',
		'mensagem',
		
),
));
$this->endWidget();
?>
</div>
</div>