<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Pedidos',
    'headerIcon' => 'icon-question-sign',
    'headerButtons' => array(
   
    )));

$this->pageTitle=Yii::app()->name.' - '."Pedidos";

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pedido-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                'data',
                'assunto',
		'nome',
		'empresa',
		//'telemovel',
                //'email',
		//'cod_postal',
		//'mensagem',
		
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {delete}',
			'buttons'=>array(
                                       
                                       
					),
		),    
),
));
$this->endWidget();
?>
</div>
</div>