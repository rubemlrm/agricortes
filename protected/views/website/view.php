<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Definições do Website',
    'headerIcon' => 'icon-globe',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar', 'url'=>Yii::app()->baseUrl.'/website/update/1'),    
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Definições do Website";

?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'id',
		'titulo_pt',
		'titulo_en',
		'titulo_fr',
		'titulo_es',
		'description_pt',
		'description_en',
		'description_fr',
		'description_es',
		'keywords_pt',
		'keywords_en',
		'keywords_fr',
		'keywords_es',
		'url',
),
));

$this->endWidget();
?>


</div>
</div>
