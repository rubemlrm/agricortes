   
    <div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Definições do Website',
    'headerIcon' => 'icon-globe',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Voltar', 'url'=>Yii::app()->baseUrl.'/website/view/1'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Definições do Website";

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>