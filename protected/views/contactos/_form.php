<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-wysihtml5-0.0.2.css">
<script src="<?php echo Yii::app()->baseUrl; ?>/js/wysihtml5-0.3.0_rc2.js"></script>      
<script src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap-wysihtml5-0.0.2.js"></script>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'contactos-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'empresa',array('class'=>'span5','maxlength'=>100)); ?>

	<?php //echo $form->textAreaRow($model,'morada',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
        <div class='control-group'>
        <?php echo $form->labelEx($model,'morada',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->textArea($model, 'morada', array('maxlength' => 300, 'rows' => 6, 'cols' => 50,'class'=>'span5','id'=>'some-textarea','placeholder'=>'')); ?>
        </div></div>
        <script type="text/javascript">
                $('#some-textarea').wysihtml5({

                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false //Button to insert an image. Default true,

                });
        </script>    

	<?php echo $form->textFieldRow($model,'telefone',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'telemovel',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'fax',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'gps',array('class'=>'span5','maxlength'=>50)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
