<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'empresa',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textAreaRow($model,'morada',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'telefone',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'telemovel',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'fax',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'gps',array('class'=>'span5','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
