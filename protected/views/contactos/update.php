   
    <div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Contactos',
    'headerIcon' => 'icon-envelope',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Voltar', 'url'=>'index.php?r=contactos/view&id=1'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Contactos";

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>