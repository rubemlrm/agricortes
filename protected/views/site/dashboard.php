<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Dashboard',
    'headerIcon' => 'icon-home',
    'headerButtons' => array(
   /*
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Left', 'url'=>'#'),
    array('label'=>'Middel', 'url'=>'#'),
    array('label'=>'Right', 'url'=>'#')
    ),
    ),*/
    )));
    ?>
   
    <div class="hero-unit">
    <h1><?php echo CHtml::encode(Yii::app()->name); ?></h1>
    <p>Backoffice</p>
    <p>
    <?php
    $url=Website::model()->findByPk(1);
    if($url!=null)
        echo'<a class="btn btn-primary btn-large" href="'.$url->url.'" target="_blank">Visitar Site</a>';
    ?>
    
    <a class="btn btn-success btn-large" href="http://www.google.com/analytics" target="_blank">
    Analytics
    </a>    
    </p>
    </div>
   
    <?php
    $this->endWidget();
?>
</div>
</div>

