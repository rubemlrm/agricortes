<?php
$this->pageTitle=Yii::app()->name . ' - DOWNLOADS';
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <?php
        $menu=Menu::model()->findByAttributes(array("link"=>"downloads"));
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$menu->imagem.'" width="100%">';
        ?>
    </div>
</div>
<div class="container branco">
        <br>
        <div class="col-md-3">
            <h5><?php echo mb_strtoupper(Content::model()->getContentReturn('catalogos', Yii::app()->language),"UTF-8"); ?></h5>
            <div class="panel-group">
            <?php
            $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
            $nome="nome_".Yii::app()->language;
            $slug="slug_".Yii::app()->language;
            foreach($area_atividade as $area)
            {
                //echo'<div class="caixa_catalogos"><h6>'.CHtml::link(mb_strtoupper($area->$nome,"UTF-8"),Yii::app()->baseUrl."/downloads/",array('style'=>'color:#6E6E6E','class'=>'link_cat', 'id'=>'link'.$area->id)).'<h6></div>';

		echo '<div class="panel panel-default"><div class="panel-heading">
                  <h5 class="panel-title">
                      '. CHtml::link(mb_strtoupper($area->$nome,"UTF-8"),Yii::app()->baseUrl."/downloads/",array('style'=>'color:#6E6E6E','class'=>'link_cat', 'id'=>'link'.$area->id))  .'
                  </h5>
                </div></div>';
            }
            ?>
</div>
            <br>
        </div>
        <div class="col-md-9">
          <br>
          <br>
            <?php
            $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
            $titulo="titulo_".Yii::app()->language;
            $display="display:none;";
            foreach($area_atividade as $area)
            {
                echo '<div id="divlink'.$area->id.'" style="'.$display.'" class="divlink">';
                $Criteria = new CDbCriteria();
                $Criteria->condition = "area_atividade_id = ".$area->id;
                $Criteria->order = "ordem ASC";
                $categoria = Categoria::model()->findAll($Criteria);
                
                $num_catalogos=0;
                
                foreach($categoria as $c)
                {
                    //if($num_catalogos==0 || (($num_catalogos%20)==0))
                      //  echo'<div style="display:block;float:left;padding-right:20px;">';
                   
                    $Criteria = new CDbCriteria();
                    $Criteria->condition = "categoria_id = ".$c->id."";
                    $Criteria->order = "ordem ASC";
                    $maquinas = Maquina::model()->findAll($Criteria);
                   
                    foreach($maquinas as $m)
                    {
                        
                        $Criteria->condition = "id_item_portfolio = ".$m->id;
                        $Criteria->order = "file ASC";
                        $files = ItemPortfolioFile::model()->findAll($Criteria);
                        
                        foreach($files as $f)
                        {
                            $num_catalogos++;
                            $file_clean=explode('.',$f->file);
                            
                            $file_clean=str_replace("_"," ",$file_clean[0]);
                            
                            echo "<span style='font-size:11px;'>".CHtml::link(mb_strtoupper($file_clean,"UTF-8"), array("ItemPortfolioFile/download", "id"=>$f->id))."<br></span>";
                        }
                        
                        //$num_catalogos++;
                        //echo CHtml::link(mb_strtoupper($m->$titulo,"UTF-8").' (.PDF)', array("Maquina/download", "id"=>$m->id))."<br>";
                    }
                   
                //if((sizeof($categoria)==$num_catalogos)||(($num_catalogos)%20==0))
                    //echo'</div>';
                }
                
                if($num_catalogos==0)
                        Content::model()->getContentEcho('catalogos_no', Yii::app()->language);
                
                echo"</div>";
                $display="display:none;";
            }
            ?>
            
        </div>
</div>

<br>
<br>

