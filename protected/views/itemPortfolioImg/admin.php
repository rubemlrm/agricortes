<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Gestão de Galerias',
    'headerIcon' => 'icon-film',
    'headerButtons' => array(
   
    )));

$this->pageTitle=Yii::app()->name.' - '."Gestão de Galerias";

?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
                'nome_pt',
                array(
                    'name'=>'id_item_portfolio',
                    'type'=>'raw',
                    'value'=>'Maquina::model()->findByPk($data->id_item_portfolio)->titulo_pt',
                ),
                array(
                    'name'=>'imagem',
                    'type'=>'raw',
                    'value'=>'($data->imagem==="" || $data->imagem==null) ? "sem imagem" : CHtml::image(Yii::app()->request->baseUrl."/uploads/".$data->imagem,"imagem",array("width"=>100,"class"=>"img-polaroid"))',
                ),
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                        
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/ItemPortfolioImg/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/ItemPortfolioImg/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>

