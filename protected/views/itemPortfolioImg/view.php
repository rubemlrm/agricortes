<?php
$this->breadcrumbs=array(
	'Item Portfolio Imgs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ItemPortfolioImg','url'=>array('index')),
array('label'=>'Create ItemPortfolioImg','url'=>array('create')),
array('label'=>'Update ItemPortfolioImg','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ItemPortfolioImg','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ItemPortfolioImg','url'=>array('admin')),
);
?>

<h1>View ItemPortfolioImg #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_item_portfolio',
		'imagem',
),
)); ?>
