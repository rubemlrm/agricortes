<?php
$this->breadcrumbs=array(
	'Item Portfolio Imgs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ItemPortfolioImg','url'=>array('index')),
array('label'=>'Manage ItemPortfolioImg','url'=>array('admin')),
);
?>

<h1>Create ItemPortfolioImg</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>