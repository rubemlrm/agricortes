<?php
$conteudo="conteudo_".Yii::app()->language;
$descricao="descricao_".Yii::app()->language;
$titulo="titulo_".Yii::app()->language;
$nome="nome_".Yii::app()->language;
$slug="slug_".Yii::app()->language;
$this->pageTitle=Yii::app()->name." - ".mb_strtoupper(Content::model()->getContentReturn('areas_atividade', Yii::app()->language),"UTF-8").' - '.mb_strtoupper($area_escolhida->$nome,"UTF-8").' - '.mb_strtoupper(Content::model()->getContentReturn('maquinas', Yii::app()->language),"UTF-8").' - '.mb_strtoupper($model->$nome,"UTF-8");
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
                <div class="row carousel-holder">
                    <div class="hidden-xs col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                                <?php
                                $cor=AreaAtividade::model()->getColor($area_escolhida->id);
                                echo'<div class="carousel-inner" style="border-bottom: '.$cor.' 5px solid;">';
                                ?>
                                <?php
                                $Criteria = new CDbCriteria();
                                $Criteria->condition = "area_atividade_id = ".$area_escolhida->id;
                                $slider = AreaAtividadeImg::model()->findAll($Criteria);

                                $active="active";
                                $titulo="titulo_".Yii::app()->language;
                                foreach ($slider as $s)
                                {
                                    echo'<div class="item '.$active.'">
                                    <img class="slide-image" src="../uploads/'.$s->imagem.'" alt="">';

                                    echo'</div>';
                                $active="";
                                }
                                ?>

                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>
    </div>
</div>

<div class="container branco">
        <br>
        <div class="col-md-3">
            <h5><?php echo mb_strtoupper($area_escolhida->$nome,"UTF-8"); ?></h5>
            <div class="panel-group" id="accordion">

              <?php

              foreach ($categorias as $c)
              {
                  $Criteria = new CDbCriteria();
                  $Criteria->condition = "categoria_main = ".$c->id;
                  $Criteria->order="ordem ASC";
                  $categorias_dependent = Categoria::model()->findAll($Criteria);
                  if(sizeof($categorias_dependent)>=1)
                  {
                  echo'<div class="panel panel-default">';
                  echo'<div class="panel-heading">
                  <h5 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$c->id.'">
                      '.mb_strtoupper($c->$nome,"UTF-8").'
                    </a>
                  </h5>
                </div>';

                    if($categorias_dependent!=null)
                    {
                    if($model->categoria_main==$c->id)
                        $in="in";
                    else
                        $in="";
                    echo'<div id="collapse'.$c->id.'" class="panel-collapse collapse '.$in.'">
                        <div class="panel-body">';
                        foreach($categorias_dependent as $c2){

                            if($c2->id==$model->id)
                                echo'<a href="'.Yii::app()->baseUrl.'/categoria/'.$c2->$slug.'"><b>'.mb_strtoupper($c2->$nome,"UTF-8").'</b></a><br>';
                            else
                                echo'<a href="'.Yii::app()->baseUrl.'/categoria/'.$c2->$slug.'">'.mb_strtoupper($c2->$nome,"UTF-8").'</a><br>';
                        }
                        echo'</div>
                      </div>';
                    }
                  echo'</div>';
                  }
                  else
                  {
                      echo'<div class="panel panel-default">';
                  echo'<div class="panel-heading">
                  <h5 class="panel-title">
                    <a href="'.Yii::app()->baseUrl.'/categoria/'.$c->$slug.'">
                      '.mb_strtoupper($c->$nome,"UTF-8").'
                    </a>
                  </h5>
                </div>';
                  echo'</div>';
                  }

              }
              ?>


            </div>
            <br>
            <br>
          <div class="hidden-xs">
            <h5><?php mb_strtoupper(Content::model()->getContentEcho('desejo_ser_contactado', Yii::app()->language),"UTF-8");?></h5>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model2); ?>
        <input type="hidden" value="<?php echo $model->$nome; ?>" id="ContactForm_assunto" name="ContactForm[assunto]">
	<div class="form-group">
                <?php echo $form->textField($model2,'name',array('class'=>'form-control','placeholder'=>'Nome')); ?>
        </div>
        <div class="form-group">
                <?php echo $form->textField($model2,'empresa',array('class'=>'form-control','placeholder'=>'Empresa')); ?>
        </div>
        <div class="form-group">
                <?php echo $form->textField($model2,'telemovel',array('class'=>'form-control','placeholder'=>'Telemóvel')); ?>
        </div>
        <div class="form-group">
                <?php echo $form->textField($model2,'email',array('class'=>'form-control','placeholder'=>'E-mail')); ?>
        </div>
        <div class="form-group">
                <?php echo $form->textField($model2,'codpostal',array('class'=>'form-control','placeholder'=>'Código Postal')); ?>
        </div>
        <div class="form-group">
                <?php echo $form->textArea($model2,'body',array('class'=>'form-control','placeholder'=>'Mensagem')); ?>
        </div>

	<br><button type="submit" class="btn btn-default">Enviar</button><br><br>
<?php $this->endWidget(); ?>


<?php endif; ?>
          </div>
        </div>
        <div class="col-md-9">
        <ul class="nav nav-tabs" id="myTab">
              <?php
              $inicio="active";
              foreach ($maquinas as $m)
              {
                  echo '<li class="'.$inicio.'"><a href="#'.$m->id.'" data-toggle="tab">'.mb_strtoupper($m->$titulo,"UTF-8").'</a></li>';
                  $inicio="";
              }
              ?>
        </ul>
        </div>



            <?php
              $inicio="active";
              echo '<div class="tab-content">';
              foreach ($maquinas as $m)
              {

                echo'<div class="tab-pane '.$inicio.'" id="'.$m->id.'">';

                echo'<div class="col-md-6">';

                if($m->imagem_capa!=null && $m->imagem_capa!="")
                    echo'<br><img src="'.Yii::app()->baseUrl."/uploads/".$m->imagem_capa.'" >';
                if($m->link_website!=null && $m->link_website!="")
                    echo "<br><a href='".$m->link_website."' target='_blank'>".$m->link_website."</a>";
                echo'<br>
                <br>
                <h5>'.mb_strtoupper($m->$titulo,"UTF-8").'</h5>';

                echo $m->$descricao;
                echo'<br><br>';

                echo'</div>';//col6

                $inicio="";


                ?>
                <div class="col-md-3">
            <?php
            echo'<br>';
            if($maquinas!=null)
            {

            $Criteria = new CDbCriteria();
            $Criteria->condition = "id_item_portfolio = ".$m->id;
            $imagens = ItemPortfolioImg::model()->findAll($Criteria);
            $conta_img=0;
            foreach($imagens as $img){
                $conta_img++;
                if($conta_img==1)
                    echo'<a href="../uploads/'.$img->imagem.'" rel="prettyPhoto[pp_gal'.$m->id.']"><img class="slide-image" src="../uploads/'.$img->imagem.'" alt="" width=100% style="padding-bottom:3px"></a>';
                else{
                    if($conta_img<=4)
                    {
                        if($conta_img==3)
                            echo'<a href="../uploads/'.$img->imagem.'" rel="prettyPhoto[pp_gal'.$m->id.']"><img class="slide-image" src="../uploads/'.$img->imagem.'" alt="" width=32% style="margin:0px 2% 0px 2%;"></a>';
                        else
                            echo'<a href="../uploads/'.$img->imagem.'" rel="prettyPhoto[pp_gal'.$m->id.']"><img class="slide-image" src="../uploads/'.$img->imagem.'" alt="" width=32% style="margin:0px 0px 0px 0px;"></a>';
                    }

                }
            }

            if($conta_img!=0)
                echo'<br><br>';

            $catalogos=ItemPortfolioFile::model()->findAllByAttributes(array('id_item_portfolio'=>$m->id));
            if($catalogos!=null)
                echo'<h5>'.mb_strtoupper(Content::model()->getContentReturn('catalogos', Yii::app()->language),"UTF-8").'</h5>';
                foreach($catalogos as $cat)
                {
                    echo "<span style='font-size:11px;'>".CHtml::link($cat->file, array("ItemPortfolioFile/download", "id"=>$cat->id))."</span><br>";
                }
            echo'<br>';
            if($m->link_video!=null && $m->link_video!="")
            {
                $videos=explode("#",$m->link_video);
                foreach($videos as $video){
                    echo'<br><iframe height="100%" width="100%" src="//www.youtube.com/embed/'.$video.'" frameborder="0" allowfullscreen></iframe>';
                    echo'<br><br>';
                }
            }


            }
            ?>
        </div>
                <?php


                echo'</div>';//tab-pane
              }
              echo'</div>';
              ?>




</div>


<br>
<br>

<script type="text/javascript">
$(document).ready(function(){
    var str = $( ".active a" ).text();
    $("#ContactForm_assunto").val(str);
    $( "ul li" ).click(function() {
        setTimeout(
        function()
        {
          var str = $( ".active a" ).text();
          $("#ContactForm_assunto").val(str);
        }, 500);

    });
});
</script>