<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_pt')); ?>:</b>
	<?php echo CHtml::encode($data->nome_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_en')); ?>:</b>
	<?php echo CHtml::encode($data->nome_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordem')); ?>:</b>
	<?php echo CHtml::encode($data->ordem); ?>
	<br />


</div>