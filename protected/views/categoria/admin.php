<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Categorias',
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/categoria/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Categorias";
$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	'Manage',
);


?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                array(
                    'name'=>'area_atividade_id',
                    'type'=>'raw',
                    'value'=>'AreaAtividade::model()->getById($data->area_atividade_id)',
                ),
                array(
                    'name'=>'categoria_main',
                    'type'=>'raw',
                    'value'=>'Categoria::model()->getById($data->categoria_main)',
                ),
                //'id',
		'nome_pt',
		//'nome_en',
                array(
                    'name'=>'ordem',
                    'value'=>'$data->ordem',
                    'filter'=>false,
                    ),
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/categoria/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/categoria/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>
