<?php
$this->breadcrumbs=array(
	'Area Atividade Imgs'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List AreaAtividadeImg','url'=>array('index')),
array('label'=>'Create AreaAtividadeImg','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('area-atividade-img-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Area Atividade Imgs</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'area-atividade-img-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'area_atividade_id',
		'imagem',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
