<?php
$this->breadcrumbs=array(
	'Area Atividade Imgs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List AreaAtividadeImg','url'=>array('index')),
	array('label'=>'Create AreaAtividadeImg','url'=>array('create')),
	array('label'=>'View AreaAtividadeImg','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage AreaAtividadeImg','url'=>array('admin')),
	);
	?>

	<h1>Update AreaAtividadeImg <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>