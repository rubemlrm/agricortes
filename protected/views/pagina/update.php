<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Página '.$model->titulo_pt,
    'headerIcon' => 'icon-book',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir Páginas', 'url'=>Yii::app()->baseUrl.'/pagina/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Página ".$model->titulo_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>