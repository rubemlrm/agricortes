<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Gestão de Páginas',
    'headerIcon' => 'icon-book',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/pagina/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Gestão de Páginas";

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pagina-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'titulo_pt',
		/*'titulo_en',
		'titulo_fr',
		'titulo_es',
		'conteudo_pt',
		
		'conteudo_en',
		'conteudo_fr',
		'conteudo_es',
		'imagem',
		*/
array(
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{update} {delete}',
        'buttons'=>array(
            'update' => array(),
            'delete' => array(),
            ),
                ), 
),
));
$this->endWidget();
?>

</div>
</div>   
