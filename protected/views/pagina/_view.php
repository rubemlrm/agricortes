<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_en')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_fr')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_es')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conteudo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->conteudo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conteudo_en')); ?>:</b>
	<?php echo CHtml::encode($data->conteudo_en); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('conteudo_fr')); ?>:</b>
	<?php echo CHtml::encode($data->conteudo_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conteudo_es')); ?>:</b>
	<?php echo CHtml::encode($data->conteudo_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagem')); ?>:</b>
	<?php echo CHtml::encode($data->imagem); ?>
	<br />

	*/ ?>

</div>