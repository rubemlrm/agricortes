<?php
$this->breadcrumbs=array(
	'Paginas',
);

$this->menu=array(
array('label'=>'Create Pagina','url'=>array('create')),
array('label'=>'Manage Pagina','url'=>array('admin')),
);
?>

<h1>Paginas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
