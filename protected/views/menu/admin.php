<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Gestão de Menus',
    'headerIcon' => 'icon-list',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/menu/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Gestão de Menus";

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'menu-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'titulo_pt',
                'posicao',
                //'ordem',
                array(
                    'name'=>'ordem',
                    'value'=>'$data->ordem',
                    'filter'=>false,
                ),
                array(
                    'name'=>'visivel',
                    'value'=>'Menu::model()->getVisivel($data->id)',
                    'filter'=>false,
                ),
array(
        'class'=>'bootstrap.widgets.TbButtonColumn',
        'template'=>'{update} {delete}',
        'buttons'=>array(
            'update' => array(),
            'delete' => array(),
            ),
                ),  
),
));
$this->endWidget();
?>

</div>
</div>        
