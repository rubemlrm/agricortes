<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Conteúdos',
    'headerIcon' => 'icon-tags',
    'headerButtons' => array(
   
    
    )));

$this->pageTitle=Yii::app()->name.' - '."Conteúdos";

$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	'Manage',
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                'slug',
		'value_pt',
		'value_es',
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}',
			'buttons'=>array(
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/content/update",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>

