<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'content-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'slug',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'value_pt',array('class'=>'span6','maxlength'=>500)); ?>

        <?php echo $form->textFieldRow($model,'value_es',array('class'=>'span6','maxlength'=>500)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
