<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Conteúdo '.$model->slug,
    'headerIcon' => 'icon-tags',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Voltar', 'url'=>'index.php?r=content/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Conteúdo ".$model->slug;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>