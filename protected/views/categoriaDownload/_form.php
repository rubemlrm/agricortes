<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'categoria-download-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span5','maxlength'=>100)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
