<?php
$this->breadcrumbs=array(
	'Categoria Downloads',
);

$this->menu=array(
array('label'=>'Create CategoriaDownload','url'=>array('create')),
array('label'=>'Manage CategoriaDownload','url'=>array('admin')),
);
?>

<h1>Categoria Downloads</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
