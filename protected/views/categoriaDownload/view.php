<?php
$this->breadcrumbs=array(
	'Categoria Downloads'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List CategoriaDownload','url'=>array('index')),
array('label'=>'Create CategoriaDownload','url'=>array('create')),
array('label'=>'Update CategoriaDownload','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete CategoriaDownload','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage CategoriaDownload','url'=>array('admin')),
);
?>

<h1>View CategoriaDownload #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'titulo_pt',
		'titulo_es',
),
)); ?>
