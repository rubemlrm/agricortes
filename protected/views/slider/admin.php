<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Slider',
    'headerIcon' => 'icon-film',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/slider/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Slider";

?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		'id',
                array(
                    'name'=>'imagem',
                    'type'=>'raw',
                    'value'=>'($data->imagem==="" || $data->imagem==null) ? "sem imagem" : CHtml::image(Yii::app()->request->baseUrl."/uploads/".$data->imagem,"imagem",array("width"=>100,"class"=>"img-polaroid"))',
                ),
		'ordem',
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                        'view' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/slider/view",array("id"=>$data->id))',

                                        ),
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/slider/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/slider/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>

