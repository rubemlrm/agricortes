<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Slider '.$model->id,
    'headerIcon' => 'icon-film',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar', 'url'=>'index.php?r=slider/update&id='.$model->id),    
    array('label'=>'Gerir Slider', 'url'=>'index.php?r=slider/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Slider ".$model->id;

?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
               
		'id',
                array(
                    'name'=>'imagem',
                    'type'=>'raw',
                    'value'=>($model->imagem==="" || $model->imagem==null) ? "sem imagem" : CHtml::image(Yii::app()->request->baseUrl.'/uploads/'.$model->imagem,'imagem',array("width"=>100,'class'=>'img-polaroid')),
                ),
    
		'ordem',
),
));

$this->endWidget();
?>


</div>
</div>




