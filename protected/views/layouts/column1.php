<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php $description = "description_" . Yii::app()->language;
    echo Website::model()->findByPk(1)->$description ?>">
    <meta name="keywords" content="<?php $keywords = "keywords_" . Yii::app()->language;
    echo Website::model()->findByPk(1)->$keywords ?>">
    <meta name="author" content="Virtualnet">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>


    <?php Yii::app()->clientScript->scriptMap = array('font-awesome.css' => false, 'bootstrap-combined.no-icons.min.css' => false, 'bootstrap-yii.css' => false, 'jquery-ui-bootstrap.css' => false, 'bootstrap-notify.css' => false, 'jquery.js' => false, 'bootstrap.js' => false, 'bootstrap.bootbox.min.js' => false, 'bootstrap.notify.js' => false); ?>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css"/>
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/agricortes.min.css" rel="stylesheet">
    <link href='http://cdn.jsdelivr.net/prettyphoto/3.1.5/css/prettyPhoto.css' media='screen' rel='stylesheet'
          type='text/css'/>
</head>

<body>

<?php
//Analisar o IP de origem

/*$location = @file_get_contents('http://freegeoip.net/json/' . $_SERVER['REMOTE_ADDR']);
if ($location != null) {
    $pais = explode("country_code", $location);
    $pais = substr($pais[1], 3, 2);
    if ($pais == "ES")
        Yii::app()->language = "es";
} */
/*elseif($pais=="ES")
    Yii::app()->language="es";
else
    Yii::app()->language="pt";*/
//-----------------------

$conteudo = "conteudo_" . Yii::app()->language;



?>

<nav id="top" class="navbar navbar-default navbar-fixed-top hidden-xs" role="navigation">
    <div class="container">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-right:300px">
            <form class="navbar-form navbar-right" role="search">
                <p id="idioma">
                    <!--<a href="#">PT</a> | <a href="#">ES</a> | <a href="#">EN</a>-->
                    <?php
                    $this->widget('application.components.widgets.LanguageSelector');
                    ?>
                </p>
            </form>

            <?php

            if (Yii::app()->user->id == null) {
                ?>
                <form name="pesquisa" class="navbar-form navbar-right" role="search"
                      action="<?php echo Yii::app()->baseUrl ?>/site/login2" method="post">
                    <button type="submit"
                            class="btn btn-default btn-xs"><?php Content::model()->getContentEcho('area_reservada', Yii::app()->language); ?></button>
                </form>
            <?php
            } else {
                ?>
                <form name="pesquisa" class="navbar-form navbar-right" role="search"
                      action="<?php echo Yii::app()->baseUrl ?>/site/AreaReservada" method="post">
                    <button type="submit"
                            class="btn btn-default btn-xs"><?php Content::model()->getContentEcho('area_reservada', Yii::app()->language); ?></button>
                </form>
                <?php
                if ( ! (Yii::app()->user->isGuest)) {
                    ?>
                    <form name="pesquisa" class="navbar-form navbar-right" role="search"
                          action="<?php echo Yii::app()->baseUrl ?>/site/logout" method="post">
                        <button type="submit" class="btn btn-default btn-xs">Logout</button>
                    </form>

                <?php
                }
            }
            ?>

            <form name="pesquisa" class="navbar-form navbar-right" role="search"
                  action="<?php echo Yii::app()->baseUrl ?>/site/pesquisa" method="post">
                <div class="form-group">
                    <input name="pesquisatxt" type="text" class="form-control input-sm"
                           placeholder="<?php Content::model()->getContentEcho('pesquisar', Yii::app()->language); ?>">
                </div>
                <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-search"></i></button>
            </form>

            <form class="navbar-form navbar-right" id="social">
                <a href="http://www.facebook.com/agricortes.sa" target="_blank"><i
                        class="fa fa-facebook-square fa-lg"></i></a>
                <a href="http://www.youtube.com/user/AGRICORTES" target="_blank"><i
                        class="fa fa-youtube-square fa-lg"></i></a>
                <a href="http://www.linkedin.com/pub/agricortes-s-a/8b/3a7/116" target="_blank"><i
                        class="fa fa-linkedin fa-lg"></i></a>
                <a href="mailto:agricortes@agricortes.com" target="_blank"><i class="fa fa-envelope fa-lg"></i></a>
            </form>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--<div class="container">
    <div class="hidden-xs col-md-offset-11 col-md-1">
    <img class="club" src="<?php //echo Yii::app()->baseUrl; ?>/images/club.png">
    </div>
</div>-->
<br class="hidden-xs">

<div class="container-fluid nopadding visible-xs">
    <div class="col-sx-12">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                    <?php
                        $menu = Yii::app()->db->createCommand("SELECT pagina.titulo_pt as pagina_titulo_pt, pagina.titulo_es as pagina_titulo_es,pagina.slug_pt as pagina_slug_pt,pagina.slug_en as pagina_slug_en, menu.titulo_pt as menu_titulo_pt,menu.titulo_es as menu_titulo_es,menu.id as menuID, menu.link,menu.ordem ,area_atividade.nome_pt as area_atividade_nome_pt, area_atividade.nome_es as area_atividade_nome_es,area_atividade.slug_pt as area_atividade_slug_pt, area_atividade.slug_en as area_atividade_slug_en FROM menu
                        LEFT JOIN pagina ON menu.pagina_id=pagina.id
                        LEFT JOIN area_atividade ON menu.id = area_atividade.collection
                        WHERE menu.posicao = 'top' AND menu.visivel = 1 AND menu.menu_main IS NULL
                        ORDER BY menu.ordem")->queryAll();

                        $titulo = "titulo_" . Yii::app()->language;
                        $slug = "slug_" . Yii::app()->language;
                        $menuName = "menu_titulo_" . Yii::app()->language;
                        $PageName = "pagina_titulo_" . Yii::app()->language;
                        $AreaName = "area_atividade_nome_"  . Yii::app()->language;
                        $AreaSlug = "area_atividade_slug_" .Yii::app()->language;
                        $PageSlug = "pagina_slug_" . Yii::app()->language;

                        $flag = 0;

                        foreach ($menu as $m) {

                        $titulo_site = explode(" - ", $this->pageTitle);
                        if (sizeof($titulo_site) > 1) {

                        if (mb_strtoupper($titulo_site[1], "UTF-8") === mb_strtoupper($m->$titulo, "UTF-8"))
                        $class = "menu_selected";
                        else
                        $class = "";
                        } else {
                        $class = "";
                        }

                        if($m[$PageName] != null){
                        echo '<li><a class="link ' . $class . '" href="' . Yii::app()->baseUrl . '/pagina/' . $m[$PageSlug] . '">' . mb_strtoupper($m[$PageName], "UTF-8") . '</a></li>';
                        }elseif($m['menuID'] == 2){
                        if($flag == 1){
                        echo '<li>' . CHtml::link(mb_strtoupper($m[$AreaName], "UTF-8"), Yii::app()->baseUrl . "/AreaAtividade/" . $m[$AreaSlug], array('style' => 'color:#6E6E6E')) . '</li>';
                        }else{
                        $flag = 1;
                        echo '<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">' . mb_strtoupper($m[$menuName], "UTF-8") . ' <b class="caret"></b></a>
                            <ul class="dropdown-menu" style="width:100%;">';

                                }
                                }elseif($flag == 1 && $m['menuID'] != 2) {
                                $flag = 0;
                                echo '</ul>
                        </li>';
                        }else{
                        echo '<li><a class="link ' . $class . '" href="' . Yii::app()->baseUrl . '/' . $m['link'] . '">' . mb_strtoupper($m[$menuName], "UTF-8") . '</a></li>';
                        }
                        }
                    ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container-fluid nopadding hidden-xs">
    <div class="col-sx-12">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <br class="hidden-md">
                    <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl; ?>/index.php"><img
                            src="<?php echo Yii::app()->baseUrl . '/images/logo_' . Yii::app()->language . '.png' ?>"
                            width="280px"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right" id="nav_principal">
                        <?php






                        $flag = 0;

                        foreach ($menu as $m) {

                             $titulo_site = explode(" - ", $this->pageTitle);
                            if (sizeof($titulo_site) > 1) {

                                if (mb_strtoupper($titulo_site[1], "UTF-8") === mb_strtoupper($m->$titulo, "UTF-8"))
                                    $class = "menu_selected";
                                else
                                    $class = "";
                            } else {
                                $class = "";
                            }

                            if($m[$PageName] != null){
                                echo '<li><a class="link ' . $class . '" href="' . Yii::app()->baseUrl . '/pagina/' . $m[$PageSlug] . '">' . mb_strtoupper($m[$PageName], "UTF-8") . '</a></li>';
                            }elseif($m['menuID'] == 2){
                                if($flag == 1){
                                    echo '<li>' . CHtml::link(mb_strtoupper($m[$AreaName], "UTF-8"), Yii::app()->baseUrl . "/AreaAtividade/" . $m[$AreaSlug], array('style' => 'color:#6E6E6E')) . '</li>';
                                }else{
                                    $flag = 1;
                                    echo '<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">' . mb_strtoupper($m[$menuName], "UTF-8") . ' <b class="caret"></b></a>
                                    <ul class="dropdown-menu" style="width:100%;">';

                                }
                            }elseif($flag == 1 && $m['menuID'] != 2) {
                                $flag = 0;
                                echo '</ul>
                                  </li>';
                            }else{
                                echo '<li><a class="link ' . $class . '" href="' . Yii::app()->baseUrl . '/' . $m['link'] . '">' . mb_strtoupper($m[$menuName], "UTF-8") . '</a></li>';
                            }
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-5 visible-xs">
            <img class="logo" src="<?php echo Yii::app()->baseUrl . '/images/logo_' . Yii::app()->language . '.png' ?>"
                 width="100%">
        </div>
    </div>
</div>

<br>

<div class="container hidden-xs">
    <div class="col-md-offset-10 col-md-2" id="fader">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/atencao.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/confianca.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/excelencia.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/experiencia.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/inovacao.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/integridade.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/qualidade.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/rigor.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/servico.png'; ?>">
        <img class="dedicacao"
             src="<?php echo Yii::app()->baseUrl . '/images/clip/' . Yii::app()->language . '/tecnologia.png'; ?>">
    </div>
</div>

<?php echo $content; ?>
<div class="container-fluid foot">
    <div class="row">
        <div class="col-md-offset-1 col-md-8">
            <p>AGRICORTES SA - <?php echo date("Y"); ?>
                - <?php Content::model()->getContentEcho('direitos', Yii::app()->language); ?></p>
        </div>
        <div class="col-md-3">
            <p><a href="http://www.virtualnet.pt" target="_blank">Virtualnet</a>&nbsp;made it</p>
        </div>
    </div>
</div>
<!-- JavaScript -->
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.prettyPhoto.js" type="text/javascript"
        charset="utf-8"></script>
<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });

</script>

<script type="text/javascript">


    $(function () {
        $('.link').click(function () {
            $(this).addClass("menu_selected");
            //$('#link').css('backgroundImage', 'url(images/tabs3.png)');

        });
    })

    $(document).ready(function () {
        $("a[rel^='prettyPhoto']").prettyPhoto();

        <?php
        if(!isset($_SESSION['slider'])){
            echo"$('.modal').modal('show');";
            echo"if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {";
            echo "window.location.replace('http://agricortes.com/pagina/contactos');";
            echo"}";

            $_SESSION['slider']=true;
        }
        ?>

        $(function () {
            $('#fader img:not(:first)').hide();
            $('#fader img').css('position', 'absolute');
            $('#fader img').css('top', '-18px');
            $('#fader img').css('left', '50%');
            $('#fader img').each(function () {
                var img = $(this);
                $('<img>').attr('src', $(this).attr('src')).load(function () {
                    img.css('margin-left', -this.width / 2 + 'px');
                });
            });

            var pause = false;

            function fadeNext() {
                $('#fader img').first().fadeOut().appendTo($('#fader'));
                $('#fader img').first().fadeIn(200);
            }

            function fadePrev() {
                $('#fader img').first().fadeOut();
                $('#fader img').last().prependTo($('#fader')).fadeIn(200);
            }

            $('#fader, #next').click(function () {
                fadeNext();
            });

            $('#prev').click(function () {
                fadePrev();
            });

            $('#fader, .button').hover(function () {
                pause = true;
            }, function () {
                pause = false;
            });

            function doRotate() {
                if (!pause) {
                    fadeNext();
                }
            }

            var rotate = setInterval(doRotate, 3000);
        });


        $('.link_cat').click(function () {
            var currentId = $(this).attr('id');
            var currentDiv = "#div" + currentId;
            $('.divlink').css("display", "none");

            $(currentDiv).css("display", "inline");
            return false;
        });
        /*
        <?php if (Yii::app()->controller->id==="categoria"): ?>
         $('html,body').animate({ scrollTop: $('.zona_categorias').offset().top }, 'slow');
        <?php endif; ?>

        <?php if (Yii::app()->controller->id==="areaatividade"): ?>
         $('html,body').animate({ scrollTop: $('.dedicacao').offset().top }, 'slow');
        <?php endif; ?>

    });

</script>


<!--div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://agricortes.com/livezilla/server.php?a=08124&request=track&output=jcrpt&fbpos=02&fbml=0&fbmt=0&fbmr=0&fbmb=0&fbw=302&fbh=320&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><h1><img src="http://agricortes.com/livezilla/server.php?a=08124&amp;request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></h1></noscript><!-- http://www.LiveZilla.net Tracking Code -->
<!--<div style="display:none;"><a href="javascript:void(window.open('http://agricortes.com/livezilla/chat.php?a=cce16','','width=590,height=760,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))" class="lz_fl"><img id="chat_button_image" src="http://agricortes.com/livezilla/image.php?a=74c73&amp;id=2&amp;type=overlay" style="border:0px;" alt="LiveZilla Live Chat Software"></a></div><!-- http://www.LiveZilla.net Chat Button Link Code -->
</body>

</html>
