<?php

class DownloadController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('download'),
'users'=>array('*'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update','view','updateajax','download'),
'roles'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Download;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Download']))
{
$model->attributes=$_POST['Download'];

$rnd = md5(time());

$uploadedFile=CUploadedFile::getInstance($model,'src');

if(!empty($uploadedFile))
{
    $fileName = "{$rnd}.{$uploadedFile->extensionName}";
    $fileBaseName = basename($uploadedFile,'.'.$uploadedFile->extensionName);
    $model->src = $fileName;
    //$model->titulo_pt=$fileBaseName;
    //$model->titulo_es=$fileBaseName;
}

if($model->save())
{
    if(!empty($uploadedFile)){
        $uploadedFile->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$fileName);
    }

    //Download::model()->lerFicheiros();

    $this->redirect(array('admin'));
}
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);
$imagem_antiga=$model->src;

if(isset($_POST['Download']))
{
    $rnd = md5(time());
    $model->attributes=$_POST['Download'];
    $uploadedFile=CUploadedFile::getInstance($model,'src');
    if(!empty($uploadedFile))
        {
            $fileName = "{$rnd}.{$uploadedFile->extensionName}";
            $fileBaseName = basename($uploadedFile,'.'.$uploadedFile->extensionName);
            $model->src = $fileName;
            //$model->titulo_pt=$fileBaseName;

            $old_file=Download::model()->findByPk($id)->src;
            if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
                unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file);
        }
        elseif($imagem_antiga!="" && $imagem_antiga!=null)
         $model->src=$imagem_antiga;

if($model->save()){
    if(!empty($uploadedFile))
    {
        $uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/'.$model->src);

    }

    $this->redirect(array('admin'));
}
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Download');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Download('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Download']))
$model->attributes=$_GET['Download'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Download::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='download-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

public function actionUpdateAjax($id)
{
    $model=Download::model()->findByPk($id);
    $fileName=$model->src;
    $model->src="";
    $model->save(false);
    if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
        unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);

    $this->renderPartial('_imagemContent', array('model'=>$model));
}

public function actionDownload($id)
        {
            $model=Download::model()->findByPk($id);
            $path = Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/uploads/' . $model->src;
            $extension=explode(".",$model->src);
            return Yii::app()->getRequest()->sendFile($model->titulo_pt .".". $extension[1] , Yii::app()->curl->get($path));
        }


}
