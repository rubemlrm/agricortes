<?php

class AreaAtividadeController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
    
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update','updateajax','updateajax2'),
'roles'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

public function actionView($id,$maquina=null)
{
$this->layout="//layouts/column1";
$model=AreaAtividade::model()->findByPk($id);

$model2=new ContactForm;
if(isset($_POST['ContactForm']))
{

        $model2->attributes=$_POST['ContactForm'];
        if($model2->validate())
        {
                $pedido=new Pedido;
                $pedido->attributes=$model2->attributes;
                $pedido->data=date("Y-m-d H:i:s");
                $pedido->nome=$model2->name;
                $pedido->cod_postal=$model2->codpostal;
                $pedido->mensagem=$model2->body;
                $pedido->save(false);
                
                $assunto=$model2['assunto'];
		
                $subject='Máq. Equip. '.$assunto;
                $mail = new YiiMailer();
                $mail->setFrom($model2->email, $model2->name);
                
                $mail->setTo(Yii::app()->params['adminEmail']);
                //$mail->setTo("rubem@virtualnet.pt");
                $mail->setSubject($subject);
                
                $mensagem="Nome: ".$model2->name."<br>";
                $mensagem.="Empresa: ".$model2->empresa."<br>";
                $mensagem.="Telemóvel: ".$model2->telemovel."<br>";
                $mensagem.="E-mail: ".$model2->email."<br>";
                $mensagem.="Código Postal: ".$model2->codpostal."<br>";
                $mensagem.="Mensagem: ".$model2->body."<br>";
                $mail->setBody($mensagem);
                $mail->send();
                Yii::app()->user->setFlash('contact','Obrigado por nos contactar. Entraremos em contacto consigo o mais breve possível.');
                $this->refresh();
        }
}

if($model===null){    
    $slug="slug_".Yii::app()->language;
    $model=AreaAtividade::model()->findByAttributes(array($slug=>$id));    
}

if($model===null)
    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

$areas_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));

$Criteria = new CDbCriteria();
$Criteria->condition = "area_atividade_id = ".$model->id." AND categoria_main IS NULL";
//$Criteria->condition = "categoria_main IS NULL";
$Criteria->order="ordem ASC";
$categorias = Categoria::model()->findAll($Criteria);


$this->render('view',array(
'model'=>$model,'areas_atividade'=>$areas_atividade,'categorias'=>$categorias,'model2'=>$model2
));

}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new AreaAtividade;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['AreaAtividade']))
{
$model->attributes=$_POST['AreaAtividade'];
$model->slug_pt=Pagina::model()->toAscii($model->nome_pt);
$model->slug_es=Pagina::model()->toAscii($model->nome_es);
$model->slug_en=Pagina::model()->toAscii($model->nome_en);

$images = CUploadedFile::getInstancesByName('images');
if (isset($images) && count($images) > 0) {
 
                // go through each uploaded image
                foreach ($images as $image => $pic) {
                    $name=explode(".",$pic->name);
                    $name=md5(time()+rand(0,1000)).".".$name[1];
                    if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$name)) {
                        // add it to the main model now
                        $img_add = new AreaAtividadeImg();
                        $img_add->imagem = $name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                        $img_add->area_atividade_id = $model->id;
                        AreaAtividadeImg::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$name);
                        $img_add->save(); // DONE
                    }
                    //else
                        // handle the errors here, if you want
                }
 
                // save the rest of your information from the form
                if ($model->save()) {
                    $this->redirect(array('view','id'=>$model->id));
                }
            }
        }

/*$rnd = md5(time());
$uploadedFile=CUploadedFile::getInstance($model,'imagem_small');
if(!empty($uploadedFile))
{
    $fileName = "s{$rnd}.{$uploadedFile->extensionName}";
    $model->imagem_small = $fileName;
}
$rnd = md5(time());
$uploadedFile2=CUploadedFile::getInstance($model,'imagem_big');
if(!empty($uploadedFile2))
{
    $fileName2 = "b{$rnd}.{$uploadedFile2->extensionName}";
    $model->imagem_big = $fileName2;
}*/

/*if($model->save()){
    if(!empty($uploadedFile)){
        $uploadedFile->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$fileName);
        AreaAtividade::model()->createThumbnailsSmall(Yii::getPathOfAlias('webroot').'/uploads/',$fileName);
    }
    if(!empty($uploadedFile2)){
        $uploadedFile2->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$fileName2);
        AreaAtividade::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$fileName);
    }
    $this->redirect(array('admin'));
}*/


$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);
$imagem_small_old=$model->imagem_small;
$imagem_big_old=$model->imagem_big;
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['AreaAtividade']))
{
    $model->attributes=$_POST['AreaAtividade'];
    $model->slug_pt=Pagina::model()->toAscii($model->nome_pt);
    $model->slug_es=Pagina::model()->toAscii($model->nome_es);
    $model->slug_en=Pagina::model()->toAscii($model->nome_en);

    $images = CUploadedFile::getInstancesByName('images');
   
    if (isset($images) && count($images) > 0) {

                    // go through each uploaded image
                    foreach ($images as $image => $pic) {
                        
                        $name=explode(".",$pic->name);
                        $name=md5(time()+rand(0,1000)).".".$name[1];
                        if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$name)) {
                            // add it to the main model now
                            $img_add = new AreaAtividadeImg();
                            $img_add->imagem = $name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                            $img_add->area_atividade_id = $model->id; // this links your picture model to the main model (like your user, or profile model)
                            AreaAtividadeImg::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$name);
                            $img_add->save(); // DONE
                        }
                        //else
                            // handle the errors here, if you want
                    }

                    
                }
            // save the rest of your information from the form
            if ($model->save()) {
                $this->redirect(array('admin'));
            }
    
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
    
$old_file=AreaAtividade::model()->findByPk($id)->imagem_small;
if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
    unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file);   

$old_file=AreaAtividade::model()->findByPk($id)->imagem_big;
if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
    unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file); 
    
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$this->layout='//layouts/column1';    
    
$dataProvider=new CActiveDataProvider('AreaAtividade');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new AreaAtividade('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['AreaAtividade']))
$model->attributes=$_GET['AreaAtividade'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=AreaAtividade::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

public function actionUpdateAjax($id)
    {
        $model=AreaAtividade::model()->findByPk($id);
        $fileName=$model->imagem_small;
        $model->imagem_small="";
        $model->save(false);
        if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
            unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);

        $this->renderPartial('_imagemContent', array('model'=>$model));
    } 
    
public function actionUpdateAjax2($id,$model_id)
    {
        $model=AreaAtividadeImg::model()->findByPk($id);
        $fileName=$model->imagem;
        
        $model->delete();
        if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
            unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);
       
        $imagens=AreaAtividadeImg::model()->findAllByAttributes(array('area_atividade_id'=>$model_id));
        $this->renderPartial('_imagemContent2', array('model'=>$model,'imagens'=>$imagens));
    }     

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='area-atividade-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
