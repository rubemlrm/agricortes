<?php

/**
 * This is the model class for table "content".
 *
 * The followings are the available columns in table 'content':
 * @property integer $id
 * @property string $slug
 * @property string $value_pt
 * @property string $value_en
 */
class Content extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug, value_pt', 'required'),
			array('slug', 'length', 'max'=>50),
			array('value_pt, value_en, value_fr, value_es', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, slug, value_pt, value_en, value_fr, value_es', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slug' => '#',
			'value_pt' => 'Valor PT',
			'value_en' => 'Valor EN',
                        'value_fr' => 'Valor FR',
                        'value_es' => 'Valor ES',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('value_pt',$this->value_pt,true);
		$criteria->compare('value_en',$this->value_en,true);
                $criteria->compare('value_fr',$this->value_fr,true);
                $criteria->compare('value_es',$this->value_es,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getContentEcho($slug,$lang){
            $content=Content::model()->findByAttributes(array('slug'=>$slug));
            switch($lang)
            {
                case 'pt':{echo $content->value_pt;break;}
                case 'en':{echo $content->value_en;break;}
                case 'fr':{echo $content->value_fr;break;}
                case 'es':{echo $content->value_es;break;}
            }
            
        }
        public function getContentReturn($slug,$lang){
            $content=Content::model()->findByAttributes(array('slug'=>$slug));
            switch($lang)
            {
                case 'pt':{return $content->value_pt;break;}
                case 'en':{return $content->value_en;break;}
                case 'fr':{return $content->value_fr;break;}
                case 'es':{return $content->value_es;break;}
            }
        }
}