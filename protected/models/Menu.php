<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property integer $id
 * @property string $titulo_pt
 * @property string $titulo_en
 * @property string $titulo_fr
 * @property string $titulo_es
 * @property string $posicao
 * @property string $link
 * @property integer $pagina_id
 * @property integer $menu_main
 * @property integer $ordem
 * @property integer $visivel
 */
class Menu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo_pt, posicao, target, imagem', 'required'),
			array('pagina_id, menu_main, ordem, visivel', 'numerical', 'integerOnly'=>true),
			array('titulo_pt, titulo_en, titulo_fr, titulo_es, posicao', 'length', 'max'=>50),
			array('link', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, titulo_pt, titulo_en, titulo_fr, titulo_es, posicao, link, pagina_id, menu_main, ordem, visivel', 'safe', 'on'=>'search'),
                        array('link','checkLinkPagina'),
                    );
	}
        
        public function checkLinkPagina($attributes,$params)
        {
          if($this->link!="" && $this->link!=null && $this->pagina_id!="" && $this->pagina_id!=""){
                 $this->addError('link','');
                 $this->addError('pagina_id','Não é possível preencher o link e a página em simultâneo.');
          }  
        }
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo_pt' => 'Título PT',
			'titulo_en' => 'Título EN',
			'titulo_fr' => 'Título FR',
			'titulo_es' => 'Título ES',
			'posicao' => 'Posição',
			'link' => 'Link',
			'pagina_id' => 'Página',
			'menu_main' => 'Menu Principal',
			'ordem' => 'Ordem',
			'visivel' => 'Visível',
                        'target'=>'Target',
                        'imagem'=>'Imagem Banner (1200x275)'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo_pt',$this->titulo_pt,true);
		$criteria->compare('titulo_en',$this->titulo_en,true);
		$criteria->compare('titulo_fr',$this->titulo_fr,true);
		$criteria->compare('titulo_es',$this->titulo_es,true);
		$criteria->compare('posicao',$this->posicao,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('pagina_id',$this->pagina_id);
		$criteria->compare('menu_main',$this->menu_main);
		$criteria->compare('ordem',$this->ordem);
		$criteria->compare('visivel',$this->visivel);
                $criteria->compare('target',$this->target);
                $criteria->compare('imagem',$this->imagem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getAllName()
        {
            
            $todos=$this->findAll();
            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->titulo_pt;
            }
            return $arr;
        }
        
        public function getById($id)
        {
            $model=$this->findByPk($id);   
            if($model!=null)
                return $model->titulo_pt;
            else
                return "N/A";
        }
        
        public function getVisivel($id)
        {
            $model=$this->findByPk($id);   
            if($model)
                return "Sim";
            else
                return "Não";
        }
        
        public function getAllNameExclude($menu_id)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id!=:id';
            $criteria->params = array(':id'=>$menu_id);
            $todos = $this->findAll($criteria);

            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->titulo_pt;
            }
            return $arr;
        }
        
        public function createThumbnails($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 1200, 275, 100 );          
        }
}