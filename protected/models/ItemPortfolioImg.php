<?php

/**
 * This is the model class for table "item_portfolio_img".
 *
 * The followings are the available columns in table 'item_portfolio_img':
 * @property integer $id
 * @property integer $id_item_portfolio
 * @property string $imagem
 */
class ItemPortfolioImg extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPortfolioImg the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_portfolio_img';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item_portfolio, imagem', 'required'),
			array('id_item_portfolio', 'numerical', 'integerOnly'=>true),
			array('imagem, nome_pt, nome_es, nome_en', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_item_portfolio, imagem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'Maquina' => array(self::BELONGS_TO, 'Maquina', 'id_item_portfolio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_item_portfolio' => 'Máquina',
			'imagem' => 'Imagem',
                        'nome_pt' => 'Nome PT',
                        'nome_es' => 'Nome ES',
                        'nome_en' => 'Nome EN',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('Maquina');

		$criteria->compare('id',$this->id);
		$criteria->compare('Maquina.titulo_pt',$this->id_item_portfolio,true);
		$criteria->compare('imagem',$this->imagem,true);
                $criteria->compare('nome_pt',$this->nome_pt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function getByItem($id_item_portfolio)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = "id_item_portfolio =:id_item_portfolio";
            $criteria->params = array(':id_item_portfolio' => $id_item_portfolio);
            $imagens = $this::model()->findAll($criteria);
            
            return $imagens;
        }
}