<?php

/**
 * This is the model class for table "pedido".
 *
 * The followings are the available columns in table 'pedido':
 * @property integer $id
 * @property string $nome
 * @property string $empresa
 * @property string $telemovel
 * @property string $cod_postal
 * @property string $mensagem
 * @property string $assunto
 * @property string $email
 * @property string $data
 */
class Pedido extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pedido the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pedido';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, empresa, telemovel, cod_postal, mensagem, assunto, email, data', 'required'),
			array('nome, empresa', 'length', 'max'=>100),
			array('telemovel, cod_postal', 'length', 'max'=>50),
			array('assunto, email', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome, empresa, telemovel, cod_postal, mensagem, assunto, email, data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'empresa' => 'Empresa',
			'telemovel' => 'Telemóvel',
			'cod_postal' => 'Cód Postal',
			'mensagem' => 'Mensagem',
			'assunto' => 'Assunto',
			'email' => 'E-mail',
			'data' => 'Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('empresa',$this->empresa,true);
		$criteria->compare('telemovel',$this->telemovel,true);
		$criteria->compare('cod_postal',$this->cod_postal,true);
		$criteria->compare('mensagem',$this->mensagem,true);
		$criteria->compare('assunto',$this->assunto,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('data',$this->data,true);
                
                $criteria->order="data DESC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}