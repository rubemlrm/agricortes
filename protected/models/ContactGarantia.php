<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactGarantia extends CFormModel
{
	public $modelo;
	public $num_serie;
        public $num_motor;
        public $matricula;
        public $empresa;
	public $nome;
	public $morada;
        public $cod_postal;
        public $localidade;
        public $contacto;
        public $email;
        public $contribuinte;
        public $data_compra;
        public $data_servico;
        public $concessionario;
        public $check;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('modelo, num_serie, nome, contacto, morada, localidade, cod_postal, contribuinte, data_compra', 'required'),
                        array('num_motor, matricula, empresa, email, data_servico, concessionario', 'safe'),
			// email has to be a valid email address
			array('email', 'email'),
                        array('check','checkAuth'),
		);
	}
        
        public function checkAuth($attributes,$params)
        {
          if($this->check==0){
                 $this->addError('check','Antes de avançar deverá autorizar a recolha de dados.');
          }  
        }

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'modelo'=>'Modelo',
                        'email'=>'E-mail',
                        'num_serie'=>'Nº Série',
                        'num_motor'=>'Nº Motor',
                        'matricula'=>'Matricula',
                        'empresa'=>'Empresa',
                        'nome'=>'Nome',
                        'morada'=>'Morada',
                        'cod_postal'=>'Cód.Postal',
                        'localidade'=>'Localidade',
                        'contacto'=>'Contacto',
                        'contribuinte'=>'Nº Contribuinte',
                        'morada'=>'Morada',
                        'data_compra'=>'Data compra',
                        'data_servico'=>'Data de entrada em serviço',
                        'concessionario'=>'Concessionário',
                        'check'=>'',
		);
	}
}