<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $nome
 * @property string $last_login_time
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 * @property string $roles
 */
class User extends MyActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
        public $password_repeat;
        public $old_password;
         
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('email','email'),
                        array('password','compare','on'=>'insert'),
                        array('password_repeat, phone, country','safe'),
                        array('email, username','unique'),
			array('username, email, firstname, lastname, roles, password', 'required'),
                        
                        array('old_password', 'required','on'=>'change_pass'),
                        array('password, password_repeat', 'required','on'=>'create'),
                        //array('password', 'compare', 'compareAttribute'=>'password_repeat','on'=>'create'),
                       
			array('username, email, password, firstname', 'length', 'max'=>100),
			array('roles', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, email, password, firstname, last_login_time, create_time, create_user_id, update_time, update_user_id, roles', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'E-mail',
			'password' => 'Password',
                        'password_repeat' => 'Repita a password',
                        'old_password' => 'Password antiga',
			'firstname' => 'Primeiro Nome',
                        'lastname' => 'Último Nome',
                        'phone'=>'Telefone',
                        'country'=>'País',
			'last_login_time' => 'Último login',
			'create_time' => 'Data de registo',
			'create_user_id' => 'Registado por',
			'update_time' => 'Editado em',
			'update_user_id' => 'Editado por',
			'roles' => 'Tipo de utilizador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('firstname',$this->firstname,true);
                $criteria->compare('lastname',$this->lastname,true);
                $criteria->compare('phone',$this->phone,true);
                $criteria->compare('country',$this->country,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('roles',$this->roles,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        protected function afterValidate()
        {
            parent::afterValidate();
            //if(!$this->hasErrors() && $this->scenario==="create" )
              //  $this->password = $this->hashPassword($this->password);
        }
        
        public function hashPassword($password)
        {
            //return md5($password);
        }
        
        public function validatePassword($password)
        {
            //return $this->hashPassword($password)===$this->password;
            return $password===$this->password;
        }
}