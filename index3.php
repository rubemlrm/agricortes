<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Agricortes S.A.</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="VirtualNet">
  <link rel="stylesheet" href="css.css">
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>

  <div class="container clearfix">
    <div class="span1">
      <a id="logo" href="/portal"><img src="imgs/header-logo.jpg" alt="Agricortes" style="border:0"></a>
      <ul id="menu">
        <li><a href="#" class="menu menu-op1">Quem somos</a></li>
        <li><a href="#" class="menu menu-op2">Empresas Associadas</a></li>
        <li><a href="#" class="menu menu-op3">História</a></li>
        <li><a href="#" class="menu menu-op4">Serviços</a></li>
        <li><a href="#" class="menu menu-op5">Marcas</a></li>
        <li><a href="#" class="menu menu-op6">Agricortes Club</a></li>
        <li><a href="#" class="menu menu-op7">Contactos</a></li>
        <li><a href="#" class="menu menu-op8">Downloads</a></li>
        <li><a href="#" class="menu menu-op9">Eventos / Feiras</a></li>
      </ul>
      <div id="langs">
        <img src="imgs/langs.jpg" alt="" usemap="#langs-map">
      </div>
    </div>
    <div class="span2">
      <div id="banner">
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" height="52" width="464">
          <param name="movie" value="/portal/images/banner.swf">
          <param name="quality" value="best"> 
          <param name="play" value="true">
          <embed height="55" pluginspage="http://www.macromedia.com/go/getflashplayer" src="/portal/images/banner.swf" type="application/x-shockwave-flash" width="464" quality="best" play="true">    
        </object>
      </div>

      <div id="social">
        <img src="imgs/btns-social.gif" alt="" usemap="#social-map">
      </div>

      <div id="catalog">
        <a href="/index.php?id=1087"><img src="imgs/btn-catalogo.gif" alt="Catálogo Agricortes"></a>
      </div>

      <div id="iframe">
        <iframe src="/portal/index.php?id=1103&layout=detail&mode=" frameborder="0" width="574" height="299"></iframe>
      </div>

    </div>
  </div>

  <map name="social-map">
    <area shape="rect" coords="0,0,80,34" href="http://www.facebook.com/profile.php?id=100003475069977&sk=wall" target="_blank" />
    <area shape="rect" coords="93,0,156,34" href="https://twitter.com/AGRICORTES" target="_blank" />
    <area shape="rect" coords="171,0,228,34" href="skype:agricortes?call" />
    <area shape="rect" coords="242,0,313,34" href="http://www.youtube.com/user/AGRICORTES" target="_blank" />
    <area shape="rect" coords="326,0,499,34" href="mailto:agricortes@agricortes.com" />
  </map>
  <map name="langs-map">
    <area shape="rect" coords="29,5,70,76" href="http://agricortes.pt/portal/index.php?lang=ES" />
    <area shape="rect" coords="73,5,112,36" href="http://agricortes.pt/portal/index.php?lang=EN" />
    <area shape="rect" coords="22,36,89,51" href="/portal" alt="">
  </map>

</body>
</html>
