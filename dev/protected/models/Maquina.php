<?php

/**
 * This is the model class for table "item_portfolio".
 *
 * The followings are the available columns in table 'item_portfolio':
 * @property integer $id
 * @property integer $categoria_id
 * @property string $titulo_pt
 * @property string $titulo_en
 * @property string $subtitulo_pt
 * @property string $subtitulo_en
 * @property string $imagem_capa
 * @property integer $ordem
 */
class Maquina extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPortfolio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_portfolio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('imagem_capa', 'file', 'types'=>'jpg,JPG','allowEmpty'=>true,'maxSize'=>5242880,'wrongType'=>'Apenas imagens são permitidas imagens .jpg','tooLarge'=>'Ficheiro demasiado grande! 5MB é o limite'),
                        //array('catalogo', 'file', 'types'=>'pdf,PDF','allowEmpty'=>true,'maxSize'=>5242880,'wrongType'=>'Apenas imagens são permitidas imagens .pdf','tooLarge'=>'Ficheiro demasiado grande! 5MB é o limite'),
			array('categoria_id, titulo_pt, destaque', 'required'),
			array('categoria_id, ordem', 'numerical', 'integerOnly'=>true),
			array('titulo_pt, titulo_en, titulo_fr, titulo_es, subtitulo_pt, subtitulo_en, subtitulo_fr, subtitulo_es', 'length', 'max'=>100),
			
                        array('descricao_pt, descricao_en, descricao_fr, descricao_es, link_website', 'safe'),
                        array('imagem_capa', 'length', 'max'=>100),
                        array('link_video', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categoria_id, titulo_pt, titulo_en, titulo_fr, titulo_es, subtitulo_pt, subtitulo_en, subtitulo_fr, subtitulo_es, descricao_pt, descricao_en, descricao_fr, descricao_es, imagem_capa, ordem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'Categoria' => array(self::BELONGS_TO, 'Categoria', 'categoria_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'categoria_id' => 'Categoria',
			'titulo_pt' => 'Título PT',
			'titulo_en' => 'Título EN',
                        'titulo_fr' => 'Título FR',
                        'titulo_es' => 'Título ES',
			'subtitulo_pt' => 'Subtítulo PT',
			'subtitulo_en' => 'Subtítulo EN',
                        'subtitulo_fr' => 'Subtítulo FR',
                        'subtitulo_es' => 'Subtítulo ES',
                        'descricao_pt' => 'Descrição PT',
                        'descricao_en' => 'Descrição EN',
                        'descricao_fr' => 'Descrição FR',
                        'descricao_es' => 'Descrição ES',
			'imagem_capa' => 'Imagem Logotipo (300x150)',
			'ordem' => 'Ordem',
                        'link_video' => 'Link Vídeo (usar # para separar vídeos)',
                        'destaque' => 'Destaque',
                        'file'=>'Catálogos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('Categoria');

		$criteria->compare('id',$this->id);
		$criteria->compare('Categoria.nome_pt',$this->categoria_id,true);
		$criteria->compare('titulo_pt',$this->titulo_pt,true);
		$criteria->compare('titulo_en',$this->titulo_en,true);
                $criteria->compare('titulo_fr',$this->titulo_fr,true);
                $criteria->compare('titulo_es',$this->titulo_es,true);
		$criteria->compare('subtitulo_pt',$this->subtitulo_pt,true);
		$criteria->compare('subtitulo_en',$this->subtitulo_en,true);
                $criteria->compare('subtitulo_fr',$this->subtitulo_fr,true);
                $criteria->compare('subtitulo_es',$this->subtitulo_es,true);
                $criteria->compare('descricao_pt',$this->descricao_pt,true);
                $criteria->compare('descricao_en',$this->descricao_en,true);
                $criteria->compare('descricao_fr',$this->descricao_fr,true);
                $criteria->compare('descricao_es',$this->descricao_es,true);
		$criteria->compare('imagem_capa',$this->imagem_capa,true);
		$criteria->compare('ordem',$this->ordem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'defaultOrder'=>'Categoria.nome_pt ASC, t.titulo_pt ASC',
                          )
		));
	}
        
        public function createThumbnails($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 300, 150, 100 );          
        }
        
        public function createThumbnailsGallery($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 800, 600, 100 );          
        }
        
        public function getAllName()
        {
            
            $todos=$this->findAll();
            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->titulo_pt;
            }
            return $arr;
        }

public function afterSave( ) {
    $this->addImages( );
    parent::afterSave( );
}        
        
public function addImages( ) {
    //If we have pending images
    if( Yii::app( )->user->hasState( 'images' ) ) {
        $userImages = Yii::app( )->user->getState( 'images' );
        //Resolve the final path for our images
        //$path = Yii::app( )->getBasePath( )."/../uploads/{$this->id}/";
        $path = Yii::app( )->getBasePath( )."/../uploads/";
        //Create the folder and give permissions if it doesnt exists
        if( !is_dir( $path ) ) {
            mkdir( $path );
            chmod( $path, 0777 );
        }
 
        //Now lets create the corresponding models and move the files
        foreach( $userImages as $image ) {
            if( is_file( $image["path"] ) ) {
                if( rename( $image["path"], $path.$image["filename"] ) ) {
                    chmod( $path.$image["filename"], 0777 );
                    $img = new ItemPortfolioImg( );
                    //$img->size = $image["size"];
                    //$img->mime = $image["mime"];
                    $img->imagem = $image["filename"];
                    Maquina::model()->createThumbnailsGallery(Yii::getPathOfAlias('webroot').'/uploads/',$img->imagem);
                    //$img->source = "/uploads/{$this->id}/".$image["filename"];
                    $img->id_item_portfolio = $this->id;
                    if( !$img->save( ) ) {
                        //Its always good to log something
                        Yii::log( "Could not save Image:\n".CVarDumper::dumpAsString( 
                            $img->getErrors( ) ), CLogger::LEVEL_ERROR );
                        //this exception will rollback the transaction
                        throw new Exception( 'Could not save Image');
                    }
                }
            } else {
                //You can also throw an execption here to rollback the transaction
                Yii::log( $image["path"]." is not a file", CLogger::LEVEL_WARNING );
            }
        }
        //Clear the user's session
        Yii::app( )->user->setState( 'images', null );
    }
}
}