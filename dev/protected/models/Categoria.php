<?php

/**
 * This is the model class for table "categoria".
 *
 * The followings are the available columns in table 'categoria':
 * @property integer $id
 * @property string $nome_pt
 * @property string $nome_en
 * @property integer $ordem
 */
class Categoria extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Categoria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categoria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_pt, area_atividade_id', 'required'),
                        array('slug_pt, slug_en, slug_es', 'safe'),
			array('ordem, categoria_main', 'numerical', 'integerOnly'=>true),
			array('nome_pt, nome_en, nome_fr, nome_es', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome_pt, nome_en, nome_fr, nome_es, ordem, categoria_main', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'Categoria2' => array(self::BELONGS_TO, 'Categoria', 'categoria_main'),
                    'Area' => array(self::BELONGS_TO, 'AreaAtividade', 'area_atividade_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_pt' => 'Nome PT',
			'nome_en' => 'Nome EN',
                        'nome_fr' => 'Nome FR',
                        'nome_es' => 'Nome ES',
                        'categoria_main'=>'Categoria Principal',
			'ordem' => 'Ordem',
                        'area_atividade_id'=>'Área de Atividade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('Categoria2','Area');
                
                
		$criteria->compare('id',$this->id);
                $criteria->compare('t.nome_pt',$this->categoria_main,true);
                $criteria->compare('t.nome_pt',$this->nome_pt,true);
		//$criteria->compare('t.nome_en',$this->nome_en,true);
                //$criteria->compare('t.nome_fr',$this->nome_fr,true);
                //$criteria->compare('t.nome_es',$this->nome_es,true);
		//$criteria->compare('t.ordem',$this->ordem);
                $criteria->compare('Area.nome_pt',$this->area_atividade_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'defaultOrder'=>'Area.nome_pt ASC, Categoria2.nome_pt ASC, t.nome_pt ASC',
                          )
		));
	}
        
        
        public function getAllName()
        {
            $todos=$this->findAll(array('order'=>nome_pt));
            $arr = array();
            foreach($todos as $t)
            {
                if($t->categoria_main==null)
                    $arr[$t->id] = $t->nome_pt;
                else
                {
                    $categoria_main=$this->findByPk($t->categoria_main);
                    $arr[$t->id] = $categoria_main->nome_pt." > ".$t->nome_pt;
                }
            }
            return $arr;
        }
        
        public function getAllNameMain()
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'categoria_main IS NULL';
            $criteria->order='nome_pt';
            $todos = $this->findAll($criteria);
            $arr = array();
            foreach($todos as $t)
            {
                if($t->categoria_main==null)
                    $arr[$t->id] = $t->nome_pt;
                else
                {
                    $categoria_main=$this->findByPk($t->categoria_main);
                    $arr[$t->id] = $categoria_main->nome_pt." > ".$t->nome_pt;
                }
            }
            return $arr;
        }
        
        public function getAllNameNotMain()
        {
            $criteria = new CDbCriteria();
            //$criteria->condition = 'categoria_main IS NOT NULL';
            $criteria->order='nome_pt';
            $todos = $this->findAll($criteria);
            $arr = array();
            foreach($todos as $t)
            {
                if($t->categoria_main==null)
                    $arr[$t->id] = $t->nome_pt;
                else
                {
                    $categoria_main=$this->findByPk($t->categoria_main);
                    $arr[$t->id] = $categoria_main->nome_pt." > ".$t->nome_pt;
                }
            }
            //var_dump($arr);
            asort($arr,2);
            //echo "<br>";
            //var_dump($arr);
            
            //die;
            return $arr;
        }
        
        public function getById($id)
        {
            $model=$this->findByPk($id);   
            if($model!=null)
                return $model->nome_pt;
            else
                return "N/A";
        }
        
        public function getAllNameExclude($categoria_id)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id!=:id AND categoria_main IS NULL';
            $criteria->params = array(':id'=>$categoria_id);
            $todos = $this->findAll($criteria);

            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->nome_pt;
            }
            return $arr;
        }
}