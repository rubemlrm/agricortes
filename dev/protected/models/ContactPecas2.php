<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactPecas2 extends CFormModel
{
	public $unidades;
	public $descricao;
        public $referencia;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('unidades, descricao, referencia', 'required'),
		);
	}
        

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'unidades'=>'Unidades',
                        'descricao'=>'Descrição',
                        'referencia'=>'Referência',
		);
	}
}