<?php

/**
 * This is the model class for table "area_atividade_img".
 *
 * The followings are the available columns in table 'area_atividade_img':
 * @property integer $id
 * @property integer $area_atividade_id
 * @property string $imagem
 */
class AreaAtividadeImg extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AreaAtividadeImg the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'area_atividade_img';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('area_atividade_id, imagem', 'required'),
			array('area_atividade_id', 'numerical', 'integerOnly'=>true),
			array('imagem', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, area_atividade_id, imagem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'area_atividade_id' => 'Area Atividade',
			'imagem' => 'Imagem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('area_atividade_id',$this->area_atividade_id);
		$criteria->compare('imagem',$this->imagem,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function createThumbnails($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 1920, 600, 100 );
           
        }
}