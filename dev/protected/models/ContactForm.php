<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
        public $telemovel;
        public $empresa;
	public $codpostal;
	public $body;
        public $assunto;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, telemovel,codpostal, body, assunto', 'required'),
			// email has to be a valid email address
                        array('email, empresa', 'safe'),
			array('email', 'email'),
			
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name'=>'Nome',
                        'email'=>'E-mail',
                        'telemovel'=>'Telemóvel',
                        'empresa'=>'Empresa',
                        'codpostal'=>'Código Postal',
                        'body'=>'Mensagem',
		);
	}
}