<?php

/**
 * This is the model class for table "item_portfolio_file".
 *
 * The followings are the available columns in table 'item_portfolio_file':
 * @property integer $id
 * @property integer $id_item_portfolio
 * @property string $file
 * @property string $nome_pt
 * @property string $nome_es
 * @property string $nome_en
 */
class ItemPortfolioFile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemPortfolioFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item_portfolio_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_item_portfolio, file', 'required'),
			array('id_item_portfolio', 'numerical', 'integerOnly'=>true),
			array('file, nome_pt, nome_es, nome_en', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_item_portfolio, file, nome_pt, nome_es, nome_en', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_item_portfolio' => 'Id Item Portfolio',
			'file' => 'File',
			'nome_pt' => 'Nome Pt',
			'nome_es' => 'Nome Es',
			'nome_en' => 'Nome En',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_item_portfolio',$this->id_item_portfolio);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('nome_pt',$this->nome_pt,true);
		$criteria->compare('nome_es',$this->nome_es,true);
		$criteria->compare('nome_en',$this->nome_en,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}