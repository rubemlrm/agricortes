<?php

/**
 * This is the model class for table "website".
 *
 * The followings are the available columns in table 'website':
 * @property integer $id
 * @property string $titulo_pt
 * @property string $titulo_en
 * @property string $titulo_fr
 * @property string $titulo_es
 * @property string $description_pt
 * @property string $description_en
 * @property string $description_fr
 * @property string $description_es
 * @property string $keywords_pt
 * @property string $keywords_en
 * @property string $keywords_fr
 * @property string $keywords_es
 * @property string $url
 */
class Website extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Website the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'website';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo_pt, description_pt, keywords_pt, url', 'required'),
			array('titulo_pt, titulo_en, titulo_fr, titulo_es, url', 'length', 'max'=>200),
			array('description_pt, description_en, description_fr, description_es, keywords_pt, keywords_en, keywords_fr, keywords_es', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, titulo_pt, titulo_en, titulo_fr, titulo_es, description_pt, description_en, description_fr, description_es, keywords_pt, keywords_en, keywords_fr, keywords_es, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo_pt' => 'Título PT',
			'titulo_en' => 'Título EN',
			'titulo_fr' => 'Título FR',
			'titulo_es' => 'Título ES',
			'description_pt' => 'Descrição PT',
			'description_en' => 'Descrição EN',
			'description_fr' => 'Descrição FR',
			'description_es' => 'Descrição ES',
			'keywords_pt' => 'Keywords PT',
			'keywords_en' => 'Keywords EN',
			'keywords_fr' => 'Keywords FR',
			'keywords_es' => 'Keywords ES',
			'url' => 'URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo_pt',$this->titulo_pt,true);
		$criteria->compare('titulo_en',$this->titulo_en,true);
		$criteria->compare('titulo_fr',$this->titulo_fr,true);
		$criteria->compare('titulo_es',$this->titulo_es,true);
		$criteria->compare('description_pt',$this->description_pt,true);
		$criteria->compare('description_en',$this->description_en,true);
		$criteria->compare('description_fr',$this->description_fr,true);
		$criteria->compare('description_es',$this->description_es,true);
		$criteria->compare('keywords_pt',$this->keywords_pt,true);
		$criteria->compare('keywords_en',$this->keywords_en,true);
		$criteria->compare('keywords_fr',$this->keywords_fr,true);
		$criteria->compare('keywords_es',$this->keywords_es,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}