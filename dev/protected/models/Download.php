<?php

/**
 * This is the model class for table "download".
 *
 * The followings are the available columns in table 'download':
 * @property integer $id
 * @property string $titulo_pt
 * @property string $titulo_es
 * @property string $src
 * @property string $categoria
 */
class Download extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Download the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'download';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo_pt, src, categoria, atividade_id', 'required'),
			array('titulo_pt, titulo_es, categoria', 'length', 'max'=>100),
			array('src', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, titulo_pt, titulo_es, src, categoria', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'Area' => array(self::BELONGS_TO, 'AreaAtividade', 'atividade_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo_pt' => 'Título PT',
			'titulo_es' => 'Título ES',
			'src' => 'Ficheiro',
			'categoria' => 'Categoria',
                        'atividade_id'=>'Área de atividade'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('Area');

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo_pt',$this->titulo_pt,true);
		$criteria->compare('titulo_es',$this->titulo_es,true);
		$criteria->compare('src',$this->src,true);
		$criteria->compare('categoria',$this->categoria,true);
                $criteria->compare('atividade_id',$this->atividade_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'defaultOrder'=>'Area.nome_pt ASC',
                          )
		));
	}
        
        public function fileType($id)
        {
            $model=$this->findByPk($id);  
            $type=explode(".",$model->src);
            if($type[1]==="pdf" || $type[1]==="htm" || $type[1]==="doc")
                return true;
            else
                return false;
        }
}