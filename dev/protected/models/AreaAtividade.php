<?php

/**
 * This is the model class for table "area_atividade".
 *
 * The followings are the available columns in table 'area_atividade':
 * @property integer $id
 * @property string $nome_pt
 * @property string $nome_en
 * @property string $nome_es
 * @property string $imagem_big
 * @property string $imagem_small
 * @property integer $ordem
 */
class AreaAtividade extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AreaAtividade the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'area_atividade';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_pt, ordem', 'required'),
                        array('slug_pt, slug_en, slug_es', 'safe'),
			array('ordem', 'numerical', 'integerOnly'=>true),
			array('nome_pt, nome_en, nome_es', 'length', 'max'=>100),
			array('imagem_big, imagem_small', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome_pt, nome_en, nome_es, imagem_big, imagem_small, ordem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_pt' => 'Nome PT',
			'nome_en' => 'Nome EN',
			'nome_es' => 'Nome ES',
			'imagem_big' => 'Imagem Grande (1920x600)',
			'imagem_small' => 'Imagem Pequena (600x723)',
			'ordem' => 'Ordem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome_pt',$this->nome_pt,true);
		$criteria->compare('nome_en',$this->nome_en,true);
		$criteria->compare('nome_es',$this->nome_es,true);
		$criteria->compare('imagem_big',$this->imagem_big,true);
		$criteria->compare('imagem_small',$this->imagem_small,true);
		$criteria->compare('ordem',$this->ordem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'defaultOrder'=>'t.nome_pt ASC',
                          )
		));
	}
        
        public function getAllName()
        {
            
            $todos=$this->findAll(array('order'=>nome_pt));
            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->nome_pt;
            }
            return $arr;
        }
        
        public function getById($id)
        {
            $model=$this->findByPk($id);   
            if($model!=null)
                return $model->nome_pt;
            else
                return "N/A";
        }
        
        public function getAllNameExclude($categoria_id)
        {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id!=:id';
            $criteria->params = array(':id'=>$categoria_id);
            $todos = $this->findAll($criteria);

            $arr = array();
            foreach($todos as $t)
            {
                $arr[$t->id] = $t->nome_pt;
            }
            return $arr;
        }
        
        public function createThumbnails($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 1920, 600, 100 );
            
        }
        
        public function createThumbnailsSmall($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 600, 723, 100 );
           
        }
        
        function toAscii($str, $replace=array(), $delimiter='-') {
                if( !empty($replace) ) {
                        $str = str_replace((array)$replace, ' ', $str);
                }

                $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
                $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
                $clean = strtolower(trim($clean, '-'));
                $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

                return $clean;
        }
        
        public function getColor($id){
            
            switch ($id) {
                case 1:
                    return "#FFC900";
                    break;
                case 2:
                    return "#727C86";
                    break;
                case 3:
                    return "#D80D1B";
                    break;
                case 4:
                    return "#008623";
                    break;
                case 5:
                    return "#76CD00";
                    break;
                case 6:
                    return "#008BD0";
                    break;
            }
        }
}