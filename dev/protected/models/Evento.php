<?php

/**
 * This is the model class for table "noticia".
 *
 * The followings are the available columns in table 'noticia':
 * @property integer $id
 * @property string $titulo_pt
 * @property string $titulo_en
 * @property string $titulo_fr
 * @property string $titulo_es
 * @property string $conteudo_pt
 * @property string $conteudo_en
 * @property string $conteudo_fr
 * @property string $conteudo_es
 * @property string $data
 * @property string $imagem
 */
class Evento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Noticia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
       
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'noticia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo_pt, conteudo_pt, data, data_fim', 'required'),
			array('titulo_pt, titulo_en, titulo_fr, titulo_es', 'length', 'max'=>100),
			array('imagem', 'length', 'max'=>45),
			array('conteudo_en, conteudo_fr, conteudo_es, link, imagem', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, titulo_pt, titulo_en, titulo_fr, titulo_es, conteudo_pt, conteudo_en, conteudo_fr, conteudo_es, data, imagem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo_pt' => 'Título PT',
			'titulo_en' => 'Título EN',
			'titulo_fr' => 'Título FR',
			'titulo_es' => 'Título ES',
			'conteudo_pt' => 'Conteúdo PT',
			'conteudo_en' => 'Conteúdo EN',
			'conteudo_fr' => 'Conteúdo FR',
			'conteudo_es' => 'Conteúdo ES',
			'data' => 'Data Início',
                        'data_fim' => 'Data Fim',
			'imagem' => 'Imagem',
                        'link' => 'Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo_pt',$this->titulo_pt,true);
		$criteria->compare('titulo_en',$this->titulo_en,true);
		$criteria->compare('titulo_fr',$this->titulo_fr,true);
		$criteria->compare('titulo_es',$this->titulo_es,true);
		$criteria->compare('conteudo_pt',$this->conteudo_pt,true);
		$criteria->compare('conteudo_en',$this->conteudo_en,true);
		$criteria->compare('conteudo_fr',$this->conteudo_fr,true);
		$criteria->compare('conteudo_es',$this->conteudo_es,true);
		$criteria->compare('data',$this->data,true);
                $criteria->compare('data_fim',$this->data_fim,true);
		$criteria->compare('imagem',$this->imagem,true);
                $criteria->compare('link',$this->imagem,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function createThumbnails($folder,$fileName){
            $cropper=new ImageCropper;
                                   
            $original_image_path=$folder. $fileName;
            list($width, $height) = getimagesize($original_image_path);
            
            $altura=($height*300)/$width;
            
            $dist_image_path=$folder.$fileName;
            $cropper->resize_and_crop($original_image_path, $dist_image_path, 300, $altura, 100 );
            
            //$dist_image_path=$folder."th_".$fileName;
            //$cropper->resize_and_crop($original_image_path, $dist_image_path, 70, 70, 100 );
            
            //$altura=($height*594)/$width;
            
            //$dist_image_path=$folder."fix_".$fileName;
            //$cropper->resize_and_crop($original_image_path, $dist_image_path, 594, $height, 100 );
           
        }
}