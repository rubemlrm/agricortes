<?php

class MaquinaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(

array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update','updateimg','view','updateajax','updateajax2','updateajaxcatalogo','upload','download'),
'roles'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Maquina;
$model2=new ItemPortfolioImg;
Yii::import("xupload.models.XUploadForm");
$model3 = new XUploadForm;
                
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Maquina']))
{
$model->attributes=$_POST['Maquina'];
   
    $images = CUploadedFile::getInstancesByName('files');
    
    if (isset($images) && count($images) > 0) {
 
    // go through each uploaded image
    foreach ($images as $image => $pic) {
        //$name=explode(".",$pic->name);
        //$name=md5(time()+rand(0,1000)).".".$name[1];
        if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$pic->name)) {
            // add it to the main model now
             $img_add = new ItemPortfolioFile();
             $img_add->file = $pic->name; 
             $img_add->id_item_portfolio = $model->id;
            
             $img_add->save();
        }
        //else
            // handle the errors here, if you want
    }

    // save the rest of your information from the form
    if ($model->save()) {
        $this->redirect(array('admin'));
    }
}
            

    if($model->validate()){
    $rnd = md5(time());
    $uploadedFile=CUploadedFile::getInstance($model,'imagem_capa');

    if(!empty($uploadedFile))
    {
        
        $fileName = "i{$rnd}.{$uploadedFile->extensionName}";
        $model->imagem_capa = $fileName;
    }

    
    }


if($model->save())
{
    
    
    if(!empty($uploadedFile)){
        $uploadedFile->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$fileName);
        Maquina::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$fileName);
    }
   
    $this->redirect(array('admin'));
}
}

$this->render('create',array(
'model'=>$model,'model2'=>$model2,'model3'=>$model3,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);
$model2=new ItemPortfolioImg;
Yii::import("xupload.models.XUploadForm");
$model3 = new XUploadForm;

$imagem_antiga=$model->imagem_capa;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Maquina']))
{
    $model->attributes=$_POST['Maquina'];
    
    $images = CUploadedFile::getInstancesByName('files');
    
    if (isset($images) && count($images) > 0) {
 
    // go through each uploaded image
    foreach ($images as $image => $pic) {
        //$name=explode(".",$pic->name);
        //$name=md5(time()+rand(0,1000)).".".$name[1];
        if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$pic->name)) {
            // add it to the main model now
             $img_add = new ItemPortfolioFile();
             $img_add->file = $pic->name; 
             $img_add->id_item_portfolio = $model->id;
            
             $img_add->save();
        }
        //else
            // handle the errors here, if you want
    }

    // save the rest of your information from the form
    if ($model->save()) {
        $this->redirect(array('admin'));
    }
}
    
    if($model->validate()){
    $rnd = md5(time());
    $uploadedFile=CUploadedFile::getInstance($model,'imagem_capa');
    if(!empty($uploadedFile))
        {
            $fileName = "i{$rnd}.{$uploadedFile->extensionName}";
            $model->imagem_capa = $fileName;
            
            $old_file=Maquina::model()->findByPk($id)->imagem_capa;
            if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
                unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file);
        }
     elseif($imagem_antiga!="" && $imagem_antiga!=null)
         $model->imagem_capa=$imagem_antiga;
    
    
    }   
    
if($model->save()){
    
    
    
    if(!empty($uploadedFile))  
    {
        $uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/'.$model->imagem_capa);
        Maquina::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$model->imagem_capa);
    }
   
    $this->redirect(array('admin'));
}
}

$this->render('update',array(
'model'=>$model,'model2'=>$model2,'model3'=>$model3
));
}

public function actionUpdateImg($id)
{
$model=$this->loadModel($id);
$model2=new ItemPortfolioImg;
Yii::import("xupload.models.XUploadForm");
$model3 = new XUploadForm;
$model->save();
$imagem_antiga=$model->imagem_capa;
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Maquina']))
{
    $rnd = md5(time());
    $model->attributes=$_POST['Maquina'];
    $uploadedFile=CUploadedFile::getInstance($model,'imagem_capa');
    if(!empty($uploadedFile))
        {
            $fileName = "{$rnd}.{$uploadedFile->extensionName}";
            $model->imagem_capa = $fileName;
            
            $old_file=Maquina::model()->findByPk($id)->imagem_capa;
            if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
                unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file);
        }
     elseif($imagem_antiga!="" && $imagem_antiga!=null)
         $model->imagem_capa=$imagem_antiga;
        
    
if($model->save()){
    
    
     //Imagens
if($sfile=CUploadedFile::getInstancesByName('imagem')){
                      
foreach ($sfile as $i=>$file){
   $fileName=time().$i.'.'.$file->getExtensionName();

   if ($file->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName)) { 


        //Maquina::model()->createThumbnails(Yii::getPathOfAlias('webroot').'/uploads/',$fileName);

        $model3i=new ItemPortfolioImg;

        $model3i->imagem=$fileName;
        $model3i->id_item_portfolio=$model->id;
        $model3i->save(false);
   }

   }

}
    
    
    if(!empty($uploadedFile))  
    {
        $uploadedFile->saveAs(Yii::app()->basePath.'/../uploads/'.$model->imagem_capa);
    }
    $this->redirect(array('admin'));
}
}

$this->render('updateimg',array(
'model'=>$model,'model2'=>$model2,'model3'=>$model3
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
       
    
$old_file=Maquina::model()->findByPk($id)->imagem_capa;
if ($old_file!="" && $old_file!=null && file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file))
    unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $old_file);    




//Apagar imagens do portfólio
                
$imagens=ItemPortfolioImg::model()->findAll(array('condition'=>'id_item_portfolio=:id_item_portfolio', 'params'=>array(':id_item_portfolio'=>$id)));

foreach($imagens as $i=>$imagem)
{
if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'.$imagem->imagem))    
    unlink(Yii::getPathOfAlias('webroot').'/uploads/'.$imagem->imagem);
}
$files=ItemPortfolioFile::model()->findAll(array('condition'=>'id_item_portfolio=:id_item_portfolio', 'params'=>array(':id_item_portfolio'=>$id)));

foreach($imagens as $i=>$imagem)
{
if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'.$imagem->file))    
    unlink(Yii::getPathOfAlias('webroot').'/uploads/'.$imagem->file);
}


$query = "delete from item_portfolio_img where id_item_portfolio=:id_item_portfolio";
$command = Yii::app()->db->createCommand($query);
$command->execute(array('id_item_portfolio' => $id));

$this->loadModel($id)->delete();




// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Maquina');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Maquina('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Maquina']))
$model->attributes=$_GET['Maquina'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Maquina::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='item-portfolio-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}


public function actionUpdateAjax($id)
        {
            
            $model=Maquina::model()->findByPk($id);
            $fileName=$model->imagem_capa;
            $model->imagem_capa="";
            $model->save(false);
            if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
                unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);
           
            $this->renderPartial('_imagemContent', array('model'=>$model));
        }    
        

public function actionUpdateAjaxCatalogo($id,$model_id)
    {
        $model_aux=ItemPortfolioFile::model()->findByPk($id);
        $fileName=$model_aux->file;
        
        $model_aux->delete();
        if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
            unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);
       
        $model=Maquina::model()->findByPk($id);
        $imagens=ItemPortfolioFile::model()->findAllByAttributes(array('id_item_portfolio'=>$model_id));
        $this->renderPartial('_fileContent', array('model'=>$model,'files'=>$imagens));
    }      
        
        

public function actionUpdateAjax2($id,$id_item_portfolio)
        {
            $imagem=ItemPortfolioImg::model()->findByPk($id);
                $fileName=$imagem->imagem;
            
            if (file_exists(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName))
                unlink(Yii::getPathOfAlias('webroot').'/uploads/'. $fileName);
            
            ItemPortfolioImg::model()->deleteByPk($id);
           
            $imagens=ItemPortfolioImg::model()->getByItem($id_item_portfolio);
            
            $this->renderPartial('_ajaxContent', array('imagens'=>$imagens));
            
        }
  /*
public function actionDownload($id)
        {
            $model=Maquina::model()->findByPk($id);
            $path = Yii::app()->request->hostInfo . Yii::app()->request->baseURL . '/uploads/' . $model->catalogo;
            $extension=explode(".",$model->catalogo);
            return Yii::app()->getRequest()->sendFile($model->titulo_pt .".". $extension[1] , Yii::app()->curl->get($path));
        }   */    
        
        
public function actionUpload( ) {
    
    Yii::import( "xupload.models.XUploadForm" );
    //Here we define the paths where the files will be stored temporarily
    $path = realpath( Yii::app( )->getBasePath( )."/../uploads/tmp/" )."/";
    $publicPath = Yii::app( )->getBaseUrl( )."/uploads/tmp/";
 
    //This is for IE which doens't handle 'Content-type: application/json' correctly
    header( 'Vary: Accept' );
    if( isset( $_SERVER['HTTP_ACCEPT'] ) 
        && (strpos( $_SERVER['HTTP_ACCEPT'], 'application/json' ) !== false) ) {
        header( 'Content-type: application/json' );
    } else {
        header( 'Content-type: text/plain' );
    }
 
    //Here we check if we are deleting and uploaded file
    if( isset( $_GET["_method"] ) ) {
        
        if( $_GET["_method"] == "delete" ) {
            if( $_GET["file"][0] !== '.' ) {
                $file = $path.$_GET["file"];
                if( is_file( $file ) ) {
                    unlink( $file );
                }
            }
            echo json_encode( true );
        }
    } else {
        $model = new XUploadForm;
        $model->file = CUploadedFile::getInstance( $model, 'file' );
        //We check that the file was successfully uploaded
        if( $model->file !== null ) {
            //Grab some data
            $model->mime_type = $model->file->getType( );
            $model->size = $model->file->getSize( );
            $model->name = $model->file->getName( );
            //(optional) Generate a random name for our file
            $filename = md5( Yii::app( )->user->id.microtime( ).$model->name);
            $filename .= ".".$model->file->getExtensionName( );
            if( $model->validate( ) ) {
                //Move our file to our temporary dir
                $model->file->saveAs( $path.$filename );
                chmod( $path.$filename, 0777 );
                //here you can also generate the image versions you need 
                //using something like PHPThumb
 
 
                //Now we need to save this path to the user's session
                if( Yii::app( )->user->hasState( 'images' ) ) {
                    $userImages = Yii::app( )->user->getState( 'images' );
                } else {
                    $userImages = array();
                }
                 $userImages[] = array(
                    "path" => $path.$filename,
                    //the same file or a thumb version that you generated
                    "thumb" => $path.$filename,
                    "filename" => $filename,
                    'size' => $model->size,
                    'mime' => $model->mime_type,
                    'name' => $model->name,
                );
                Yii::app( )->user->setState( 'images', $userImages );
 
                //Now we need to tell our widget that the upload was succesfull
                //We do so, using the json structure defined in
                // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                echo json_encode( array( array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => $publicPath.$filename,
                        //"thumbnail_url" => $publicPath."thumbs/$filename",
                        "thumbnail_url" => $publicPath."$filename",
                        "delete_url" => $this->createUrl( "upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        ) ),
                        "delete_type" => "POST"
                    ) ) );
            } else {
                //If the upload failed for some reason we log some data and let the widget know
                echo json_encode( array( 
                    array( "error" => $model->getErrors( 'file' ),
                ) ) );
                Yii::log( "XUploadAction: ".CVarDumper::dumpAsString( $model->getErrors( ) ),
                    CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction" 
                );
            }
        } else {
            throw new CHttpException( 500, "Could not upload file" );
        }
    }
}


}
