<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
                    
                         'upload'=>array(
                            'class'=>'xupload.actions.XUploadAction',
                            'path' =>Yii::app() -> getBasePath() . "/../uploads",
                            'publicPath' => Yii::app() -> getBaseUrl() . "/uploads",
                        ),
		);
	}
        
        public function accessRules()
        {
        return array(

        array('allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions'=>array('index','updateajax','dashboard'),
        'roles'=>array('admin'),
        ),
        array('allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions'=>array('index','pesquisa','pecas','login2'),
        'users'=>array('*'),
        ),    
        array('allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions'=>array('AreaReservada'),
        'users'=>array('@'),
        ),      
        array('deny',  // deny all users
        'users'=>array('*'),
        ),
        );
        }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
                        
                $this->pageTitle=Yii::app()->name;
                $this->render('index');
	}
        
        public function actionAreaReservada()
	{
            if(Yii::app()->user->id!=null){
                $this->layout="//layouts/column1";        
                $this->pageTitle=Yii::app()->name;
                $this->render('area_reservada');
            }
            else
                throw new CHttpException(404,'Página não encontrada.');
	}
        
        public function actionContact()
	{
                $this->layout="//layouts/column1";
		$model=new ContactForm;
                
		if(isset($_POST['ContactForm']))
		{
                    
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
                            
				$subject='Contacto do website';
                                
                                $mail = new YiiMailer();
                                //$mail->clearLayout();//if layout is already set in config
                                $mail->setFrom($model->email, $model->name);
                                $mail->setTo(Yii::app()->params['adminEmail']);
                                //$mail->setTo("mail@norberto.eu");
                                $mail->setSubject($subject);
                                $mail->setBody($model->body);
                                $mail->send();
                                //var_dump($mail);die;
				Yii::app()->user->setFlash('contact','Obrigado por nos contactar. Entraremos em contacto consigo o mais breve possível.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
        
        public function actionUpdateAjax()
        {
            $data = array();
            $data["myValue"] = "Content updated in AJAX";

            $this->renderPartial('_portfolio', $data, false, true);
        
        }
        
        public function actionPesquisa()
        {
            $this->pageTitle=Yii::app()->name;
            $var_pesquisa=$_POST['pesquisatxt'];
            
            $area_atividade=null;
            $categoria=null;
            if($var_pesquisa!=""){
            $var_pesquisa_aux=explode(" ",$var_pesquisa);
            
            //Pesquisar áreas de atividade
            $criteria = new CDbCriteria();
            $query=null;
            for($i=0;$i<sizeof($var_pesquisa_aux);$i++)
            {
                $str=$var_pesquisa_aux[$i];
                if($i>0)
                    $query.=' OR ';
                $query.='nome_pt="'.$str.'" OR nome_es="'.$str.'" OR nome_en="'.$str.'"';
            }
            $criteria->condition = $query;
            $area_atividade = AreaAtividade::model()->findAll($criteria);
            
            //Pesquisar categorias
            $criteria = new CDbCriteria();
            $query='(';
            for($i=0;$i<sizeof($var_pesquisa_aux);$i++)
            {
                $str=$var_pesquisa_aux[$i];
                if($i>0)
                    $query.=' OR ';
                $query.='(nome_pt LIKE "%'.$str.'%" OR nome_es LIKE "%'.$str.'%" OR nome_en LIKE "%'.$str.'%")';
            }
            $query.=")AND (categoria_main IS NOT NULL)";
            //var_dump($query);die;
            $criteria->condition = $query;
            $categoria = Categoria::model()->findAll($criteria);
            
            }
            $this->render('pesquisa',array('varpesquisa'=>$var_pesquisa,'areaatividade'=>$area_atividade,'categoria'=>$categoria));
            
            
        }
       
        public function actionPecas()
        {
            $model=new ContactPecas;
            $model2=new ContactGarantia;
            $model3=new ContactPecas2;
            $selecionado=false;
            
            if(isset($_POST['ContactPecas']))
            {
                if($_POST['ContactPecas']['pecasinput']=="true")
                {
                    
                $model->attributes=$_POST['ContactPecas'];
                if(isset($_POST['ContactPecas2']))
                {
                    $pecas="PEÇAS:<br>";
                    foreach ($_POST['ContactPecas2'] as $item)
                    {
                        $pecas.="Unidades: ".$item['unidades']."<br>";
                        $pecas.="Descrição: ".$item['descricao']."<br>";
                        $pecas.="Referência: ".$item['referencia']."<br><br>";
                    }
                    
                    
                }
                
                               
                if($model->validate())
                {
                    $subject='Pedido de Peças';
                    $mail = new YiiMailer();
                    $mail->setFrom($model->email, $model->nome);
                    $mail->setTo(Yii::app()->params['adminEmail']);
                    //$mail->setTo("mail@norberto.eu");
                    $mail->setSubject($subject);
                    $mensagem="Modelo: ".$model->modelo."<br>";
                    $mensagem.="Nº Série: ".$model->num_serie."<br>";
                    $mensagem.="Nº Motor: ".$model->num_motor."<br>";
                    $mensagem.="Empresa: ".$model->empresa."<br>";
                    $mensagem.="Nome: ".$model->nome."<br>";
                    $mensagem.="Morada: ".$model->morada."<br>";
                    $mensagem.="Cód.Postal: ".$model->cod_postal."<br>";
                    $mensagem.="Localidade: ".$model->localidade."<br>";
                    $mensagem.="Contacto: ".$model->contacto."<br>";
                    $mensagem.="E-mail: ".$model->email."<br>";
                    //$mensagem.="Morada: ".$model->morada."<br>";
                    $mensagem.="Nº Contribuinte: ".$model->contribuinte."<br>";
                    $mensagem.="<br><br>";
                    $mensagem.=$pecas;
                    $mail->setBody($mensagem);
                    $mail->send();
                    Yii::app()->user->setFlash('pecas','O seu pedido foi realizado com sucesso. Entraremos em contacto consigo o mais breve possível.');
                    $this->refresh();
                }
                }
            }
            
            if(isset($_POST['ContactGarantia']))
            {
                $selecionado=true;  
                if($_POST['ContactGarantia']['registoinput']=="true")    
                {
                  
                $model2->attributes=$_POST['ContactGarantia'];
                               
                if($model2->validate())
                {
                    $subject='Registo online de garantia';
                    $mail = new YiiMailer();
                    $mail->setFrom($model2->email, $model2->nome);
                    $mail->setTo(Yii::app()->params['adminEmail']);
                    //$mail->setTo("mail@norberto.eu");
                    $mail->setSubject($subject);
                    $mensagem="Modelo: ".$model2->modelo."<br>";
                    $mensagem.="Nº Série: ".$model2->num_serie."<br>";
                    $mensagem.="Nº Motor: ".$model2->num_motor."<br>";
                    $mensagem.="Matricula: ".$model2->matricula."<br>";
                    $mensagem.="Empresa: ".$model2->empresa."<br>";
                    $mensagem.="Nome: ".$model2->nome."<br>";
                    $mensagem.="Morada: ".$model2->morada."<br>";
                    $mensagem.="Cód.Postal: ".$model2->cod_postal."<br>";
                    $mensagem.="Localidade: ".$model2->localidade."<br>";
                    $mensagem.="Contacto: ".$model2->contacto."<br>";
                    $mensagem.="E-mail: ".$model2->email."<br>";
                    $mensagem.="Nº Contribuinte: ".$model2->contribuinte."<br>";
                    $mensagem.="Data compra: ".$model2->data_compra."<br>";
                    $mensagem.="Data de entrada em serviço: ".$model2->data_servico."<br>";
                    $mensagem.="Concessionário: ".$model2->concessionario."<br>";
                   
                    $mail->setBody($mensagem);
                    $mail->send();
                    Yii::app()->user->setFlash('registo','O seu registo foi realizado com sucesso.');
                    //$this->refresh();
                   
                }
                }
            }
            
            $this->render('pecas',array('model'=>$model,'model2'=>$model2,'model3'=>$model3,'selecionado'=>$selecionado));
        }
       
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
                $this->layout="column1";
                
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
                $this->layout="login";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
                    
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
                        //if($model->login())    
				$this->redirect(Yii::app()->baseUrl.'/site/dashboard');
                             
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
        
        public function actionLogin2()
	{
                $this->layout="column1";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
                        
			if($model->validate() && $model->login())
                        {
                            $user=User::model()->findByAttributes(array('username'=>$model->username));
                            
                                if($user->roles=="admin")
                                    $this->redirect(Yii::app()->baseUrl.'/site/dashboard');
                                elseif($user->roles=="user")
                                    $this->redirect(Yii::app()->baseUrl.'/site/areaReservada');
                                else{
                                    Yii::app()->user->logout();
                                    $_SESSION['slider']=true;
                                    Yii::app()->user->setFlash('conta','A sua conta ainda não foi validada.');
                                    
                                }
                        }
                             
		}
		// display the login form
		$this->render('login2',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
            $user=User::model()->findByPk(Yii::app()->user->id);
            $roles=$user->roles;
            
            $_SESSION['slider']=true;
            
		Yii::app()->user->logout();
                if($roles=="admin")
                    $this->redirect(Yii::app()->baseUrl.'/site/login');
                else
                    $this->redirect(Yii::app()->baseUrl.'/site/login2');
	}
        
        public function actionDashboard()
	{
		$this->layout="column2";
                
                if(Yii::app()->user->id==null || Yii::app()->user->roles!="admin")
                    $this->redirect(Yii::app()->baseUrl.'/site/login');
                else
                    $this->render('dashboard');
	}
        
        
        
}