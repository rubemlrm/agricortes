<?php

class UserController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(

array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','update','create','view','ChangePass'),
'roles'=>array('admin'),
),
array('allow',  
'actions'=>array('create2'),    
'users'=>array('*'),
),    
    /*
array('allow',  
        'actions'=>array('view','update','ChangePass'),
        'users'=>array(Yii::app()->user->name),
        'expression' => '(($_GET[\'id\']) == Yii::app()->user->id)',
),  */  
    
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new User;
//$user->setScenario('create');
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['User']))
{
$model->attributes=$_POST['User'];
//$user->password = md5($_POST['User']['password']);
//$user->password = md5($_POST['User']['repeat_password']);

if($model->save())
$this->redirect(array('admin'));
}

$this->render('create',array(
'model'=>$model,
));
}

public function actionCreate2()
{
$this->layout="column1";    
    
$model=new User;

if(isset($_POST['User']))
{
$model->attributes=$_POST['User'];

//$model->password = md5($_POST['User']['password']);
//$model->password_repeat = md5($_POST['User']['password_repeat']);
$model->roles="pendente";
        
if($model->save())

//Enviar E-mail de notificação
$subject='Novo registo de utilizador';
$mail = new YiiMailer();
$mail->setFrom($model->email, $model->nome);
$mail->setTo(Yii::app()->params['adminEmail']);
//$mail->setTo("mail@norberto.eu");
$mail->setSubject($subject);
$mensagem="Foi criado um novo registo na área reservada com o nome:<br>";
$mensagem.=$model->firstname." ".$model->lastname ."<br>";
$mensagem.="Nome de utilizador: " . $model->username . "<br>";
$mensagem .="Password: " .$model->password . "<br>"; 

$mail->setBody($mensagem);
$mail->send();
      
Yii::app()->user->setFlash('success-register', Content::model()->getContentReturn('registo_ok', Yii::app()->language));    
}

$this->render('create2',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);


if(isset($_POST['User']))
{
$model->attributes=$_POST['User'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

public function actionChangePass($id)
    {  
    $user = $this->loadModel($id);
    $user->setScenario('change_pass');
    
    if(isset($_POST['User']))
    {
        if($_POST['User']['old_password'] === $user->password)
        {
           if($_POST['User']['password']===$_POST['User']['password_repeat'] && $_POST['User']['password']!="")
           {
               
           
           $user->attributes = $_POST['User'];                
           //$user->password = md5($_POST['User']['password']);
           if($user->save())
           Yii::app()->user->setFlash('success', 'A password foi alterada com sucesso.');
           }
           else
           {
               if($_POST['User']['password']==="")
                    Yii::app()->user->setFlash('error', 'A password não foi alterada. Escreva a nova password.');
               else
                    Yii::app()->user->setFlash('error', 'A password não foi alterada. Repita a password corretamente.');
           }


        }            
        else
        {
          Yii::app()->user->setFlash('error', 'A password não foi alterada. A password anterior não está correta.');                    
        } 
    }
    //$user->setScenario('change_pass');
    $user->unsetAttributes(array('password'));
    $this->render('changePass',array(
    'model'=>$user,
));
 }

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('User');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new User('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['User']))
$model->attributes=$_GET['User'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=User::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
