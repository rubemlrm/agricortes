<?php

class CategoriaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('view'),
'users'=>array('*'),
),
    
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete','create','update'),
'roles'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
    
$this->layout="//layouts/column1";
$model=Categoria::model()->findByPk($id);

$model2=new ContactForm;
if(isset($_POST['ContactForm']))
{

        $model2->attributes=$_POST['ContactForm'];
        if($model2->validate())
        {
                $pedido=new Pedido;
                $pedido->attributes=$model2->attributes;
                $pedido->data=date("Y-m-d H:i:s");
                $pedido->nome=$model2->name;
                $pedido->cod_postal=$model2->codpostal;
                $pedido->mensagem=$model2->body;
                $pedido->save(false);
            
                $assunto=$model2['assunto'];
                $subject='Máq. Equip. '.$assunto;
                $mail = new YiiMailer();
                $mail->setFrom($model2->email, $model2->name);
                $mail->setTo(Yii::app()->params['adminEmail']);
                //$mail->setTo("mail@norberto.eu");
                $mail->setSubject($subject);
                
                $mensagem="Nome: ".$model2->name."<br>";
                $mensagem.="Empresa: ".$model2->empresa."<br>";
                $mensagem.="Telemóvel: ".$model2->telemovel."<br>";
                $mensagem.="E-mail: ".$model2->email."<br>";
                $mensagem.="Código Postal: ".$model2->codpostal."<br>";
                $mensagem.="Mensagem: ".$model2->body."<br>";
                $mail->setBody($mensagem);
                $mail->send();
                Yii::app()->user->setFlash('contact','Obrigado por nos contactar. Entraremos em contacto consigo o mais breve possível.');
                $this->refresh();
        }
}

if($model===null){    
    $model=Categoria::model()->findByAttributes(array('slug_pt'=>$id));    
}

if($model===null)
    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  

$area_escolhida=AreaAtividade::model()->findByPk($model->area_atividade_id);

    
$areas_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));

$Criteria = new CDbCriteria();
$Criteria->condition = "area_atividade_id = ".$area_escolhida->id." AND categoria_main IS NULL";
$Criteria->order="ordem ASC";
$categorias = Categoria::model()->findAll($Criteria);

$Criteria = new CDbCriteria();
$Criteria->condition = "categoria_id = ".$model->id;
$Criteria->order="ordem ASC";
$maquinas = Maquina::model()->findAll($Criteria);

$this->render('view',array(
'model'=>$model,'areas_atividade'=>$areas_atividade,'categorias'=>$categorias,'area_escolhida'=>$area_escolhida,'maquinas'=>$maquinas,'model2'=>$model2
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Categoria;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Categoria']))
{
$model->attributes=$_POST['Categoria'];
$model->slug_pt=Pagina::model()->toAscii($model->nome_pt);
$model->slug_es=Pagina::model()->toAscii($model->nome_es);
$model->slug_en=Pagina::model()->toAscii($model->nome_en);

if($model->save())
$this->redirect(array('admin'));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Categoria']))
{
$model->attributes=$_POST['Categoria'];
$model->slug_pt=Pagina::model()->toAscii($model->nome_pt);
$model->slug_es=Pagina::model()->toAscii($model->nome_es);
$model->slug_en=Pagina::model()->toAscii($model->nome_en);

if($model->save())
$this->redirect(array('admin'));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Categoria');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Categoria('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Categoria']))
$model->attributes=$_GET['Categoria'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Categoria::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='categoria-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
