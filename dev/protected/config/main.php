<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'AGRICORTES',
        'language'=>'pt',
	// preloading 'log' component
	'preload'=>array('log','bootstrap',),
        'behaviors'=>array(
        'onBeginRequest' => array(
            'class' => 'application.components.behaviors.BeginRequest'
            ),
        ),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'ext.YiiMailer.YiiMailer',
                'ext.imagecropper.ImageCropper',
                'ext.curl.Curl',
	),
        'aliases' => array(
            //If you used composer your path should be
            //'xupload' => 'ext.vendor.asgaroth.xupload',
            //If you manually installed it
            'xupload' => 'ext.xupload',
        ),
    

	'modules'=>array(
            
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1','89.152.241.209'),
                        'generatorPaths' => array(
                        'bootstrap.gii'
                     ),
		),
		
	),

	// application components
	'components'=>array(
            
                'curl' => array(
                    'class' => 'ext.curl.Curl',
                    //'options' => array(/.. additional curl options ../)
               ),
            
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'class'=>'WebUser',
		),
                    'request'=>array(
                    'enableCookieValidation'=>true,
                    'enableCsrfValidation'=>false,
                ),
               
            'bootstrap' => array(
                'class' => 'ext.bootstrap.components.Bootstrap',
                'responsiveCss' => true,
            ),

            

            
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
                            
                                
                                '<view:(downloads|terms|copyright)>' => 'site/page',
                            
                                'pagina/admin'=>'pagina/admin',
                                'pagina/create'=>'pagina/create',
                                'pagina/<id:[a-zA-Z0-9-]+>'=>'pagina/view',
                                
                                'AreaAtividade/admin'=>'AreaAtividade/admin',
                                'AreaAtividade/AreaAtividade'=>'AreaAtividade/create',
                                'AreaAtividade/<id:[a-zA-Z0-9-]+>'=>'AreaAtividade/view',
                            
                                'itemPortfolioImg/admin'=>'itemPortfolioImg/admin',
                                'itemPortfolioImg/itemPortfolioImg'=>'itemPortfolioImg/create',
                                'itemPortfolioImg/itemPortfolioImg'=>'itemPortfolioImg/update',
                            
                                'categoria/admin'=>'categoria/admin',
                                'categoria/create'=>'categoria/create',
                                'categoria/<id:[a-zA-Z0-9-]+>'=>'categoria/view',
                            
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
            
           	
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		/*'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=mcr',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),*/
            
                'db'=>array(
			//'connectionString' => 'mysql:host=127.0.0.1;dbname=agricortes',
                        'connectionString' => 'mysql:host=127.0.0.1;dbname=agricort_db',
			'emulatePrepare' => true,
			//'username' => 'root',
			//'password' => '',
                        'username' => 'agricort_user',
			'password' => 'XObcMCuW5zaE',
			'charset' => 'utf8',
		),
           
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'agricortes@agricortes.com',
            'languages'=>array('pt'=>'PT', 'es'=>'ES'),
	),
);


