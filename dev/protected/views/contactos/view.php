<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Contactos',
    'headerIcon' => 'icon-envelope',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar', 'url'=>'index.php?r=contactos/update&id=1'),    
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Contactos";

$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	'Manage',
);


?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'empresa',
                array(
                    'name'=>'morada',
                    'type'=>'raw',
                    'value'=>$model->morada,
                ),
		'telefone',
		'telemovel',
		'fax',
		'email',
		'gps',
               
),
));

$this->endWidget();
?>


</div>
</div>