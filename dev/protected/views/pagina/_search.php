<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'titulo_en',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'titulo_fr',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textAreaRow($model,'conteudo_pt',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textAreaRow($model,'conteudo_en',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textAreaRow($model,'conteudo_fr',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textAreaRow($model,'conteudo_es',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'imagem',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
