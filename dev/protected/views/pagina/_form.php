<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'noticia-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span8','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span8','maxlength'=>100)); ?>

	<div class='control-group'>
        <?php echo $form->labelEx($model,'conteudo_pt',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->textArea($model, 'conteudo_pt', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-pt','placeholder'=>''));
        
        ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace('some-textarea-pt', {
                "extraPlugins": "imagebrowser",
                "imageBrowser_listUrl": "<?php
                $url=Website::model()->findByPk(1);
              
                if($url!=null)
                    echo $url->url;
                ?>/uploads/image_list_agricortes.js"
            });
        </script>

	


	<div class='control-group'>
        <?php echo $form->labelEx($model,'conteudo_es',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->textArea($model, 'conteudo_es', array('class'=>'span12', 'rows' => 6,'id'=>'some-textarea-es','placeholder'=>''));
      
        ?>
        </div></div>
        <script type="text/javascript">
            CKEDITOR.replace('some-textarea-es', {
                "extraPlugins": "imagebrowser",
                "imageBrowser_listUrl": "<?php
                $url=Website::model()->findByPk(1);
                if($url!=null)
                    echo $url->url;
                ?>/uploads/image_list_agricortes.js"
            });
        </script>

	<?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'imagem',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'imagem');
        echo"</div></div>";
        ?>
        
        <?php
            if(!$model->isNewRecord)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>
        
        <?php echo $form->dropDownListRow($model,'visivel',array(1=>'Sim',0=>'Não')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
