<?php
$conteudo="conteudo_".Yii::app()->language;
$titulo="titulo_".Yii::app()->language;
$this->pageTitle=Yii::app()->name . ' - '.mb_strtoupper($model->$titulo,"UTF-8");
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <?php
        
        $menu=Menu::model()->findByAttributes(array("pagina_id"=>$model->id));
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$menu->imagem.'" width="100%">';
        ?>
        
        
    </div>
</div>
<div class="container branco">
        <?php
        if($model->imagem!=null)
        {    
        ?>
        <div class="col-md-8">
            
            <div class="texto">
                <h5><?php echo mb_strtoupper($model->$titulo,"UTF-8"); ?></h5><br>
                <?php echo $model->$conteudo; ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            if($model->imagem!=null)
            {
                echo'<img class="texto" src="'.Yii::app()->baseUrl."/uploads/".$model->imagem.'" width="100%">';
            }
            ?>
        </div>
        <?php
        }
        else
        {
        ?>
        
        <div class="col-md-12">
            <div class="texto">
                <h5><?php echo mb_strtoupper($model->$titulo,"UTF-8"); ?></h5><br>
                <?php echo $model->$conteudo; ?>
            </div>
        </div>
        
        <?php } ?>
</div>

<br>
<br>
