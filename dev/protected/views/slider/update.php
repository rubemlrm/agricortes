<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Slider '.$model->id,
    'headerIcon' => 'icon-film',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir Slider', 'url'=>Yii::app()->baseUrl.'/slider/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Slider ".$model->id;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>