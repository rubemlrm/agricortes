<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar imagem '.$model->nome_pt,
    'headerIcon' => 'icon-film',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir galerias', 'url'=>Yii::app()->baseUrl.'/ItemPortfolioImg/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar galeria ".$model->nome_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>