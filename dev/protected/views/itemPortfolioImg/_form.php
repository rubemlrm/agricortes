<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'item-portfolio-img-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nome_pt',array('class'=>'span6','maxlength'=>100)); ?>
        <?php echo $form->textFieldRow($model,'nome_es',array('class'=>'span6','maxlength'=>100)); ?>

        <?php
        echo"<div class='control-group'>";
            echo"<div class='controls'>";
            echo'<ul class="thumbnails">';
            echo'<li class="span2">
            <div class="thumbnail">
              <img src="'.Yii::app()->request->baseUrl.'/uploads/'.$model->imagem.'" alt="">';

           echo'</div>
          </li>';
            echo'</ul>';
        echo"</div></div>"; 
        ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
