<?php
$this->breadcrumbs=array(
	'Item Portfolio Imgs',
);

$this->menu=array(
array('label'=>'Create ItemPortfolioImg','url'=>array('create')),
array('label'=>'Manage ItemPortfolioImg','url'=>array('admin')),
);
?>

<h1>Item Portfolio Imgs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
