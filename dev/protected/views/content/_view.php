<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value_pt')); ?>:</b>
	<?php echo CHtml::encode($data->value_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value_en')); ?>:</b>
	<?php echo CHtml::encode($data->value_en); ?>
	<br />


</div>