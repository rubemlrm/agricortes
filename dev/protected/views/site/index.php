<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
                <div class="row carousel-holder">
                    <div class="hidden-xs col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                
                                <?php
                                $slider=Slider::model()->findAll(array('order'=>'ordem ASC'));
                                $active="active";
                                $titulo="titulo_".Yii::app()->language;
                                foreach ($slider as $s)
                                {
                                    echo'<div class="item '.$active.'">
                                    <img class="slide-image" src="uploads/'.$s->imagem.'" alt="">
                                    <div class="carousel-caption">';
                                    if($s->$titulo!="")
                                        echo'<p><button onclick="window.location.href=\''.$s->link.'\'" type="button" class="btn btn-default btn-lg">'.mb_strtoupper($s->$titulo,"UTF-8").'</button></p>';
                                    
                                    echo'</div></div>';
                                $active="";    
                                }
                                ?>
                                
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>
    </div>
</div>
<br>
<br>
<div class="container">
        <div class="col-md-4">
            <h5><?php echo mb_strtoupper(Content::model()->getContentReturn('areas_atividade', Yii::app()->language),"UTF-8"); ?></h5>
            
            <?php
            $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
            $nome="nome_".Yii::app()->language;
            $slug="slug_".Yii::app()->language;
            foreach($area_atividade as $area)
            {
                echo'<div class="caixa_index">
                <div class="row">
                    <div class="col-xs-10 col-md-10">
                        <h8>'.CHtml::link('AGRICORTES <b>'.mb_strtoupper($area->$nome,"UTF-8").'</b>',Yii::app()->baseUrl."/AreaAtividade/".$area->$slug,array('style'=>'color:#6E6E6E')).'<h8>
                    </div>
                    <div class="col-xs-2 col-md-2">
                        <i class="fa fa-chevron-circle-right fa-1x"></i>
                    </div>
                </div>
            </div>';
            }
            ?>
          
        </div>
        <br class="visible-xs">
        <!--<div class="col-md-8">
            <h5><?php //echo mb_strtoupper(Content::model()->getContentReturn('bem_vindo', Yii::app()->language),"UTF-8"); ?></h5>
        </div>-->
        <div class="col-md-8">
               
                <div class="col-md-offset-1 col-md-10">
                    <img src="images/45anos.png" width="60%">
                    <br><br>
                    <p><?php Content::model()->getContentEcho('texto_home_1', Yii::app()->language); ?></p>
                    <p>
                    <br>
                    
                    <?php
                    $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
                    $nome="nome_".Yii::app()->language;
                    $slug="slug_".Yii::app()->language;
                    $links="";
                    foreach($area_atividade as $area)
                    {
                       $links.=CHtml::link($area->$nome,Yii::app()->baseUrl."/AreaAtividade/".$area->$slug)." | ";
                    }
                    echo substr($links, 0, -3);
                    ?>
                    </p>
                    <br>
                    <p><?php Content::model()->getContentEcho('texto_home_2', Yii::app()->language); ?></p>
                </div>
        </div>
</div>

<br>
<br>