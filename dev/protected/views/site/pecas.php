<?php
$this->pageTitle=Yii::app()->name." - ".mb_strtoupper(Content::model()->getContentReturn('pecas_servicos', Yii::app()->language),"UTF-8");


?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <?php
        $menu=Menu::model()->findByAttributes(array("link"=>"site/pecas"));
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$menu->imagem.'" width="100%">';
        ?>
    </div>
</div>
 <div class="container branco">
            <div class="texto">
                
                <ul class="nav nav-tabs">
                    <li <?php if(!$selecionado) echo 'class="active"';  ?>><a href="#pecas" data-toggle="tab" id="pecas_link"><i class="fa fa-cogs"></i>&nbsp;<?php Content::model()->getContentEcho('pecas', Yii::app()->language);?></a></li>
                    <li><a href="#assiste" data-toggle="tab"><i class="fa fa-user"></i>&nbsp;<?php Content::model()->getContentEcho('assistencia_tecnica', Yii::app()->language);?></a></li>
                    <li <?php if($selecionado) echo 'class="active"';  ?>><a href="#registo" data-toggle="tab" id="registo_link"><i class="fa fa-check-square"></i>&nbsp;<?php Content::model()->getContentEcho('registo_online', Yii::app()->language);?></a></li>
                    <li><a href="#doctec" data-toggle="tab"><i class="fa fa-file"></i>&nbsp;<?php Content::model()->getContentEcho('documentacao_tecnica', Yii::app()->language);?></a></li>
                </ul>
                
                <div class="tab-content">
                    <div class="tab-pane" id="assiste">
                        <div class="col-md-9">
                            <br>
                            <br>
                            <p><?php Content::model()->getContentEcho('txt01_assiste', Yii::app()->language);?></p>
                            <br><br>
                        </div>
                        
                        <br><br>
                        
                    </div>
               
                    <div class="tab-pane <?php if(!$selecionado) echo 'active';  ?>" id="pecas">
                    <div class="col-md-9">
                    <br>
                    <br>
                    <p><?php Content::model()->getContentEcho('txt01_pecas', Yii::app()->language);?></p>
                    <br><br>
                    </div>

                <div class="col-md-9">
                  <h5><?php Content::model()->getContentEcho('faca_encomenda', Yii::app()->language);?></h5>
                  <br>
                  <?php if(Yii::app()->user->hasFlash('pecas')): ?>

                    <div class="flash-success">
                            <?php echo Yii::app()->user->getFlash('pecas'); ?>
                    </div>

                    <?php else: ?>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                    'validateOnSubmit'=>true,
                            ),
                    )); ?>
                    <?php echo $form->errorSummary($model); ?>
                    <input type="hidden" value="true" id="ContactPecas_pecasinput" name="ContactPecas[pecasinput]"> 
                    <div class="row">
                      <div class="form-group col-md-3">
                        <?php echo $form->textField($model,'modelo',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('modelo', Yii::app()->language))); ?>
                      </div>
                      <div class="form-group col-md-3">
                        <?php echo $form->textField($model,'num_serie',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('num_serie', Yii::app()->language))); ?>
                      </div>
                        <div class="form-group col-md-3">
                        <?php echo $form->textField($model,'num_motor',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('num_motor', Yii::app()->language))); ?>
                      </div>
                    </div>
                    <br>
                    
                    <div class="row">
                      <div class="form-group col-md-3">
                        <?php echo $form->textField($model,'empresa',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('empresa', Yii::app()->language))); ?>
                      </div>
                      <div class="form-group col-md-6">
                        <?php echo $form->textField($model,'nome',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('nome', Yii::app()->language),'size'=>55)); ?>
                      </div>
                    </div>
                    <br>
                    
                    
                    <div class="row">
                      <div class="form-group col-md-4">
                         <?php echo $form->textField($model,'morada',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('morada', Yii::app()->language),'size'=>30)); ?>
                      </div>  
                      <div class="form-group col-md-2">
                         <?php echo $form->textField($model,'cod_postal',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('cod_postal', Yii::app()->language),'size'=>8)); ?>
                      </div>
                       <div class="form-group col-md-3">
                         <?php echo $form->textField($model,'localidade',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('localidade', Yii::app()->language),'size'=>20)); ?>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="form-group col-md-3">
                         <?php echo $form->textField($model,'contacto',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('contacto', Yii::app()->language))); ?>
                      </div>
                      <div class="form-group col-md-3">
                         <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'E-mail')); ?>
                      </div>
                        <div class="form-group col-md-3">
                         <?php echo $form->textField($model,'contribuinte',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('contribuinte', Yii::app()->language))); ?>
                      </div>
                    </div>
                    <div id="linha_pecas">
                        
                               <?php
                               if(isset($_POST['ContactPecas2']))
                                {
                                    
                                   $conta=0;
                                 foreach ($_POST['ContactPecas2'] as $item)
                                    {
                                     
                                    $model3->unidades=$item['unidades'];
                                    $model3->descricao=$item['descricao'];
                                    $model3->referencia=$item['referencia'];
                                    
                                    $conta++; 
                                    echo'<div class="row">';
                                    echo'<div class="form-group col-md-3">';
                                    echo $form->textField($model3,'['.$conta.']unidades',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('unidades', Yii::app()->language)));
                                    echo'</div>';
                                    echo'<div class="form-group col-md-3">';
                                    echo $form->textField($model3,'['.$conta.']descricao',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('descricao', Yii::app()->language)));
                                    echo'</div>';
                                    echo'<div class="form-group col-md-3">';
                                    echo $form->textField($model3,'['.$conta.']referencia',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('referencia', Yii::app()->language)));
                                    echo'</div>';
                                    echo'</div>';  
                                    }
                                  
                                }
                               ?>
                           
                        </div> 
                  
                    <br>
                    <a id="adicionar" href="#"><?php Content::model()->getContentEcho('adicionar_peca', Yii::app()->language);?></a>
                    <script>
                    $( "#adicionar" ).click(function() {
                         var num = $("#linha_pecas > div > div > input").length;
                         num++;
                         $("#linha_pecas").append('<div class="row">\n\
                                                   <div class="form-group col-md-3">\n\
                                                        <input id="ContactPecas2_'+num+'_unidades" class="form-control" type="text" name="ContactPecas2['+num+'][unidades]" placeholder="Unidades"></input>\n\
                                                   </div>\n\
                                                   <div class="form-group col-md-3">\n\
                                                        <input id="ContactPecas2_'+num+'_descricao" class="form-control" type="text" name="ContactPecas2['+num+'][descricao]" placeholder="Descrição"></input>\n\
                                                   </div>\n\
                                                   <div class="form-group col-md-3">\n\
                                                        <input id="ContactPecas2_'+num+'_referencia" class="form-control" type="text" name="ContactPecas2['+num+'][referencia]" placeholder="Referência"></input>\n\
                                                   </div>\n\
                                                   </div>');
                    });
                    
                    </script>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <?php echo $form->checkBox($model,'check'); ?>  
                            <?php Content::model()->getContentEcho('autorizo', Yii::app()->language);?>
                          </label>
                      </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default btn-sm"><?php Content::model()->getContentEcho('enviar_pedido', Yii::app()->language);?></button>
                    </div>
                    <?php $this->endWidget(); ?>
                    <?php endif; ?>
                       <br><br>
                </div>
                    </div>
                    
                    <div class="tab-pane <?php if($selecionado) echo 'active';  ?>" id="registo">
                        <div class="col-md-9">
                            <br>
                            <br>
                            <p><?php Content::model()->getContentEcho('este_servico', Yii::app()->language);?></p>
                            <br>
                            <br>
                            <?php if(Yii::app()->user->hasFlash('registo')): ?>

                              <div class="flash-success">
                                      <?php echo Yii::app()->user->getFlash('registo'); ?>
                              </div>

                              <?php else: ?>
                              <?php $form2=$this->beginWidget('CActiveForm', array(
                                      'id'=>'contact-form2',
                                      'enableClientValidation'=>true,
                                      'clientOptions'=>array(
                                              'validateOnSubmit'=>true,
                                      ),
                              )); ?>
                           
                              <?php echo $form2->errorSummary($model2); ?>
                              <input type="hidden" value="<?php if($_POST['ContactGarantia']['registoinput']=="true") echo "true"; else echo "false"; ?>" id="ContactGarantia_registoinput" name="ContactGarantia[registoinput]">   
                              <div class="row">
                                <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'modelo',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('modelo', Yii::app()->language))); ?>
                                </div>
                                <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'num_serie',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('num_serie', Yii::app()->language))); ?>
                                </div>
                                  <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'num_motor',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('num_motor', Yii::app()->language))); ?>
                                </div>
                                <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'matricula',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('matricula', Yii::app()->language))); ?>
                                </div>  
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'empresa',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('empresa', Yii::app()->language))); ?>
                                </div>   
                                <div class="form-group col-md-8">
                                  <?php echo $form2->textField($model2,'nome',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('nome', Yii::app()->language))); ?>
                                </div>   
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'morada',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('morada', Yii::app()->language))); ?>
                                </div> 
                                <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'cod_postal',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('cod_postal', Yii::app()->language))); ?>
                                </div>  
                                <div class="form-group col-md-5">
                                  <?php echo $form2->textField($model2,'localidade',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('localidade', Yii::app()->language))); ?>
                                </div>   
                                  
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'contacto',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('contacto', Yii::app()->language))); ?>
                                </div> 
                                <div class="form-group col-md-5">
                                  <?php echo $form2->textField($model2,'email',array('class'=>'form-control','placeholder'=>'E-mail')); ?>
                                </div>  
                                <div class="form-group col-md-3">
                                  <?php echo $form2->textField($model2,'contribuinte',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('contribuinte', Yii::app()->language))); ?>
                                </div>   
                                  
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'data_compra',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('data_compra', Yii::app()->language))); ?>
                                </div> 
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'data_servico',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('data_entrada_servico', Yii::app()->language))); ?>
                                </div>  
                                <div class="form-group col-md-4">
                                  <?php echo $form2->textField($model2,'concessionario',array('class'=>'form-control','placeholder'=>Content::model()->getContentReturn('concessionario', Yii::app()->language))); ?>
                                </div>   
                              </div>
                              <br>
                              <div class="form-group">
                                    <div class="checkbox">
                                      <label>
                                        <?php echo $form2->checkBox($model2,'check'); ?>  
                                        <?php Content::model()->getContentEcho('autorizo', Yii::app()->language);?>
                                      </label>
                                  </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-sm"><?php Content::model()->getContentEcho('enviar_certificado', Yii::app()->language);?></button>
                                </div>
                              <?php $this->endWidget(); ?>
                              <?php endif; ?>
                       <br><br>  
                            
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="doctec">
                        <div class="col-md-9">
                            <br>
                            <br>
                            <p>
                                <?php Content::model()->getContentEcho('para_dar_conhecer', Yii::app()->language);?>
                            </p>
                            <br><br>
                        </div>
                        
                        <br><br>
                        
                    </div>
                    
                    
                    
                </div>         
                        
            </div>

        </div>
<br>
<br>

<script type="text/javascript">
$(document).ready(function(){
    $( "#registo_link" ).click(function() {
        setTimeout(
        function() 
        {
          $("#ContactGarantia_registoinput").val("true");
          $("#ContactPecas_pecasinput").val("false");
        }, 500);
        
    });
     $( "#pecas_link" ).click(function() {
        setTimeout(
        function() 
        {
          $("#ContactGarantia_registoinput").val("false");
          $("#ContactPecas_pecasinput").val("true");
        }, 500);
        
    });
});
</script>