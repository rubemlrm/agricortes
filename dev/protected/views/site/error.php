<?php
$this->pageTitle=Yii::app()->name . ' - Error '.$code;
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <img src="<?php echo Yii::app()->baseUrl; ?>/images/grandes-empresas.jpg" width="100%">
    </div>
</div>
<div class="container branco">
    <div class="texto">
                
            <h1>Error <?php echo $code; ?></h1>
            <p><?php echo CHtml::encode($message); ?></p>
            <a class="btn btn-primary btn-large" href=<?php echo Yii::app()->baseUrl; ?>>
    Voltar ao inicio
    </a>
                
             
            </div>
        </div>
    </div>
</div>

<br>
<br>