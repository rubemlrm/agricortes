<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span6','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'titulo_en',array('class'=>'span6','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'titulo_fr',array('class'=>'span6','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span6','maxlength'=>50)); ?>

	<?php echo $form->dropDownListRow($model,'posicao',array('top'=>'top','right'=>'right','bottom'=>'bottom','left'=>'left')); ?>

        <?php echo $form->dropDownListRow($model,'target',array('_self'=>'_self','_blank'=>'_blank')); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span8','maxlength'=>200)); ?>


        <div class='control-group'>
        <?php echo $form->labelEx($model,'pagina_id',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        echo $form->dropDownList($model,'pagina_id',Pagina::model()->getAllName(),array('class'=>'span4','prompt'=>'Escolha a página'));
        ?>
        </div></div>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'menu_main',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        if($model->isNewRecord)
            echo $form->dropDownList($model,'menu_main',Menu::model()->getAllName(),array('class'=>'span4','prompt'=>'Escolha o menu'));
        else
            echo $form->dropDownList($model,'menu_main',Menu::model()->getAllNameExclude($model->id),array('class'=>'span4','prompt'=>'Escolha o menu'));
        
        ?>
        </div></div>

	<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span1')); ?>

	<?php echo $form->dropDownListRow($model,'visivel',array(1=>'Sim',0=>'Não')); ?>
        
        <?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'imagem',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'imagem');
        echo"</div></div>";
        ?>
        
        <?php
            if(!$model->isNewRecord)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
