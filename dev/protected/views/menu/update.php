<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Menu '.$model->titulo_pt,
    'headerIcon' => 'icon-list',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir Menus', 'url'=>Yii::app()->baseUrl.'/menu/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar menu ".$model->titulo_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>