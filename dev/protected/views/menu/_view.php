<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_en')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_fr')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_es')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posicao')); ?>:</b>
	<?php echo CHtml::encode($data->posicao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link')); ?>:</b>
	<?php echo CHtml::encode($data->link); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pagina_id')); ?>:</b>
	<?php echo CHtml::encode($data->pagina_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menu_main')); ?>:</b>
	<?php echo CHtml::encode($data->menu_main); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordem')); ?>:</b>
	<?php echo CHtml::encode($data->ordem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visivel')); ?>:</b>
	<?php echo CHtml::encode($data->visivel); ?>
	<br />

	*/ ?>

</div>