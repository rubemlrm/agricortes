<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'titulo_en',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'titulo_fr',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'posicao',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'pagina_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'menu_main',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'visivel',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
