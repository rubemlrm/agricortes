<?php
$this->breadcrumbs=array(
	'Area Atividade Imgs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List AreaAtividadeImg','url'=>array('index')),
array('label'=>'Manage AreaAtividadeImg','url'=>array('admin')),
);
?>

<h1>Create AreaAtividadeImg</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>