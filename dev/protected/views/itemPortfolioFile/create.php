<?php
$this->breadcrumbs=array(
	'Item Portfolio Files'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ItemPortfolioFile','url'=>array('index')),
array('label'=>'Manage ItemPortfolioFile','url'=>array('admin')),
);
?>

<h1>Create ItemPortfolioFile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>