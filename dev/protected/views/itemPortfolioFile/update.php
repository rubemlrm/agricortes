<?php
$this->breadcrumbs=array(
	'Item Portfolio Files'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ItemPortfolioFile','url'=>array('index')),
	array('label'=>'Create ItemPortfolioFile','url'=>array('create')),
	array('label'=>'View ItemPortfolioFile','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ItemPortfolioFile','url'=>array('admin')),
	);
	?>

	<h1>Update ItemPortfolioFile <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>