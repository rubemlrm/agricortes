<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar categoria '.$model->titulo_pt,
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir categorias downloads', 'url'=>Yii::app()->baseUrl.'/categoriaDownload/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar categoria ".$model->titulo_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>