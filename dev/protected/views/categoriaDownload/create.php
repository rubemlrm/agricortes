<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Adicionar categoria download',
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir categorias downloads', 'url'=>Yii::app()->baseUrl.'/categoriaDownload/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Adicionar categoria download";

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>