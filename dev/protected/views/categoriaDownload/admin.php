<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Categorias Downloads',
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/categoriaDownload/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Categorias Downloads";


?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                
		'titulo_pt',
		'titulo_es',
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons'=>array(
                                        'update' => array
                                        (
                                        //    'url'=>'Yii::app()->getController()->createUrl("/categoria/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                        //    'url'=>'Yii::app()->getController()->createUrl("/categoria/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>
