<?php $this->beginContent('//layouts/main'); ?>
<?php
$page=$action="";
$page = Yii::app()->getController()->id;
$action = Yii::app()->getController()->getAction()->id;

$pagina = $page."/".$action;

$this->widget('bootstrap.widgets.TbNavbar', array(
'type'=>null, // null or 'inverse'
'brand'=>Yii::app()->name,
'brandUrl'=>'#',
  
'collapse'=>true, // requires bootstrap-responsive.css
'items'=>array(
array(
'class'=>'bootstrap.widgets.TbMenu',
'items'=>array(
array('label'=>'Home', 'url'=>Yii::app()->baseUrl.'/site/dashboard', 'active'=>($pagina==="site/dashboard") ? true : false),
 
array('label'=>'Estrutura', 'url'=>'#', 'items'=>array(
    array('label'=>'Menus', 'url'=>Yii::app()->baseUrl.'/menu/admin', 'active'=>($page==="menu") ? true : false),
    array('label'=>'Páginas', 'url'=>Yii::app()->baseUrl.'/pagina/admin', 'active'=>($page==="pagina") ? true : false),    
    array('label'=>'Conteúdos', 'url'=>Yii::app()->baseUrl.'/content/admin', 'active'=>($page==="content") ? true : false),
)),    
    
array('label'=>'Portfólio', 'url'=>'#', 'items'=>array(
    array('label'=>'Áreas de Atividade', 'url'=>Yii::app()->baseUrl.'/AreaAtividade/admin', 'active'=>($page==="AreaAtividade") ? true : false),
    array('label'=>'Categorias', 'url'=>Yii::app()->baseUrl.'/categoria/admin', 'active'=>($page==="categoria") ? true : false),
    array('label'=>'Máquinas', 'url'=>Yii::app()->baseUrl.'/maquina/admin', 'active'=>($page==="maquina") ? true : false),   
/*
'---',
array('label'=>'NAV HEADER'),
array('label'=>'Separated link', 'url'=>'#'),
array('label'=>'One more separated link', 'url'=>'#'),*/
)),   
    
array('label'=>'Multimédia', 'url'=>'#', 'items'=>array(
    array('label'=>'Slider', 'url'=>Yii::app()->baseUrl.'/slider/admin', 'active'=>($page==="slider") ? true : false),
    array('label'=>'Gestão de Galerias', 'url'=>Yii::app()->baseUrl.'/itemPortfolioImg/admin', 'active'=>($page==="itemPortfolioImg") ? true : false),
    array('label'=>'Gestão de Ficheiros', 'url'=>Yii::app()->baseUrl.'/ficheiro/admin', 'active'=>($page==="ficheiro") ? true : false),    
)),   
array('label'=>'Eventos', 'url'=>Yii::app()->baseUrl.'/evento/admin', 'active'=>($page==="evento") ? true : false),   
array('label'=>'Pedidos', 'url'=>Yii::app()->baseUrl.'/pedido/admin', 'active'=>($page==="pedido") ? true : false),
array('label'=>'Downloads', 'url'=>Yii::app()->baseUrl.'/download/admin', 'active'=>($page==="download") ? true : false),
array('label'=>'Cat. Downloads', 'url'=>Yii::app()->baseUrl.'/categoriaDownload/admin', 'active'=>($page==="categoriaDownload") ? true : false),    
),
),
array(
'class'=>'bootstrap.widgets.TbMenu',
'htmlOptions'=>array('class'=>'pull-right'),
'items'=>array(
    array('icon'=>'icon-off','label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>Yii::app()->baseUrl.'/site/logout'),

),
),
),
));

?>
           

    <div class="container">
      <div class="row-fluid" style="padding-top: 60px;">
        <div class="span3">
          <div class="well sidebar-nav">
              <?php
            $this->widget('bootstrap.widgets.TbMenu', array(
            'type'=>'list',
            'items' => array(
         
            array('label'=>'Perfil de Utilizador', 'itemOptions'=>array('class'=>'nav-header')),
            array('icon'=>'icon-eye-open','label'=>'Ver Detalhes', 'url'=>Yii::app()->baseUrl.'/user/'.Yii::app()->user->id, 'active'=>($page==="user" && $action==='view' && Yii::app()->user->id==$_GET['id']) ? true : false),
            array('icon'=>'icon-pencil','label'=>'Editar Dados', 'url'=>Yii::app()->baseUrl.'/user/update/'.Yii::app()->user->id, 'active'=>($page==="user" && $action==='update' && Yii::app()->user->id==$_GET['id']) ? true : false),
            array('icon'=>'icon-asterisk','label'=>'Alterar Password', 'url'=>Yii::app()->baseUrl.'/user/changePass/'.Yii::app()->user->id, 'active'=>($page==="user" && $action==='changePass' && Yii::app()->user->id==$_GET['id']) ? true : false),
            //'',
            //array('label'=>'Logout', 'url'=>'index.php?r=site/logout'),
            //'',    
            array('label'=>'Configurações', 'itemOptions'=>array('class'=>'nav-header')),
            array('icon'=>'icon-globe','label'=>'Definições do Website', 'url'=>Yii::app()->baseUrl.'/website/view/1'),    
            array('icon'=>'icon-group','label'=>'Gestão de Utilizadores', 'url'=>Yii::app()->baseUrl.'/user/admin'),
            array('icon'=>'icon-envelope','label'=>'Contactos', 'url'=>Yii::app()->baseUrl.'/contactos/view/1'),
            '',
            array('icon'=>'icon-off','label'=>'Logout', 'url'=>Yii::app()->baseUrl.'/site/logout'),    
            )
                
            
            ));
            ?>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <!--<div class="hero-unit">
            <h1>Hello, world!</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
            <p><a href="#" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
          </div>-->
          
          <?php
        
          echo $content; ?>
          
         
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; <?php echo Yii::app()->name."&nbsp;".date("Y"); ?></p>
      </footer>

    </div><!--/.fluid-container-->
 
<?php $this->endContent(); ?>    