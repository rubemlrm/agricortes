<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php $description="description_".Yii::app()->language; echo Website::model()->findByPk(1)->$description ?>">
    <meta name="keywords" content="<?php $keywords="keywords_".Yii::app()->language; echo Website::model()->findByPk(1)->$keywords ?>">
    <meta name="author" content="Virtualnet">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>-->
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom CSS for the 'Full' Template -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/agricortes.css" rel="stylesheet">
    <?php Yii::app()->clientScript->scriptMap = array('bootstrap-combined.no-icons.min.css' => false); ?>
    
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    
<?php
//Analisar o IP de origem

$location = @file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);
if($location!=null)
{
$pais=explode("country_code",$location);
$pais=substr($pais[1], 3, 2);
if($pais=="ES")
    Yii::app()->language="es";
}
/*elseif($pais=="ES")
    Yii::app()->language="es";
else
    Yii::app()->language="pt";*/
//-----------------------

$popup=Pagina::model()->findAllByAttributes(array('slug_pt'=>'popup','visivel'=>1));
$conteudo="conteudo_".Yii::app()->language;
if($popup!=null)
{
?>
<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <?php 
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$popup[0]->imagem.'" width="100%">';  
        echo "<p>".$popup[0]->$conteudo."</p>";
        ?>
      </div>
     
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
}
?>
    
<nav id="top" class="navbar navbar-default navbar-fixed-top hidden-xs" role="navigation">
  <div class="container">
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-right:300px">
        <form class="navbar-form navbar-right" role="search">
            <p id="idioma">
                <!--<a href="#">PT</a> | <a href="#">ES</a> | <a href="#">EN</a>-->
            <?php 
                $this->widget('application.components.widgets.LanguageSelector');
            ?>
            </p>
        </form>
        
        <?php
    
        if(Yii::app()->user->id==null)
        {
        ?>
        <form name="pesquisa" class="navbar-form navbar-right" role="search" action="<?php echo Yii::app()->baseUrl ?>/site/login2" method="post">
            <button type="submit" class="btn btn-default btn-xs"><?php Content::model()->getContentEcho('area_reservada', Yii::app()->language); ?></button>
        </form>
        <?php
        }
        else
        {
        ?>
        <form name="pesquisa" class="navbar-form navbar-right" role="search" action="<?php echo Yii::app()->baseUrl ?>/site/AreaReservada" method="post">
            <button type="submit" class="btn btn-default btn-xs"><?php Content::model()->getContentEcho('area_reservada', Yii::app()->language); ?></button>
        </form>
         <?php
        if(!(Yii::app()->user->isGuest))
        {
        ?>
        <form name="pesquisa" class="navbar-form navbar-right" role="search" action="<?php echo Yii::app()->baseUrl ?>/site/logout" method="post">
            <button type="submit" class="btn btn-default btn-xs">Logout</button>
        </form>
        
        <?php
        }
        }
        ?>
        
        <form name="pesquisa" class="navbar-form navbar-right" role="search" action="<?php echo Yii::app()->baseUrl ?>/site/pesquisa" method="post">
            <div class="form-group">
              <input name="pesquisatxt" type="text" class="form-control input-sm" placeholder="<?php Content::model()->getContentEcho('pesquisar', Yii::app()->language); ?>">
            </div>
            <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-search"></i></button>
        </form>
        
        <form class="navbar-form navbar-right" id="social">
            <a href="http://www.facebook.com/agricortes.sa" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>
            <a href="http://www.youtube.com/user/AGRICORTES" target="_blank"><i class="fa fa-youtube-square fa-lg"></i></a>
            <a href="http://www.linkedin.com/pub/agricortes-s-a/8b/3a7/116" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a>
            <a href="mailto:agricortes@agricortes.com" target="_blank"><i class="fa fa-envelope fa-lg"></i></a>
        </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!--<div class="container">
    <div class="hidden-xs col-md-offset-11 col-md-1">
    <img class="club" src="<?php //echo Yii::app()->baseUrl; ?>/images/club.png">
    </div>
</div>-->
<br class="hidden-xs">
    <div class="container-fluid nopadding visible-xs">
        <div class="col-sx-12">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <?php
                                $Criteria = new CDbCriteria();
                                $Criteria->condition = "posicao = 'top'";
                                $Criteria->condition = "visivel = 1";
                                $Criteria->order = "ordem ASC";
                                $menu = Menu::model()->findAll($Criteria);
                                $titulo="titulo_".Yii::app()->language;
                                $slug="slug_".Yii::app()->language;
                                
                                foreach ($menu as $m)
                                {
                                    if($m->pagina_id!=null){
                                        
                                           if($m->menu_main==null){
                                               $menu_dependente=Menu::model()->findAllByAttributes(array('menu_main'=>$m->id));
                                               if($menu_dependente==null){
                                                   $pagina = Pagina::model()->findByPk($m->pagina_id);
                                                   if($pagina->visivel==1)
                                                   echo '<li><a class="link" href="'.Yii::app()->baseUrl.'/pagina/'.$pagina->$slug.'">'.mb_strtoupper($m->$titulo,"UTF-8").'</a></li>';
                                               }
                                               else{
                                                 echo'<li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.mb_strtoupper($m->$titulo,"UTF-8").' <b class="caret"></b></a>
                                                <ul class="dropdown-menu" style="width:100%;">';
                                                $Criteria2 = new CDbCriteria();
                                                $Criteria2->condition = "menu_main = ".$m->id;
                                                $Criteria2->order="ordem ASC";
                                                $menu2 = Menu::model()->findAll($Criteria2);
                                                foreach($menu2 as $menu2_aux)
                                                {
                                                    $pagina_aux = Pagina::model()->findByPk($menu2_aux->pagina_id);
                                                    if($pagina_aux->visivel==1)
                                                    echo'<li>'.CHtml::link(mb_strtoupper($menu2_aux->$titulo,"UTF-8"),Yii::app()->baseUrl."/pagina/".$pagina_aux->$slug,array('style'=>'color:#6E6E6E')).'</li>';
                                                }    

                                                echo'</ul>
                                              </li>';   
                                               }
                                           }
                                    
                                    }
                                    elseif($m->id==2){
                                    $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
                                    $nome="nome_".Yii::app()->language;
                                    $slug="slug_".Yii::app()->language;
                                    
                                    echo'<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.mb_strtoupper($m->$titulo,"UTF-8").' <b class="caret"></b></a>
                                    <ul class="dropdown-menu">';
                                    foreach($area_atividade as $area)
                                    {
                                        echo'<li>'.CHtml::link(mb_strtoupper($area->$nome,"UTF-8"),Yii::app()->baseUrl."/AreaAtividade/".$area->$slug,array('style'=>'color:#6E6E6E')).'</li>';
                                    }    
                                   
                                    echo'</ul>
                                  </li>';    
                                    }
                                    else
                                        echo '<li><a href="'.Yii::app()->baseUrl.'/'.$m->link.'">'.mb_strtoupper($m->$titulo,"UTF-8").'</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
        </div>
    </div>

    <div class="container-fluid nopadding hidden-xs">
        <div class="col-sx-12">
                <nav class="navbar" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <br class="hidden-md">
                            <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl; ?>/index.php"><img src="<?php echo Yii::app()->baseUrl.'/images/logo_'.Yii::app()->language.'.png' ?>" width="280px"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav navbar-right"  id="nav_principal">
                                <?php
                                $Criteria = new CDbCriteria();
                                $Criteria->condition = "posicao = 'top' AND visivel = 1";
                                $Criteria->order = "ordem ASC";
                                $menu = Menu::model()->findAll($Criteria);
                                $titulo="titulo_".Yii::app()->language;
                                $slug="slug_".Yii::app()->language;
                                //var_dump(Yii::app()->controller->id);
                                foreach ($menu as $m)
                                {
                                    
                                    $titulo_site=explode(" - ",$this->pageTitle);
                                    
                                    
                                    if(sizeof($titulo_site)>1)
                                    {
                                       
                                        if(mb_strtoupper($titulo_site[1],"UTF-8") === mb_strtoupper($m->$titulo,"UTF-8"))
                                            $class="menu_selected";
                                        else
                                            $class="";
                                    }
                                    else{
                                            $class="";
                                    }
                                    
                                    
                                    if($m->pagina_id!=null){
                                        
                                           if($m->menu_main==null){
                                               $menu_dependente=Menu::model()->findAllByAttributes(array('menu_main'=>$m->id));
                                               if($menu_dependente==null){
                                                   $pagina = Pagina::model()->findByPk($m->pagina_id);
                                                   if($pagina->visivel==1)
                                                   echo '<li><a class="link '.$class.'" href="'.Yii::app()->baseUrl.'/pagina/'.$pagina->$slug.'">'.mb_strtoupper($m->$titulo,"UTF-8").'</a></li>';
                                               }
                                                else{
                                                 echo'<li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.mb_strtoupper($m->$titulo,"UTF-8").' <b class="caret"></b></a>
                                                <ul class="dropdown-menu" style="width:200%;">';
                                                $Criteria2 = new CDbCriteria();
                                                $Criteria2->condition = "menu_main = ".$m->id;
                                                $Criteria2->order="ordem ASC";
                                                $menu2 = Menu::model()->findAll($Criteria2);
                                               
                                                foreach($menu2 as $menu2_aux)
                                                {
                                                    $pagina_aux = Pagina::model()->findByPk($menu2_aux->pagina_id);
                                                    if($pagina_aux->visivel==1)
                                                    echo'<li>'.CHtml::link(mb_strtoupper($menu2_aux->$titulo,"UTF-8"),Yii::app()->baseUrl."/pagina/".$pagina_aux->$slug,array('style'=>'color:#6E6E6E')).'</li>';
                                                }    

                                                echo'</ul>
                                              </li>';   
                                               }
                                           }
                                    
                                    }
                                    elseif($m->id==2){
                                      
                                    $area_atividade=AreaAtividade::model()->findAll(array('order'=>'ordem ASC'));
                                    $nome="nome_".Yii::app()->language;
                                    $slug="slug_".Yii::app()->language;
                                    
                                    echo'<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.mb_strtoupper($m->$titulo,"UTF-8").' <b class="caret"></b></a>
                                    <ul class="dropdown-menu" style="width:100%;">';
                                    foreach($area_atividade as $area)
                                    {
                                        echo'<li>'.CHtml::link(mb_strtoupper($area->$nome,"UTF-8"),Yii::app()->baseUrl."/AreaAtividade/".$area->$slug,array('style'=>'color:#6E6E6E')).'</li>';
                                    }    
                                   
                                    echo'</ul>
                                  </li>';    
                                    }
                                      else
                                        echo '<li><a class="link '.$class.'" href="'.Yii::app()->baseUrl.'/'.$m->link.'">'.mb_strtoupper($m->$titulo,"UTF-8").'</a></li>';
                                        
                                    
                                }
                                ?>
                               
                            </ul>
                        </div>
                    </div>
                </nav>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col-sm-5 visible-xs">
            <img class="logo" src="<?php echo Yii::app()->baseUrl.'/images/logo_'.Yii::app()->language.'.png' ?>" width="100%">
        </div>
    </div>
</div>

<br>

<div class="container hidden-xs">
    <div class="col-md-offset-10 col-md-2" id="fader">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/atencao.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/confianca.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/excelencia.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/experiencia.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/inovacao.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/integridade.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/qualidade.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/rigor.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/servico.png'; ?>">
        <img class="dedicacao" src="<?php echo Yii::app()->baseUrl.'/images/clip/'.Yii::app()->language.'/tecnologia.png'; ?>">
    </div>
</div>

<?php echo $content; ?>
<div class="container-fluid foot">
    <div class="row">
    <div class="col-md-offset-1 col-md-8">
        <p>AGRICORTES SA - <?php echo date("Y"); ?> - <?php Content::model()->getContentEcho('direitos', Yii::app()->language); ?></p>
    </div> 
    <div class="col-md-3">
        <p><a href="http://www.virtualnet.pt" target="_blank">Virtualnet</a>&nbsp;made it</p>
    </div>
    </div>
</div>
        <!-- JavaScript -->
    
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/bootstrap.js"></script>
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });
   
    </script>
    
    <script type="text/javascript">
    
        
        
        $(function() {
              $('.link').click(function() {
                  $(this).addClass("menu_selected");
                    //$('#link').css('backgroundImage', 'url(images/tabs3.png)');
                   
              });
        })
       
    $(document).ready(function(){  
        $("a[rel^='prettyPhoto']").prettyPhoto();
        
        <?php
        if(!isset($_SESSION['slider'])){
            echo"$('.modal').modal('show');";
            echo"if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {";
            echo "window.location.replace('http://novo.agricortes.com/pagina/contactos');";
            echo"}";
            
            $_SESSION['slider']=true;
        }
        ?>
        
        $(function() {
            $('#fader img:not(:first)').hide();
            $('#fader img').css('position', 'absolute');
            $('#fader img').css('top', '-18px');
            $('#fader img').css('left', '50%');
            $('#fader img').each(function() {
                var img = $(this);
                $('<img>').attr('src', $(this).attr('src')).load(function() {
                    img.css('margin-left', -this.width / 2 + 'px');
                });
            });

            var pause = false;

            function fadeNext() {
                $('#fader img').first().fadeOut().appendTo($('#fader'));
                $('#fader img').first().fadeIn(200);
            }

            function fadePrev() {
                $('#fader img').first().fadeOut();
                $('#fader img').last().prependTo($('#fader')).fadeIn(200);
            }

            $('#fader, #next').click(function() {
                fadeNext();
            });

            $('#prev').click(function() {
                fadePrev();
            });

            $('#fader, .button').hover(function() {
                pause = true;
            },function() {
                pause = false;
            });

            function doRotate() {
                if(!pause) {
                    fadeNext();
                }    
            }

            var rotate = setInterval(doRotate, 3000);
        });
        
        
        $('.link_cat').click(function() {
        var currentId = $(this).attr('id');
        var currentDiv = "#div"+currentId;
        $('.divlink').css("display","none");
        
        $(currentDiv).css("display","inline");
        return false;
    });
   /*
            <?php if (Yii::app()->controller->id==="categoria"): ?>
            $('html,body').animate({ scrollTop: $('.zona_categorias').offset().top }, 'slow');
            <?php endif; ?>
            <?php if (Yii::app()->controller->id==="areaatividade"): ?>
            $('html,body').animate({ scrollTop: $('.dedicacao').offset().top }, 'slow');
            <?php endif; ?>    
    */
}); 
       
    </script>
<!-- LiveZilla Chat Button Link Code (ALWAYS PLACE IN BODY ELEMENT) --><!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) --><div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://agricortes.com/livezilla/server.php?a=c1b65&request=track&output=jcrpt&fbpos=02&fbml=0&fbmt=0&fbmr=20&fbmb=0&fbw=112&fbh=32&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><img src="http://agricortes.com/livezilla/server.php?a=c1b65&amp;request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript><!-- http://www.LiveZilla.net Tracking Code --><div style="display:none;"><a href="javascript:void(window.open('http://agricortes.com/livezilla/chat.php?a=0d8e1','','width=590,height=760,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))" class="lz_fl"><img id="chat_button_image" src="http://agricortes.com/livezilla/image.php?a=66536&amp;id=1&amp;type=overlay" width="112" height="32" style="border:0px;" alt="LiveZilla Live Chat Software"></a></div><!-- http://www.LiveZilla.net Chat Button Link Code -->
</body>

</html>
