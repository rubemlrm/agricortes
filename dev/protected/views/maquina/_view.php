<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoria_id')); ?>:</b>
	<?php echo CHtml::encode($data->categoria_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_en')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtitulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->subtitulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtitulo_en')); ?>:</b>
	<?php echo CHtml::encode($data->subtitulo_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagem_capa')); ?>:</b>
	<?php echo CHtml::encode($data->imagem_capa); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ordem')); ?>:</b>
	<?php echo CHtml::encode($data->ordem); ?>
	<br />

	*/ ?>

</div>