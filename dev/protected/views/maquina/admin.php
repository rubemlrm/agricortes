<style>
    .grid-view .button-column{width:70px;}
</style>
<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Máquinas',
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/maquina/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Máquinas";

?>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
                array(
                    'name'=>'categoria_id',
                    'type'=>'raw',
                    'value'=>'Categoria::model()->findByPk($data->categoria_id)->nome_pt',
                    //'value'=>'$data->categoria_id',
                ),
		
		'titulo_pt',
		'subtitulo_pt',
array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {updateimg} {delete}',
			'buttons'=>array(
                                        'view' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/maquina/view",array("id"=>$data->id))',

                                        ),
                                        'update' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/maquina/update",array("id"=>$data->id))',

                                        ),
                                        'updateimg' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/maquina/updateimg",array("id"=>$data->id))',
                                            'label'=>'Gerir Imagens',

                                        ),
                                        'delete' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/maquina/delete",array("id"=>$data->id))',

                                        ),
					),
		),
),
)); 


$this->endWidget();
?>


</div>
</div>

