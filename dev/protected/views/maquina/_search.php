<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'categoria_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'titulo_en',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'subtitulo_pt',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'subtitulo_en',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'imagem_capa',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
