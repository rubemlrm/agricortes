<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Máquina '.$model->titulo_pt,
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar Imagens', 'url'=>Yii::app()->baseUrl.'/maquina/updateimg/'.$model->id),    
    array('label'=>'Gerir Máquinas', 'url'=>Yii::app()->baseUrl.'/maquina/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Máquina ".$model->titulo_pt;

?>
<script>
    $( document ).ready(function() {
        $(".errorSummary").addClass("alert alert-block alert-error");
    });
</script>
<?php

/*
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
));
*/
$form = $this->beginWidget('CActiveForm', array(
          'id' => 'somemodel-form',
          'enableAjaxValidation' => false,
          'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model);



$this->widget('bootstrap.widgets.TbTabs', array(
	    'type'=>'pills',
	    'encodeLabel'=>false,
	    'tabs'=>array(
	        array(
		        'label'=>'<div class="ico"><i class="icon-cog icon-white"></i>&nbsp;Geral</div>', 
		        'content'=>$this->renderPartial('_form', array('form'=>$form,'model'=>$model), true),
		        'active'=>true
	        ),
                array(
		        'label'=>'<div class="ico"><i class="icon-plus-sign icon-white"></i>&nbsp;Detalhe</div>', 
		        'content'=>$this->renderPartial('_detalhe', array('form'=>$form,'model'=>$model), true),
	        ),
                /*array(
		        'label'=>'<div class="ico"><i class="icon-picture icon-white"></i>&nbsp;Imagens</div>', 
		        'content'=>$this->renderPartial('_imagens', array('form'=>$form,'model2'=>$model2,'model3'=>$model3,'model'=>$model), true),
		    ),*/
	    ),
	)); 

?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget();



$this->endWidget();
?>


</div>
</div>