<?php
$this->breadcrumbs=array(
	'Item Portfolios',
);

$this->menu=array(
array('label'=>'Create ItemPortfolio','url'=>array('create')),
array('label'=>'Manage ItemPortfolio','url'=>array('admin')),
);
?>

<h1>Item Portfolios</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
