        <div class='control-group'>
        <?php echo $form->labelEx($model,'categoria_id',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->dropDownList($model,'categoria_id',Categoria::model()->getAllNameNotMain(),array('class'=>'span4','prompt'=>'Escolha a categoria')); ?>
        </div></div>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'destaque',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->dropDownList($model,'destaque',array(1=>'Sim',0=>'Não'),array('class'=>'span4')); ?>
        </div></div>

        <?php echo $form->labelEx($model,'titulo_pt'); ?>
        <?php echo $form->textField($model,'titulo_pt',array('class'=>'span8','maxlength'=>100)); ?>
        <?php //echo $form->error($model,'titulo_pt'); ?>

        <?php echo $form->labelEx($model,'titulo_es'); ?>
        <?php echo $form->textField($model,'titulo_es',array('class'=>'span8','maxlength'=>100)); ?>
        <?php //echo $form->error($model,'titulo_es'); ?>

        <?php //echo $form->labelEx($model,'subtitulo_pt'); ?>
        <?php //echo $form->textField($model,'subtitulo_pt',array('class'=>'span6','maxlength'=>100)); ?>

        <?php //echo $form->labelEx($model,'subtitulo_en'); ?>
        <?php //echo $form->textField($model,'subtitulo_en',array('class'=>'span6','maxlength'=>100)); ?>

        <?php //echo $form->labelEx($model,'subtitulo_es'); ?>
        <?php //echo $form->textField($model,'subtitulo_es',array('class'=>'span6','maxlength'=>100)); ?>

        <?php echo $form->labelEx($model,'link_video'); ?>
        <?php echo $form->textField($model,'link_video',array('class'=>'span4','maxlength'=>200)); ?>

        <?php echo $form->labelEx($model,'link_website'); ?>
        <?php echo $form->textField($model,'link_website',array('class'=>'span8','maxlength'=>200)); ?>


	<?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'imagem_capa',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'imagem_capa');
        echo"</div></div>";
        
        ?>
        <?php
            if(!$model->isNewRecord && $model->imagem_capa!="" && $model->imagem_capa!=null)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>

        <?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'file',array('class'=>'control-label'));
        echo"<div class='controls'>";
        $this->widget('CMultiFileUpload', array(
                'name' => 'files',
                'accept' => 'pdf|PDF', // useful for verifying files
                'duplicate' => 'Duplicate file!', // useful, i think
                'denied' => 'Invalid file type', // useful, i think
            ));
        echo"</div></div>";
        ?>
        <?php
            $files=ItemPortfolioFile::model()->findAllByAttributes(array('id_item_portfolio'=>$model->id)); 
            if(!$model->isNewRecord && $files!=null)
            {
            echo '<div id="data">';    
            $this->renderPartial('_fileContent', array('model'=>$model,'files'=>$files));
            echo '</div>';
            }
        ?>

        <?php echo $form->labelEx($model,'ordem'); ?>
        <?php echo $form->textField($model,'ordem',array('class'=>'span1')); ?>        