<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar área de atividade '.$model->nome_pt,
    'headerIcon' => 'icon-folder-open',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir áreas de atividade', 'url'=>Yii::app()->baseUrl.'/AreaAtividade/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar área de atividade ".$model->nome_pt;
?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>