<?php
$this->breadcrumbs=array(
	'Ficheiros'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Ficheiro','url'=>array('index')),
array('label'=>'Create Ficheiro','url'=>array('create')),
array('label'=>'Update Ficheiro','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Ficheiro','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Ficheiro','url'=>array('admin')),
);
?>

<h1>View Ficheiro #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'src',
),
)); ?>
