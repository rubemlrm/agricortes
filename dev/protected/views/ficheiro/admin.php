<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Gestão de Ficheiros',
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Adicionar', 'url'=>Yii::app()->baseUrl.'/ficheiro/create'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Gestão de Ficheiros";

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'publicidade-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'titulo_pt',
		array(
                    'name'=>'src',
                    'type'=>'raw',
                    'filter'=>false,
                    'value'=>'(Ficheiro::model()->fileType($data->id))? "N/A" : CHtml::image(Yii::app()->request->baseUrl."/uploads/gestor/".$data->src,"src",array("width"=>100,"class"=>"img-polaroid"))',
                ),
                
		
        array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{download} {update} {delete}',
			'buttons'=>array(
                                        'download' => array
                                        (
                                            'url'=>'Yii::app()->getController()->createUrl("/Ficheiro/download",array("id"=>$data->id))',

                                        ),
                            
                                        'update' => array
                                        (
                                            //'url'=>'Yii::app()->getController()->createUrl("/itemPortfolio/update",array("id"=>$data->id))',

                                        ),
                                        'delete' => array
                                        (
                                            //'url'=>'Yii::app()->getController()->createUrl("/itemPortfolio/delete",array("id"=>$data->id))',

                                        ),
					),
		),    
    
),
));
$this->endWidget();
?>

</div>
</div>