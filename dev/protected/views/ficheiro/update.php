<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Ficheiro '.$model->titulo_pt,
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir Ficheiros', 'url'=>Yii::app()->baseUrl.'/ficheiro/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Ficheiro ".$model->titulo_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>