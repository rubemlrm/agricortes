<?php
$this->breadcrumbs=array(
	'Ficheiros',
);

$this->menu=array(
array('label'=>'Create Ficheiro','url'=>array('create')),
array('label'=>'Manage Ficheiro','url'=>array('admin')),
);
?>

<h1>Ficheiros</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
