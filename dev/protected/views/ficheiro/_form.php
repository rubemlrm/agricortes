<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

        <?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span8','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span8','maxlength'=>100)); ?>

	<?php
        echo"<div class='control-group'>";
        echo $form->labelEx($model,'src',array('class'=>'control-label'));
        echo"<div class='controls'>";
        echo CHtml::activeFileField($model, 'src');
        echo"</div></div>";
        ?>
        
        <?php
            if(!$model->isNewRecord)
            {
            echo '<div id="data2">';    
            $this->renderPartial('_imagemContent', array('model'=>$model));
            echo '</div>';
            }
        ?>

	

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
