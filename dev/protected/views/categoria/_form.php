<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'categoria-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>
        
        <div class='control-group'>
        <?php echo $form->labelEx($model,'area_atividade_id',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
            echo $form->dropDownList($model,'area_atividade_id',AreaAtividade::model()->getAllName(),array('class'=>'span4','prompt'=>'Escolha a área de atividade'));
        ?>
        </div></div>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'categoria_main',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php
        if($model->isNewRecord)
            echo $form->dropDownList($model,'categoria_main',Categoria::model()->getAllNameMain(),array('class'=>'span4','prompt'=>'Escolha a categoria'));
        else
            echo $form->dropDownList($model,'categoria_main',Categoria::model()->getAllNameExclude($model->id),array('class'=>'span4','prompt'=>'Escolha a categoria'));
        
        ?>
        </div></div>
        
	<?php echo $form->textFieldRow($model,'nome_pt',array('class'=>'span6','maxlength'=>100)); ?>

	<?php //echo $form->textFieldRow($model,'nome_en',array('class'=>'span6','maxlength'=>100)); ?>

        <?php //echo $form->textFieldRow($model,'nome_fr',array('class'=>'span6','maxlength'=>100)); ?>

        <?php echo $form->textFieldRow($model,'nome_es',array('class'=>'span6','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'ordem',array('class'=>'span1')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
