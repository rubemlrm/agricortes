<style>
    .help-inline{padding-left: 20px;}
</style>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	

        <?php echo $form->textFieldRow($model,'firstname',array('class'=>'span5','maxlength'=>100)); ?>

        <?php echo $form->textFieldRow($model,'lastname',array('class'=>'span5','maxlength'=>100)); ?>

        <?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>100)); ?>

        <?php echo $form->textFieldRow($model,'country',array('class'=>'span5','maxlength'=>100));
        
        $paises=Country::model()->findAll();
        echo $form->dropDownListRow($model,'country',CHtml::listData($paises, 'countryName', 'countryName'));

        echo $form->textFieldRow($model,'username',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php if($model->isNewRecord) echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>100)); ?>

        <?php if($model->isNewRecord) echo $form->passwordFieldRow($model,'password_repeat',array('class'=>'span5','maxlength'=>100)); ?>

        <div class='control-group'>
        <?php echo $form->labelEx($model,'roles',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->dropDownList($model,'roles',array('admin'=>'admin','user'=>'user','pendente'=>'pendente'),array('class'=>'span3','prompt'=>'Tipo de utilizador')); ?>
        </div></div>

	<?php //echo $form->textFieldRow($model,'roles',array('class'=>'span5','maxlength'=>30)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Adicionar' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
