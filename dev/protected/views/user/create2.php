<?php
$conteudo="conteudo_".Yii::app()->language;
$titulo="titulo_".Yii::app()->language;
$this->pageTitle=Yii::app()->name . ' - LOGIN';
?>
<div class="container-fluid fundo hidden-xs">
    <div class="container sombra">
        <?php
        $menu=Menu::model()->findByAttributes(array("link"=>"site/login2"));
        echo'<img src="'.Yii::app()->baseUrl.'/uploads/'.$menu->imagem.'" width="100%">';
        ?>
        
        
    </div>
</div>
<div class="container branco">
        
        <div class="col-md-8">
            
            <div class="texto">
                <h5><?php echo mb_strtoupper(Content::model()->getContentReturn('criar_conta', Yii::app()->language),"UTF-8"); ?></h5><br>
                
               <?php
               echo $this->renderPartial('_form2', array('model'=>$model));
               ?>
            </div>
        </div>
        <div class="col-md-4">
           
        </div>
       
</div>

<br>
<br>


