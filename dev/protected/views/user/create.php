<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Adicionar utilizador',
    'headerIcon' => 'icon-user',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir utilizadores', 'url'=>Yii::app()->baseUrl.'/user/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Adicionar utilizador";
$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	'Manage',
);


?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>