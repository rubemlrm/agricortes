<style>
    .help-inline{padding-left: 20px;}
</style>
<?php
    if(Yii::app()->user->hasFlash('success-register')){
        echo'<div class="alert alert-success">';
            echo Yii::app()->user->getFlash('success-register');
        echo'</div>';
        }
    else    
    {
?> 

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
	'enableClientValidation'=>true,
	
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

        <?php echo $form->textFieldRow($model, 'firstname', array('style'=>'width:60%;padding:8px;'));?>
        <?php echo $form->textFieldRow($model, 'lastname', array('style'=>'width:60%;padding:8px;'));?>
        <?php echo $form->textFieldRow($model, 'phone', array('style'=>'width:40%;padding:8px;'));?>
        <?php
        $paises=Country::model()->findAll();
        echo $form->dropDownListRow($model,'country',CHtml::listData($paises, 'countryName', 'countryName'),array('style'=>'width:40%;padding:8px;'));
        ?>
        <?php echo $form->textFieldRow($model, 'email', array('style'=>'width:40%;padding:8px;'));?>
	<?php echo $form->textFieldRow($model, 'username', array('style'=>'width:30%;padding:8px;'));?>

        <?php echo $form->passwordFieldRow($model, 'password', array('style'=>'width:30%;padding:8px;'));?>
        <?php echo $form->passwordFieldRow($model, 'password_repeat', array('style'=>'width:30%;padding:8px;'));?>

<br>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>Content::model()->getContentReturn('criar_conta', Yii::app()->language),
		)); ?>

<?php $this->endWidget(); ?>
<?php } ?>