<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Detalhes do Utilizador '.$model->username,
    'headerIcon' => 'icon-user',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Editar', 'url'=>Yii::app()->baseUrl.'/user/update/'.$model->id),    
    array('label'=>'Gerir Utilizadores', 'url'=>Yii::app()->baseUrl.'/user/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Detalhes do Utilizador ".$model->username;


?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
                
		'username',
		'email',
		'firstname',
                'lastname',
                'password',
                'phone',
                'country',
		'last_login_time',
		'create_time',
                /*array(
                    'name'=>'create_user_id',
                    'type'=>'raw',
                    'value'=>User::model()->findByPk($model->create_user_id)->firstname." (".User::model()->findByPk($model->create_user_id)->username.")",
                ),*/
		'update_time',
                /*array(
                    'name'=>'update_user_id',
                    'type'=>'raw',
                    'value'=>User::model()->findByPk($model->update_user_id)->firstname." (".User::model()->findByPk($model->update_user_id)->username.")",
                ),*/
		'roles',
    
),
));

$this->endWidget();
?>


</div>
</div>




