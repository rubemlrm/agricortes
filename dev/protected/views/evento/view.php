<?php
$conteudo="conteudo_".Yii::app()->language;
$titulo="titulo_".Yii::app()->language;
$this->pageTitle=Yii::app()->name . ' - '.$model->$titulo;
?>
<!-- begin Content -->
    <section id="single-news" >
        <!-- begin Sub Header -->
        <div class="sub-header" >
            <div class="container">
                <div class="row" >
                    <ul class="sub-header-container" >
                        <li>
                            <h3 class="title"><?php echo $model->$titulo; ?></h3>
                        </li>
                        <li>
                            <ul class="custom-breadcrumb" >
                                <li><h6><a href="index.php">Home</a></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6><?php echo CHtml::link(Content::model()->getContentReturn('evento',Yii::app()->language),array('index')); ?></h6></li>
                                <li><i class="separator entypo-play" ></i></li>
                                <li><h6><?php echo $model->$titulo; ?></h6></li>
                            </ul>                    
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- begin Sub Header -->


        <!-- begin New -->
        <article class="article-container">
            <div class="container" >
                <div class="row" >
                    <div class="col-md-9">
                        <h2 class="article-title" ><?php echo $model->$titulo; ?></h2>
                        <span class="line" >
                            <span class="sub-line" ></span>
                        </span>

                        <!-- begin Post -->
                        <section class="row">
                            <article class="post">
                                <img src="uploads/<?php echo $model->imagem; ?>" alt="//" />
                                <p></p>
                                <?php /* ?><p><label class="cap" ><?php echo substr($model->$conteudo, 0, 1); ?></label><?php echo substr($model->$conteudo, 1, strlen($model->$conteudo)); ?></p><?php */ ?>
                                <p><?php echo $model->$conteudo; ?></p>
                                <?php echo CHtml::link('<i class="entypo-left-open" ></i> '.Content::model()->getContentReturn('voltar',Yii::app()->language),array('index'),array('class'=>'readmore')); ?>  
                            </article>
                        </section>
                        <!-- end Post -->

                    </div>

                    <!-- begin Sidebar -->
                    <?php
                    $this->renderPartial('_all_news');
                    ?>
                    <!-- end Sidebar -->
                </div>
            </div>
        </article>
        <!-- end New -->
        
    </section>
    <!-- end Content -->