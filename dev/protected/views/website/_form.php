<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'contactos-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Os campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'titulo_pt',array('class'=>'span8','maxlength'=>200)); ?>

	<?php //echo $form->textFieldRow($model,'titulo_en',array('class'=>'span8','maxlength'=>200)); ?>

	<?php //echo $form->textFieldRow($model,'titulo_fr',array('class'=>'span8','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'titulo_es',array('class'=>'span8','maxlength'=>200)); ?>

	
        <div class='control-group'>
        <?php echo $form->labelEx($model,'description_pt',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->textArea($model, 'description_pt', array('maxlength' => 300, 'rows' => 6, 'cols' => 50,'class'=>'span8','id'=>'some-textarea-pt','placeholder'=>'')); ?>
        </div></div>
        <script type="text/javascript">
                $('#some-textarea-pt').wysihtml5({

                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": false, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false //Button to insert an image. Default true,

                });
        </script>    
        
        <div class='control-group'>
        <?php echo $form->labelEx($model,'description_es',array('class'=>'control-label')); ?>   
        <div class='controls'>
        <?php echo $form->textArea($model, 'description_es', array('maxlength' => 300, 'rows' => 6, 'cols' => 50,'class'=>'span8','id'=>'some-textarea-es','placeholder'=>'')); ?>
        </div></div>
        <script type="text/javascript">
                $('#some-textarea-es').wysihtml5({

                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": false, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false //Button to insert an image. Default true,

                });
        </script>


	<?php echo $form->textFieldRow($model,'keywords_pt',array('class'=>'span8','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'keywords_en',array('class'=>'span8','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'keywords_fr',array('class'=>'span8','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'keywords_es',array('class'=>'span8','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span8','maxlength'=>200)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
