<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_pt')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_en')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_fr')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_es')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_pt')); ?>:</b>
	<?php echo CHtml::encode($data->description_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_en')); ?>:</b>
	<?php echo CHtml::encode($data->description_en); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('description_fr')); ?>:</b>
	<?php echo CHtml::encode($data->description_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_es')); ?>:</b>
	<?php echo CHtml::encode($data->description_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_pt')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_en')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_fr')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywords_es')); ?>:</b>
	<?php echo CHtml::encode($data->keywords_es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	*/ ?>

</div>