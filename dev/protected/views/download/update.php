<div class="row-fluid">
    <div class="span12">

<?php
    $this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => 'Editar Download '.$model->titulo_pt,
    'headerIcon' => 'icon-file',
    'headerButtons' => array(
   
    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons'=>array(
    array('label'=>'Gerir Downloads', 'url'=>Yii::app()->baseUrl.'/download/admin'),
    ),
    ),
    )));

$this->pageTitle=Yii::app()->name.' - '."Editar Download ".$model->titulo_pt;

?>

<?php echo $this->renderPartial('_form', array('model'=>$model));

$this->endWidget();
?>


</div>
</div>