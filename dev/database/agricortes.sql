-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2014 at 05:17 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agricortes`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_atividade`
--

CREATE TABLE IF NOT EXISTS `area_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(100) NOT NULL,
  `nome_en` varchar(100) DEFAULT NULL,
  `nome_es` varchar(100) DEFAULT NULL,
  `imagem_big` varchar(200) DEFAULT NULL,
  `imagem_small` varchar(200) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `slug_pt` varchar(200) DEFAULT NULL,
  `slug_en` varchar(200) DEFAULT NULL,
  `slug_es` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `area_atividade`
--

INSERT INTO `area_atividade` (`id`, `nome_pt`, `nome_en`, `nome_es`, `imagem_big`, `imagem_small`, `ordem`, `slug_pt`, `slug_en`, `slug_es`) VALUES
(1, 'Construção', 'Construction', 'Construcción', '', '', 1, 'construcao', 'construction', 'construccion'),
(2, 'Ambiente', 'Environment', 'Ambiente', 'bd0f101efe47329e0b6c91e64e59af66f.jpg', 's377fd457baab812fa736878d92622736.JPG', 2, 'ambiente', 'environment', 'ambiente'),
(3, 'Agricultura', 'Agriculture', 'Agricultura', 'b4e13165e1102acd45d78bbad225b8b50.jpg', 'sb69965f0c83f177441198a21a378babf.jpg', 3, 'agricultura', 'agriculture', 'agricultura'),
(4, 'Floresta', 'Forest', 'Bosque', 'b1975d382520f8a2df079d96e259f5df8.jpg', 's0a10bc6623e844a8820f2d5ccac62b53.jpg', 4, 'floresta', 'forest', 'bosque'),
(5, 'Logística', 'Logistics', 'Logística', 'b6c843595b6f18a289bb1e5f28cbe7522.jpg', 's6c843595b6f18a289bb1e5f28cbe7522.JPG', 5, 'logistica', 'logistics', 'logistica'),
(6, 'Motores e peças', 'Engines and parts', 'Motores y piezas', 'be5f4e6dbc619fb72da0ac6f59d532be5.JPG', 'se5f4e6dbc619fb72da0ac6f59d532be5.jpg', 6, 'motores-e-pecas', 'engines-and-parts', 'motores-y-piezas');

-- --------------------------------------------------------

--
-- Table structure for table `area_atividade_img`
--

CREATE TABLE IF NOT EXISTS `area_atividade_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_atividade_id` int(11) NOT NULL,
  `imagem` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `area_atividade_id` (`area_atividade_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `area_atividade_img`
--

INSERT INTO `area_atividade_img` (`id`, `area_atividade_id`, `imagem`) VALUES
(17, 2, '359e32a69d0376f4e4c02c4397bcf723.jpg'),
(19, 3, 'd51adbfbe2b3d87b33ae53a3303dace8.jpg'),
(20, 3, 'fffe8fc7955e2541ecdfa43828200cac.jpg'),
(21, 1, 'cfd51021aa70ccffc8d25348fbbcf615.jpg'),
(22, 1, '4dd4b3ecc6d4dce2f7740373a13e10b6.jpg'),
(23, 4, '540e2ef57627ce63033ba692e5b87a54.jpg'),
(24, 5, '42d703ed38fe2319fae374001e72b178.jpg'),
(25, 6, 'b3ca8184cf264f0e198280b4af8e2e36.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(100) NOT NULL,
  `nome_en` varchar(100) DEFAULT NULL,
  `nome_fr` varchar(100) DEFAULT NULL,
  `nome_es` varchar(100) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria_main` int(11) DEFAULT NULL,
  `area_atividade_id` int(11) NOT NULL,
  `slug_pt` varchar(200) DEFAULT NULL,
  `slug_en` varchar(200) DEFAULT NULL,
  `slug_es` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `area_atividade_id` (`area_atividade_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id`, `nome_pt`, `nome_en`, `nome_fr`, `nome_es`, `ordem`, `categoria_main`, `area_atividade_id`, `slug_pt`, `slug_en`, `slug_es`) VALUES
(3, 'Tractores', '', '', '', 1, NULL, 3, NULL, NULL, NULL),
(4, 'Máquinas de Pequena Potência', '', NULL, '', 2, NULL, 3, NULL, NULL, NULL),
(5, 'Alfaias e Implementos', '', NULL, '', 3, NULL, 3, NULL, NULL, NULL),
(6, 'Outras Máquinas Agrícolas', '', NULL, '', 4, NULL, 3, NULL, NULL, NULL),
(7, 'CARRARO', '', NULL, '', 1, 3, 3, 'carraro', '', ''),
(8, 'FARMTRAC', '', NULL, '', 2, 3, 3, 'farmtrac', '', ''),
(9, 'TYM', 'TYM', NULL, 'TYM', 3, 3, 3, 'tym', 'tym', 'tym'),
(10, 'AGT', 'AGT', NULL, 'AGT', 4, 3, 3, 'agt', 'agt', 'agt'),
(11, 'Tractocarro AGRICO', '', NULL, '', 5, 3, 3, 'tractocarro-agrico', '', ''),
(12, 'Transportador', '', NULL, '', 1, 4, 3, 'transportador', '', ''),
(13, 'Motocultivadores', '', NULL, '', 2, 4, 3, NULL, NULL, NULL),
(14, 'Motoenxadas', '', NULL, '', 3, 4, 3, NULL, NULL, NULL),
(15, 'Motoceifeiras', '', NULL, '', 4, 4, 3, NULL, NULL, NULL),
(16, 'Corta-Relvas', '', NULL, '', 5, 4, 3, NULL, NULL, NULL),
(19, 'Retroescavadoras VENIERI', '', NULL, '', 1, 20, 1, 'retroescavadoras-venieri', '', ''),
(20, 'Retroescavadoras', '', NULL, '', 1, NULL, 1, 'retroescavadoras', '', ''),
(21, 'Retroescavadoras pequenas EUROCOMACH', '', NULL, '', 0, 20, 1, 'retroescavadoras-pequenas-eurocomach', '', ''),
(22, 'Varredouras urbanas', '', NULL, '', 0, NULL, 2, 'varredouras-urbanas', '', ''),
(23, 'Varredouras urbanas SICAS', '', NULL, '', 0, 22, 2, 'varredouras-urbanas-sicas', '', ''),
(24, 'Veículos Elétricos', '', NULL, '', 0, NULL, 2, 'veiculos-eletricos', '', ''),
(25, 'Modelos de Veículos Elétricos', '', NULL, '', 0, 24, 2, 'modelos-de-veiculos-eletricos', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(100) NOT NULL,
  `morada` text NOT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `telemovel` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `gps` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contactos`
--

INSERT INTO `contactos` (`id`, `empresa`, `morada`, `telefone`, `telemovel`, `fax`, `email`, `gps`) VALUES
(1, 'Empresa Teste', 'morada teste<br>', '', '', '', 'mail@norberto.eu', '');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `value_pt` varchar(500) NOT NULL,
  `value_en` varchar(500) DEFAULT NULL,
  `value_fr` varchar(500) DEFAULT NULL,
  `value_es` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `slug`, `value_pt`, `value_en`, `value_fr`, `value_es`) VALUES
(1, 'home', 'home', 'home', NULL, NULL),
(2, 'area_reservada', 'Área Reservada', 'Reserved Area', NULL, 'Reserved Area'),
(3, 'pesquisar', 'Pesquisar', 'Search', NULL, 'Búsqueda'),
(4, 'direitos', 'Todos os direitos reservados', 'All rights reserved', NULL, 'Reservados todos los derechos'),
(5, 'areas_atividade', 'máquinas e equipamentos', 'máquinas e equipamentos', NULL, 'máquinas e equipamentos'),
(6, 'bem_vindo', 'bem vindo à agricortes', 'welcome to agricortes', NULL, 'bienvenido to agricortes'),
(7, 'eventos', 'eventos', 'events', NULL, 'eventos'),
(8, 'texto_home_1', 'Fundada em 1969 a AGRICORTES é uma sólida estrutura importadora e distribuidora que, com a dedicação da sua equipa de profissionais, representa e assiste máquinas e equipamentos nos sectores da:', '', NULL, ''),
(9, 'texto_home_2', 'A sua consolidada presença no mercado e a confiança conquistada junto dos concessionários e clientes em geral, foi desde sempre desenvolvida na base de fortes princípios de gestão.', '', NULL, ''),
(10, 'catalogos', 'catálogos', '', NULL, ''),
(11, 'maquinas', 'Máquinas', '', NULL, ''),
(12, 'catalogos_no', 'Não existem catálogos neste separador.', '', NULL, ''),
(13, 'desejo_ser_contactado', 'DESEJO SER CONTACTADO!', '', NULL, ''),
(14, 'pesquisa', 'Pesquisa', '', NULL, ''),
(15, 'resultados_pesquisa', 'Resultados da Pesquisa:', '', NULL, ''),
(16, 'resultados_nao_encontrado', 'A pesquisa não devolveu resultados.', '', NULL, ''),
(17, 'pecas_servicos', 'Peças e Serviços', '', NULL, 'Piezas y Servicios'),
(18, 'catalogos', 'catálogos', NULL, NULL, 'catálogos');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `idCountry` int(5) NOT NULL AUTO_INCREMENT,
  `countryName` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`idCountry`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`idCountry`, `countryName`) VALUES
(1, 'Andorra'),
(2, 'United Arab Emirates'),
(3, 'Afghanistan'),
(4, 'Antigua and Barbuda'),
(5, 'Anguilla'),
(6, 'Albania'),
(7, 'Armenia'),
(8, 'Angola'),
(9, 'Antarctica'),
(10, 'Argentina'),
(11, 'American Samoa'),
(12, 'Austria'),
(13, 'Australia'),
(14, 'Aruba'),
(15, 'Åland'),
(16, 'Azerbaijan'),
(17, 'Bosnia and Herzegovina'),
(18, 'Barbados'),
(19, 'Bangladesh'),
(20, 'Belgium'),
(21, 'Burkina Faso'),
(22, 'Bulgaria'),
(23, 'Bahrain'),
(24, 'Burundi'),
(25, 'Benin'),
(26, 'Saint Barthélemy'),
(27, 'Bermuda'),
(28, 'Brunei'),
(29, 'Bolivia'),
(30, 'Bonaire'),
(31, 'Brazil'),
(32, 'Bahamas'),
(33, 'Bhutan'),
(34, 'Bouvet Island'),
(35, 'Botswana'),
(36, 'Belarus'),
(37, 'Belize'),
(38, 'Canada'),
(39, 'Cocos [Keeling] Islands'),
(40, 'Democratic Republic of the Congo'),
(41, 'Central African Republic'),
(42, 'Republic of the Congo'),
(43, 'Switzerland'),
(44, 'Ivory Coast'),
(45, 'Cook Islands'),
(46, 'Chile'),
(47, 'Cameroon'),
(48, 'China'),
(49, 'Colombia'),
(50, 'Costa Rica'),
(51, 'Cuba'),
(52, 'Cape Verde'),
(53, 'Curacao'),
(54, 'Christmas Island'),
(55, 'Cyprus'),
(56, 'Czechia'),
(57, 'Germany'),
(58, 'Djibouti'),
(59, 'Denmark'),
(60, 'Dominica'),
(61, 'Dominican Republic'),
(62, 'Algeria'),
(63, 'Ecuador'),
(64, 'Estonia'),
(65, 'Egypt'),
(66, 'Western Sahara'),
(67, 'Eritrea'),
(68, 'Spain'),
(69, 'Ethiopia'),
(70, 'Finland'),
(71, 'Fiji'),
(72, 'Falkland Islands'),
(73, 'Micronesia'),
(74, 'Faroe Islands'),
(75, 'France'),
(76, 'Gabon'),
(77, 'United Kingdom'),
(78, 'Grenada'),
(79, 'Georgia'),
(80, 'French Guiana'),
(81, 'Guernsey'),
(82, 'Ghana'),
(83, 'Gibraltar'),
(84, 'Greenland'),
(85, 'Gambia'),
(86, 'Guinea'),
(87, 'Guadeloupe'),
(88, 'Equatorial Guinea'),
(89, 'Greece'),
(90, 'South Georgia and the South Sandwich Islands'),
(91, 'Guatemala'),
(92, 'Guam'),
(93, 'Guinea-Bissau'),
(94, 'Guyana'),
(95, 'Hong Kong'),
(96, 'Heard Island and McDonald Islands'),
(97, 'Honduras'),
(98, 'Croatia'),
(99, 'Haiti'),
(100, 'Hungary'),
(101, 'Indonesia'),
(102, 'Ireland'),
(103, 'Israel'),
(104, 'Isle of Man'),
(105, 'India'),
(106, 'British Indian Ocean Territory'),
(107, 'Iraq'),
(108, 'Iran'),
(109, 'Iceland'),
(110, 'Italy'),
(111, 'Jersey'),
(112, 'Jamaica'),
(113, 'Jordan'),
(114, 'Japan'),
(115, 'Kenya'),
(116, 'Kyrgyzstan'),
(117, 'Cambodia'),
(118, 'Kiribati'),
(119, 'Comoros'),
(120, 'Saint Kitts and Nevis'),
(121, 'North Korea'),
(122, 'South Korea'),
(123, 'Kuwait'),
(124, 'Cayman Islands'),
(125, 'Kazakhstan'),
(126, 'Laos'),
(127, 'Lebanon'),
(128, 'Saint Lucia'),
(129, 'Liechtenstein'),
(130, 'Sri Lanka'),
(131, 'Liberia'),
(132, 'Lesotho'),
(133, 'Lithuania'),
(134, 'Luxembourg'),
(135, 'Latvia'),
(136, 'Libya'),
(137, 'Morocco'),
(138, 'Monaco'),
(139, 'Moldova'),
(140, 'Montenegro'),
(141, 'Saint Martin'),
(142, 'Madagascar'),
(143, 'Marshall Islands'),
(144, 'Macedonia'),
(145, 'Mali'),
(146, 'Myanmar [Burma]'),
(147, 'Mongolia'),
(148, 'Macao'),
(149, 'Northern Mariana Islands'),
(150, 'Martinique'),
(151, 'Mauritania'),
(152, 'Montserrat'),
(153, 'Malta'),
(154, 'Mauritius'),
(155, 'Maldives'),
(156, 'Malawi'),
(157, 'Mexico'),
(158, 'Malaysia'),
(159, 'Mozambique'),
(160, 'Namibia'),
(161, 'New Caledonia'),
(162, 'Niger'),
(163, 'Norfolk Island'),
(164, 'Nigeria'),
(165, 'Nicaragua'),
(166, 'Netherlands'),
(167, 'Norway'),
(168, 'Nepal'),
(169, 'Nauru'),
(170, 'Niue'),
(171, 'New Zealand'),
(172, 'Oman'),
(173, 'Panama'),
(174, 'Peru'),
(175, 'French Polynesia'),
(176, 'Papua New Guinea'),
(177, 'Philippines'),
(178, 'Pakistan'),
(179, 'Poland'),
(180, 'Saint Pierre and Miquelon'),
(181, 'Pitcairn Islands'),
(182, 'Puerto Rico'),
(183, 'Palestine'),
(184, 'Portugal'),
(185, 'Palau'),
(186, 'Paraguay'),
(187, 'Qatar'),
(188, 'Réunion'),
(189, 'Romania'),
(190, 'Serbia'),
(191, 'Russia'),
(192, 'Rwanda'),
(193, 'Saudi Arabia'),
(194, 'Solomon Islands'),
(195, 'Seychelles'),
(196, 'Sudan'),
(197, 'Sweden'),
(198, 'Singapore'),
(199, 'Saint Helena'),
(200, 'Slovenia'),
(201, 'Svalbard and Jan Mayen'),
(202, 'Slovakia'),
(203, 'Sierra Leone'),
(204, 'San Marino'),
(205, 'Senegal'),
(206, 'Somalia'),
(207, 'Suriname'),
(208, 'South Sudan'),
(209, 'São Tomé and Príncipe'),
(210, 'El Salvador'),
(211, 'Sint Maarten'),
(212, 'Syria'),
(213, 'Swaziland'),
(214, 'Turks and Caicos Islands'),
(215, 'Chad'),
(216, 'French Southern Territories'),
(217, 'Togo'),
(218, 'Thailand'),
(219, 'Tajikistan'),
(220, 'Tokelau'),
(221, 'East Timor'),
(222, 'Turkmenistan'),
(223, 'Tunisia'),
(224, 'Tonga'),
(225, 'Turkey'),
(226, 'Trinidad and Tobago'),
(227, 'Tuvalu'),
(228, 'Taiwan'),
(229, 'Tanzania'),
(230, 'Ukraine'),
(231, 'Uganda'),
(232, 'U.S. Minor Outlying Islands'),
(233, 'United States'),
(234, 'Uruguay'),
(235, 'Uzbekistan'),
(236, 'Vatican City'),
(237, 'Saint Vincent and the Grenadines'),
(238, 'Venezuela'),
(239, 'British Virgin Islands'),
(240, 'U.S. Virgin Islands'),
(241, 'Vietnam'),
(242, 'Vanuatu'),
(243, 'Wallis and Futuna'),
(244, 'Samoa'),
(245, 'Kosovo'),
(246, 'Yemen'),
(247, 'Mayotte'),
(248, 'South Africa'),
(249, 'Zambia'),
(250, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `deadlock_users`
--

CREATE TABLE IF NOT EXISTS `deadlock_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL DEFAULT '',
  `lastname` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `phone` varchar(15) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(30) NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=322 ;

--
-- Dumping data for table `deadlock_users`
--

INSERT INTO `deadlock_users` (`id`, `firstname`, `lastname`, `email`, `phone`, `country`, `username`, `password`) VALUES
(4, 'Luis Frazao', 'Virtual-net', 'lfrazao@virtual-net.pt', '', 'Not Selected', 'lfrazao', 'brasas07'),
(6, 'Ana Sofia', 'Agricortes', 'agricortes@agricortes.com', '244819112', 'Portugal', 'agricortes', 'milho2ag'),
(8, 'Ana Gordalina', 'Agricortes', 'import@agricortes.com', '244819112', 'Portugal', 'import', '3eduric'),
(11, 'Susana Vieira', 'Agricortes', 'marketing@agricortes.com', '', 'Portugal', 'user4', 'susiuser4'),
(27, 'Antonio', 'Agrorecta Lda', 'GERAL@AGRORECTA.COM', '262990697', 'Portugal', 'ANTONIO', 'ANT6179'),
(15, 'Lda', 'Agropeixoto', 'agropeixoto.lda@hotmail.com', '253921960', 'Portugal', 'agropeixoto', 'filipa1'),
(19, 'Joao Gaspar', 'Agrogaspares', 'geral@agrogaspares.pt', '243605054', 'Poland', 'agrogaspares', 'jgaspar1'),
(186, 'Hudarauto', 'Hudarauto', 'pecashudarauto@sapo.pt', '289862529', 'Portugal', 'Hudarauto', 'sporting7'),
(18, 'Jaume', 'Ribalta', 'comercial@ribaltaros.com', '0034973310805', 'Spain', 'RIBALTAROS', 'rib310'),
(68, 'Bernardo', 'Perez', 'bp_jp_agric@hotmail.com', '', 'Spain', 'bperez', 'bperez08'),
(321, 'Raul', 'Dias', 'luar541@hotmail.com', '919708805', 'Portugal', 'radigon', 'luar1954'),
(158, 'Magriber', 'Magriber', 'magribersl@terra.es', '', 'Not Selected', 'magriber', 'magriber01'),
(159, 'Mario ', 'Santos', 'agromundial@gmail.com', '', 'Portugal', 'agromundial', 'a917010436'),
(319, 'Rui', 'Gabirra', 'gabirra1976@gmail.com', '', 'Portugal', 'RuiGabirra', 'RG238423'),
(154, 'Leonor', 'Moniz', 'PECAS@AGRICORTES.COM', '', 'Not Selected', 'LMONIZ3', 'migas123'),
(45, 'Maqsogran', 'Maqsogran', 'maqsogran@agentes.winterthur.es', '620139692', 'Spain', 'maqsogran2005', 'maqsogran2'),
(167, 'Azurmaquinas', 'Azurmaquinas', 'azurmaquinas@sapo.pt', '232617265', 'Portugal', 'azurmaquinas', 'AZM2002'),
(163, '141619', 'Teixeira Pinto', 'ltpinto@sapo.pt', '259469106', 'Portugal', 'carricalp', 'ltp141619'),
(56, 'Torre', 'Marco', 'geral@torremarco.com', '252661840', 'Portugal', 'torremarco', 'tm123456'),
(57, 'Torre', 'Marco', 'liliana.ramos@torremarco.com', '252661840', 'Portugal', 'vilaconde', 'vc654321'),
(66, 'Adao', 'Moreira', 'moreirastractores@gmail.com', '255781317', 'Portugal', 'adao73moreira', 'manuel73'),
(172, 'Dina', 'Avelar', 'sapagric@sapo.pt', '', 'Not Selected', 'sapagric', 'cdi007ij'),
(171, 'Ofiagri', 'Luis Marques', 'ofiagri@hotmail.com', '', 'Not Selected', 'Ofiagri', 'oficina1'),
(86, 'Auto Agricola ', 'Redondense', 'auto_agricola_redondense@hotmail.com', '266909971', 'Portugal', 'autoagricola08', 'auto2008'),
(126, 'Ibarra Totana', 'Ibarra Totana', 'administracion@ibarratotana.com', '968422044', 'Spain', 'IBARRA', 'B30442719'),
(99, 'Semetra', 'Lda', 'semetra@mail.telepac.pt', '249566279', 'Portugal', 'semetra', 'smt2008'),
(320, 'Agostinho', 'Serranito', 'agostinho_p_29@hotmail.com', '933682738', 'Portugal', 'Agostinho', 'Minority4'),
(131, 'Marta ', 'Macario', 'marta.macario@agriloja.pt', '', 'Not Selected', 'agriloja', 'agriloja1'),
(187, 'Rafael', 'Viguera', 'rafaviguera@agrocon.es', '', 'Not Selected', 'agrocon', '26logrono'),
(119, 'Julio-caeiro ', 'Jeronimo-caeiro', 'caeiro@sapo.pt', '266701772', 'Portugal', 'jersaeiro', 'caeiro9999'),
(129, 'Pedro', 'Machado', 'oficinatemilobauto@sapo.pt', '969077294', 'Portugal', 'temilobauto', '1temiloba1'),
(185, 'Licio ', 'Cardoso', 'licio.cardoso@smcsucrs.com', '912847177', 'Portugal', 'severino1941', 'sev1941'),
(130, 'Alfredo', 'Pinto', 'geral@alfredopintoefilhos.pt', '254821583', 'Portugal', 'alfredo', 'alfredo18'),
(120, 'Magrimal', 'Gomes Santos', 'magrimallda@gmail.com', '256811469', 'Portugal', 'magrimal', '753267sr'),
(312, 'Sainhasefilho', 'Sainhas', 'sainhas.filho@sapo.pt', '275924508', 'Portugal', 'sainhas', 'sainhas96'),
(112, 'Hermanos Villar', 'Hermanos Villar', 'HERMANOSVILLAR@terra.es', '986585574', 'Spain', 'VILLAR', 'VILLAR123'),
(135, 'Adelino ', 'Lopes', 'adelino.lopes.mecal@sapo.pt', '964449408', 'Portugal', 'mecal', 'mecal00'),
(183, 'Cunha', 'Companhia', 'cunhacompanhia@gmail.com', '', 'Portugal', 'cunhacomp', 'cunha1'),
(182, 'Jose Carlos', 'Tavares Gouveia', 'geral@cartotrans.pt', '', 'Not Selected', 'jctgouveia', 'jctg2010'),
(147, 'A Simoes', 'A Simoes', 'a.simoes.filhos@sapo.pt', '', 'Not Selected', 'asimoes', 'asf3360'),
(165, 'Xerocar Sa', 'Xerocar Sa', 'geral@xerocar.pt', '254583375', 'Not Selected', 'xerocar', 'FIPE1960'),
(315, 'Agrituines ', 'Agritunes', 'agritunesgeral@gmail.com', '964430112', 'Not Selected', 'AGRITUNES2014', 'TUNES2014'),
(146, 'Agrodemolda', 'Agrodemo', 'agrodemo@sapo.pt', '254582809', 'Portugal', 'agrodemo', 'agrodemo12'),
(194, 'Cti', 'Cti', 'luis.pinto@ctitractoresemaquinas.pt', '932028008', 'Portugal', 'CTILDA', 'CTILUIS1'),
(201, 'Candidomiranda', 'Candidomiranda', 'c.miranda@candidomiranda.es', '', 'Spain', 'candido', '770441es'),
(196, 'Luis', 'Ribeiro', 'lofilassiste@sapo.pt', '232451482', 'Not Selected', 'Lofilassiste', 'LJLR196603'),
(193, 'Vecotorres', 'Vecotorres', 'vecotorres.tvd@mail.telepac.pt', '+351261325494', 'Portugal', 'VECOTORRES', 'VECOTORR1'),
(200, 'Francisco', 'Lima', 'f.lima79@hotmail.com', '966862087', 'Portugal', 'flima', 'lima1979'),
(311, 'Silvagriauto', 'Rui Silva', 'silvagriauto@gmail.com', '', 'Not Selected', 'silvagriauto', 'cm158484'),
(219, 'Nuno', 'Rodrigues', 'maquinaseurico@gmail.com', '964690684', 'Portugal', 'MAEURICO', 'MAE1981'),
(218, 'Firmino1', 'Santos1', 'firmino.lda@sapo.pt', '232871156', 'Portugal', 'firmino1', 'mdimdi1'),
(216, 'Lousagricola ', 'Nunes', 'lousagricolalda@sapo.pt', '', 'Not Selected', 'jose1807', '179lousa'),
(217, 'Estrada', 'Companhia', 'pauloestrada@sapo.pt', '241333525', 'Portugal', 'estrada', 'p500101230'),
(215, 'Maquinaria', 'Andres', 'magricola-andres@wanadoo.es', '', 'Not Selected', 'andres', 'toñi2222'),
(317, 'Zarko', 'Kocevski', 'zarko.kocevski@ipardpa.gov.mk', '', 'Macedonia', 'kocevski101', 'Aa123456'),
(244, 'Hidraulica', 'Motor Hermanos ', 'info@hidraulicamotor.com', '', 'Spain', 'hidraulicamotor', 'hermanos7'),
(236, 'Autovilamou', 'Autovilamou', 'autovilamou@hotmail.com', '258732690', 'Portugal', 'autovilamo', 'joao2001'),
(246, 'Pectrac', 'Borges', 'pectrac@sapo.pt', '', 'Not Selected', 'pectrac', 'pectrac12'),
(268, 'Roberto ', 'Picado Romero', 'AGRAFER@HOTMAIL.COM', '0034981771907', 'Spain', 'agrafer', 'betanzos1'),
(318, 'Victor', 'Santos', 'vmartinssantos@sapo.pt', '916638131', 'Portugal', 'victor', 'victor01'),
(247, 'Vitor', 'Lapa', 'amagrinabao.vitor@gmail.com', '249321850', 'Not Selected', 'amagrinabao', 'goncalo2'),
(234, 'Jose Duarte', 'Esteves', 'joseduartefesteves@sapo.pt', '', 'Not Selected', 'fernanda', '1994maio'),
(242, 'Joaquim', 'Cardoso', 'agricardoso@hotmail.com', '917524077', 'Portugal', 'jc5032', 'jc5032'),
(243, 'Talleres', 'Gandara', 'gandaraantelo@gmail.com', '', 'Spain', 'tgandara', '15vimianzo'),
(257, 'Gil', 'Goncalves', 'automec.geral@gmail.com', '236980500', 'Portugal', 'AUTOMEC', 'AUTOMEC09'),
(252, 'Comerciala', 'Martinperez', 'info@cagricolamartinperez.com', '', 'Not Selected', 'comercial', 'martin18'),
(265, 'Juan', 'Planells', 'comercialplanells@hotmail.com', '', 'Not Selected', 'ibiza', 'planells26'),
(274, 'Filipe', 'Caetano', 'geral@tractorcaldas.pt', '262839140', 'Portugal', 'tractorcaldas', 'tractor123'),
(272, 'Jorge', 'Cruz', 'Tamimaquinas@hotmail.com', '933198625', 'Portugal', 'jcruz', 'viajante1'),
(288, 'Antonio ', 'Prata', 'a.f.n.prata@sapo.pt', '263789220', 'Portugal', 'prata', 'prata1234'),
(281, 'Tobias', 'Reis', 'tobias_reis@sapo.pt', '963937815', 'Portugal', 'tobiasreis', 'reis96'),
(303, 'Francisco', 'Conde', 'frankonde@agrichico.com', '', 'Not Selected', 'agrimaquinaria', 'chico13'),
(301, 'Pedro', 'Pragosa', 'pedropragosatractores@hotmail.com', '966106486', 'Not Selected', 'pragosa2642', 'ag2642'),
(300, 'Hudarauto', 'Zacarias', 'geralhudarauto@sapo.pt', '+351917005458', 'Portugal', 'hudarauto1', 'otuaraduh1'),
(305, 'Agrorainha', 'Agrorainha', 'isabelagrorainha@sapo.pt', '262842727', 'Portugal', 'agrorainha34', 'carraro34'),
(307, 'Batalhagri', 'Batalhagri', 'batalhagri@hotmail.com', '913949267', 'Portugal', 'BATALHAGRI', 'batalha12'),
(297, 'Magrimal', 'Mansores', 'MAGRIMALMANSORES@GMAIL.COM', '256921424', 'Portugal', 'MAGRIMALMANSORE', 'jsantoss1');

-- --------------------------------------------------------

--
-- Table structure for table `ficheiro`
--

CREATE TABLE IF NOT EXISTS `ficheiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(100) NOT NULL,
  `titulo_pt` varchar(200) DEFAULT NULL,
  `titulo_en` varchar(200) DEFAULT NULL,
  `titulo_fr` varchar(200) DEFAULT NULL,
  `titulo_es` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_portfolio`
--

CREATE TABLE IF NOT EXISTS `item_portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `titulo_pt` varchar(100) NOT NULL,
  `titulo_en` varchar(100) DEFAULT NULL,
  `titulo_fr` varchar(100) DEFAULT NULL,
  `titulo_es` varchar(100) DEFAULT NULL,
  `subtitulo_pt` varchar(100) DEFAULT NULL,
  `subtitulo_en` varchar(100) DEFAULT NULL,
  `subtitulo_fr` varchar(100) DEFAULT NULL,
  `subtitulo_es` varchar(100) DEFAULT NULL,
  `descricao_pt` text,
  `descricao_en` text,
  `descricao_fr` text,
  `descricao_es` text,
  `imagem_capa` varchar(100) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `link_video` varchar(200) DEFAULT NULL,
  `destaque` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categoria_id` (`categoria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `item_portfolio`
--

INSERT INTO `item_portfolio` (`id`, `categoria_id`, `titulo_pt`, `titulo_en`, `titulo_fr`, `titulo_es`, `subtitulo_pt`, `subtitulo_en`, `subtitulo_fr`, `subtitulo_es`, `descricao_pt`, `descricao_en`, `descricao_fr`, `descricao_es`, `imagem_capa`, `ordem`, `link_video`, `destaque`) VALUES
(5, 10, 'TRACTORES COMPACTOS 4RM DE CILINDRADAS 1649 A 2199 CC', '', NULL, '', NULL, NULL, NULL, NULL, '<p>Tractor compacto com capacidade para adaptar-se a uma grande variedade de terrenos, desde a vinha, hort&iacute;colas e pomares, at&eacute; ao uso em terrenos montanhosos e com declives, at&eacute; aos menos exigentes como parques, jardins, campos de golfe, parques de campismo, etc.</p>\r\n\r\n<p>Especifica&ccedil;&otilde;es&nbsp;principais:<br />\r\n&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Motores Lombardini, de 1649 cc e 2199 cc</li>\r\n	<li>Chassi articulado com oscila&ccedil;&atilde;o central ou r&iacute;gida com rodas direccionais</li>\r\n	<li>Direc&ccedil;&atilde;o revers&iacute;vel ou n&atilde;o revers&iacute;vel</li>\r\n	<li>Tra&ccedil;&atilde;o permanente &agrave;s quatro rodas motrizes</li>\r\n	<li>Direc&ccedil;&atilde;o hidrost&aacute;tica</li>\r\n	<li>Bloqueio do diferencial anterior</li>\r\n	<li>Raio de viragem reduzido</li>\r\n	<li>Tr&ecirc;s pontos posteriores (opc. engate TDF anterior)</li>\r\n	<li>Transmiss&atilde;o 8x4 ou 12x12&nbsp;sincronizada.</li>\r\n</ul>\r\n', '', NULL, '', '', 0, '', 0),
(6, 19, 'Retroescavadoras VENIERI', '', NULL, '', NULL, NULL, NULL, NULL, '<table border="0" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="2">\r\n			<p>Retroescavadora de transmiss&atilde;o completamente hidrost&aacute;tica, constitu&iacute;da por bombas de caudal vari&aacute;vel e motores hidr&aacute;ulicos, em circuito fechado. Melhor rendimento total, varia&ccedil;&atilde;o cont&iacute;nua da velocidade, sistema de auto travagem, possibilidade de velocidade baix&iacute;ssimas para uso em certos implementos, sistema <em>Load Sensing</em> (permite o uso da direc&ccedil;&atilde;o hidr&aacute;ulica sempre com prioridade em rela&ccedil;&atilde;o ao sistema hidr&aacute;ulico), 4 rodas motrizes, iguais e direccionais, trav&otilde;es em banho de &oacute;leo, cabina ROPS / FOPS, requisitos totais de seguran&ccedil;a, visibilidade e conforto, componentes de reconhecida elevada qualidade.</p>\r\n\r\n			<p>A retroescavadora articulada <strong>Venieri</strong> &eacute; uma m&aacute;quina que se apresenta como uma aut&ecirc;ntica p&aacute; carregadora com uma retro acoplada. Estabilidade garantida pela melhor distribui&ccedil;&atilde;o da massa e pelas 4 rodas motrizes, iguais e direccionais. Para mobilidade em espa&ccedil;o reduzido e trabalhos de maior versatilidade.</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Modelos </strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Hp</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Motor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Peso operativo</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>P&aacute;</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Profund. escava&ccedil;&atilde;o</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>1.33 B</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>54</p>\r\n			</td>\r\n			<td>\r\n			<p>Perkins</p>\r\n			</td>\r\n			<td>\r\n			<p>3300 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,40 m3</p>\r\n			</td>\r\n			<td>\r\n			<p>2,75 - 3,25 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>6.23 B</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>86</p>\r\n			</td>\r\n			<td>\r\n			<p>Perkins</p>\r\n			</td>\r\n			<td>\r\n			<p>7400 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>1,1 - 1,3&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>4,30 - 5,10 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>8.23 D</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>105</p>\r\n			</td>\r\n			<td>\r\n			<p>Perkins</p>\r\n			</td>\r\n			<td>\r\n			<p>7600 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>1,1 - 1,4&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>4,50 - 5,40 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>10.33 B</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>105</p>\r\n			</td>\r\n			<td>\r\n			<p>Perkins</p>\r\n			</td>\r\n			<td>\r\n			<p>8800 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>1,2 - 1,5&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>4,80&nbsp;- 5,80 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>10.23 C</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>117</p>\r\n			</td>\r\n			<td>\r\n			<p>Perkins</p>\r\n			</td>\r\n			<td>\r\n			<p>9000 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>1,3 - 1,6&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>5,00&nbsp;- 6,00 m</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', NULL, '', 'i5c9e11dba0f98a25d1002a4fdf04ca12.jpg', 0, '', 0),
(7, 21, 'Retroescavadoras pequenas EUROCOMACH', '', NULL, '', NULL, NULL, NULL, NULL, '<table border="0" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="2">\r\n			<p>Design inovativo, pot&ecirc;ncia, servi&ccedil;o, e a m&aacute;xima manobrabilidade em condi&ccedil;&otilde;es extremas. A retroescavadora articulada <strong>Eurocomach</strong> &eacute; uma m&aacute;quina que se apresenta como uma aut&ecirc;ntica p&aacute; carregadora com uma retro acoplada. Para mobilidade em espa&ccedil;o reduzido e trabalhos de maior versatilidade.</p>\r\n\r\n			<p>Retroescavadoras articuladas, de transmiss&atilde;o completamente hidrost&aacute;tica constitu&iacute;da por bomba auto regul&aacute;vel de caudal vari&aacute;vel e por motores hidr&aacute;ulicos. Trabalha em circuito fechado. Melhor rendimento total, varia&ccedil;&atilde;o cont&iacute;nua da velocidade, sistema de auto travagem, possibilidade de velocidade baix&iacute;ssimas - para uso em certos implementos opcionais, sistema <em>Load Sensing</em>, 4 rodas motrizes iguais, canopy ou cabina ROPS / FOPS, requisitos totais de seguran&ccedil;a, visibilidade e conforto, componentes de reconhecida elevada qualidade.</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Modelos </strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Hp</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Motor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Peso operativo</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>P&aacute;</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Profund. escava&ccedil;&atilde;o</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>E215K</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>25</p>\r\n			</td>\r\n			<td>\r\n			<p>Kubota</p>\r\n			</td>\r\n			<td>\r\n			<p>1560 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,17 m3</p>\r\n			</td>\r\n			<td>\r\n			<p>2,20 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>E225K</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>25</p>\r\n			</td>\r\n			<td>\r\n			<p>Kubota</p>\r\n			</td>\r\n			<td>\r\n			<p>1900 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,20&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>2,35 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>E235K</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>45</p>\r\n			</td>\r\n			<td>\r\n			<p>Kubota</p>\r\n			</td>\r\n			<td>\r\n			<p>2900 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,35&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>2,85 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>E245K</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>50</p>\r\n			</td>\r\n			<td>\r\n			<p>Kubota</p>\r\n			</td>\r\n			<td>\r\n			<p>3800 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,45&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>3,00 m</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>E265</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>65</p>\r\n			</td>\r\n			<td>\r\n			<p>Cummins</p>\r\n			</td>\r\n			<td>\r\n			<p>5900 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>0,75&nbsp;m3</p>\r\n			</td>\r\n			<td>\r\n			<p>3,30 m</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', NULL, '', 'i41faa0e7cd9c6962ec747c6cf7c0460b.jpg', 2, '9F0djtAKNOQ;Q79MVjjQ', 1),
(8, 23, 'Varredouras urbanas SICAS', '', NULL, '', NULL, NULL, NULL, NULL, '<table border="0" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan="2">As varredoras urbanas e as lavadoras de estradas <strong>SICAS</strong> s&atilde;o compactas e autoportantes, de alto rendimento e elevada versatilidade de servi&ccedil;o.<br />\r\n			<br />\r\n			As varredoras equipam um sistema de recolha de lixo completo e preciso, com &oacute;ptima manobrabilidade e not&aacute;vel capacidade de trabalho. A caixa de recolha em inox cont&eacute;m perfeitamente o lixo sem qualquer risco de perda ou fuga. S&atilde;o dotadas de um sistema de abatimento de poeiras que funciona exclusivamente a &aacute;gua, com dep&oacute;sito de &aacute;gua em inox. Gra&ccedil;as ao sistema de reciclagem da &aacute;gua presente na caixa de recolha do lixo, a humidifica&ccedil;&atilde;o do canal de aspira&ccedil;&atilde;o aumenta consideralvelmente a autonomia do trabalho. O chassis &eacute; em a&ccedil;o e constru&iacute;do de forma a poderem ser acoplados diferentes acess&oacute;rios. A cabina, tamb&eacute;m em a&ccedil;o, &eacute; dotada de uma grande superf&iacute;cie vidrada que oferece total visibilidade em todo o redor da m&aacute;quina. A transmiss&atilde;o &eacute; hidrost&aacute;tica Automotive e &eacute; controlada electronicamente.<br />\r\n			<br />\r\n			As lavadoras s&atilde;o de constru&ccedil;&atilde;o id&ecirc;ntica, de conceito compacto, dotadas de r&eacute;gua frontal de lavagem de estradas a alta press&atilde;o com dep&oacute;sitos de &aacute;gua em inox.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\n<strong>Varredoras de estradas e pavimentos:</strong></p>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Modelo</strong></td>\r\n			<td><strong>SA2.5</strong></td>\r\n			<td><strong>Millenium</strong></td>\r\n			<td><strong>4000/4</strong></td>\r\n			<td><strong>6000</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Motor</strong></td>\r\n			<td>VM</td>\r\n			<td>DEUTZ ou MAN</td>\r\n			<td>IVECO</td>\r\n			<td>IVECO</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Presta&ccedil;&atilde;o de Servi&ccedil;o</strong></td>\r\n			<td>Varredura e aspira&ccedil;&atilde;o</td>\r\n			<td>Varredura e aspira&ccedil;&atilde;o</td>\r\n			<td>Varredura mec&acirc;nica</td>\r\n			<td>Varredura e aspira&ccedil;&atilde;o<br />\r\n			Chassi sob cami&atilde;o</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Classe</strong></td>\r\n			<td>2 m3</td>\r\n			<td>4 m3</td>\r\n			<td>4 m3</td>\r\n			<td>6 m3</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tanque de &aacute;gua</strong></td>\r\n			<td>320 + 150&nbsp;ltrs.</td>\r\n			<td>700 ltrs.</td>\r\n			<td>1400 ltrs.</td>\r\n			<td>2000 ltrs.</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Faixa de limpeza</strong></td>\r\n			<td>2000 - 2700 mm</td>\r\n			<td>2200 - 2800 mm</td>\r\n			<td>3150 mm</td>\r\n			<td>2200 - 2800 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Capacidade de aspira&ccedil;&atilde;o</strong></td>\r\n			<td>10.000 m3/h</td>\r\n			<td>18.000 m3/h</td>\r\n			<td>n.a.</td>\r\n			<td>15.500 m3/h</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\n<strong>Lavadoras de estradas e pavimentos: </strong></p>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Modelo</strong></td>\r\n			<td><strong>SL2.V</strong></td>\r\n			<td><strong>NL5M3</strong></td>\r\n			<td><strong><strong>NA4M3VPLUS</strong></strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Motor</strong></td>\r\n			<td>VM</td>\r\n			<td>DEUTZ</td>\r\n			<td>DEUTZ</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Presta&ccedil;&atilde;o de Servi&ccedil;o</strong></td>\r\n			<td>Lavagem de estradas e pavimentos a alta press&atilde;o</td>\r\n			<td>Lavagem de estradas e pavimentos a alta press&atilde;o</td>\r\n			<td>Lavagem de estradas e pavimentos a alta press&atilde;o.<br />\r\n			Varredura e aspira&ccedil;&atilde;o</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Classe</strong></td>\r\n			<td>2 m3</td>\r\n			<td>4 m3</td>\r\n			<td>4&nbsp;m3</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tanque de &aacute;gua</strong></td>\r\n			<td>1800&nbsp;ltrs.</td>\r\n			<td>5350 ltrs.</td>\r\n			<td>800 ltrs.</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Faixa de limpeza</strong></td>\r\n			<td>2000 - 2500 mm</td>\r\n			<td>1896 - 3094 mm</td>\r\n			<td>1896 - 3094 mm</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', NULL, '', 'i7d4fcea343a52aa79176d2ee205396ce.jpg', 0, 'zE0rauCYaH4', 0),
(9, 25, 'Veículos Elétricos ALKÈ', '', NULL, '', NULL, NULL, NULL, NULL, '<p>ALK&Egrave; &eacute; comercializada pela&nbsp;<strong><strong>INTERCORTES</strong> </strong>uma empresa associada da <strong>AGRICORTES</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>ALK&Egrave; &eacute; um ve&iacute;culo el&eacute;trico profissional, extremamente compacto, constru&iacute;do com um posicionamento elevado nos planos construtivo e de qualidade.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>O enchimento autom&aacute;tico da &aacute;gua das baterias do sistema de desloca&ccedil;&atilde;o, com tanque incorporado no ve&iacute;culo, diminui quase a &ldquo;zero&rdquo; a manuten&ccedil;&atilde;o. O enchimento &eacute; quinzenal atrav&eacute;s de torneira colocada na lateral do ve&iacute;culo.</p>\r\n\r\n<p>As baterias de altas presta&ccedil;&otilde;es da Trojan, l&iacute;der mundial neste setor, t&ecirc;m o tempo de carregamento de 8 horas.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Este ve&iacute;culo el&eacute;trico &eacute; respeitador de todas as normas de seguran&ccedil;a CE (certifica&ccedil;&atilde;o CE).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Opcionais:</p>\r\n\r\n<ul>\r\n	<li>Velocidade 40 km/h</li>\r\n	<li>Cabina</li>\r\n	<li>Aquecimento</li>\r\n	<li>Caixa de carga com basculamento eletro-hidr&aacute;ulico</li>\r\n	<li>Taipais em altura e lonas</li>\r\n	<li>Cor personalizada</li>\r\n	<li>Pneus de cidade, de jardinagem, todo-terreno</li>\r\n	<li>Sistema de eleva&ccedil;&atilde;o de contentores</li>\r\n	<li>Motoriza&ccedil;&otilde;es diesel com tra&ccedil;&atilde;o 4x4</li>\r\n	<li>Outros por consulta</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Modelo</strong></td>\r\n			<td>\r\n			<p><strong>ATX 100 E</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>ATX 200 E</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>ATX 200 E AR</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>ATX 280 E</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tra&ccedil;&atilde;o</strong></td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Autonomia</strong></td>\r\n			<td>\r\n			<p>70 Km</p>\r\n			</td>\r\n			<td>\r\n			<p>70 Km</p>\r\n			</td>\r\n			<td>\r\n			<p>70 Km</p>\r\n			</td>\r\n			<td>\r\n			<p>90 Km</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Transmiss&atilde;o</strong></td>\r\n			<td>\r\n			<p>Eletr&oacute;nica</p>\r\n			</td>\r\n			<td>\r\n			<p>Eletr&oacute;nica</p>\r\n			</td>\r\n			<td>\r\n			<p>Eletr&oacute;nica</p>\r\n			</td>\r\n			<td>\r\n			<p>Eletr&oacute;nica</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Velocidade</strong></td>\r\n			<td>\r\n			<p>30 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>30 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>30 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>25 km/h</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Trav&otilde;es</strong></td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Capacidade de reboque</strong></td>\r\n			<td>\r\n			<p>2000 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>2000 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>2000 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>3000 Kg</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Capacidade da caixa de carga</strong></td>\r\n			<td>\r\n			<p>490 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>530 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>530 Kg</p>\r\n			</td>\r\n			<td>\r\n			<p>1000 Kg</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Dimens&otilde;es da caixa de carga</strong></td>\r\n			<td>\r\n			<p>130 x 120 x 26 cm</p>\r\n			</td>\r\n			<td>\r\n			<p>180 x 120 x 26 cm</p>\r\n			</td>\r\n			<td>\r\n			<p>180 x 120 x 26 cm</p>\r\n			</td>\r\n			<td>\r\n			<p>180 x 120 x 26 cm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Observa&ccedil;&otilde;es</strong></td>\r\n			<td>\r\n			<p>Modelo estreito</p>\r\n			</td>\r\n			<td>\r\n			<p>Modelo largo</p>\r\n			</td>\r\n			<td>\r\n			<p>Caixa tipo dumper</p>\r\n			</td>\r\n			<td>\r\n			<p>Mais potentes e resistentes</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', NULL, '', 'ic0aa846f232fff84f4a9de07a8bcdf3c.jpg', 0, 'Cd7mMsgCoYE', 0),
(10, 25, 'Veículos Elétricos EAGLE', '', NULL, '', NULL, NULL, NULL, NULL, '<p>EAGLE &eacute; comercializada pela&nbsp;<strong><strong>INTERCORTES</strong> </strong>uma empresa associada da <strong>AGRICORTES</strong>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Os ve&iacute;culos el&eacute;ctricos EAGLE da s&eacute;rie 6043 apresentam uma autonomia at&eacute; 80 km, sendo o carregamento do ve&iacute;culo efectuada atrav&eacute;s de uma tomada local de 220~230V 50 Hz, com um consumo de 10,8 kW por carregamento completo (cerca de 60 c&ecirc;ntimos num tarif&aacute;rio de baixa tens&atilde;o acima dos 20,7 kVA nas horas de vazio).<br />\r\n<br />\r\nPodem ser de 2 ou 4 passageiros, com homologa&ccedil;&atilde;o europeia e matr&iacute;cula para circular em estrada, podem circular em via p&uacute;blica com matr&iacute;cula portuguesa e tem certifica&ccedil;&atilde;o CE.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Opcionais:</p>\r\n\r\n<ul>\r\n	<li>Portas</li>\r\n	<li>Cor personalizada</li>\r\n	<li>Personaliza&ccedil;&atilde;o por consulta:</li>\r\n</ul>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;-&nbsp;Ve&iacute;culos para o sector Seguran&ccedil;a</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Ve&iacute;culos para Recolha de res&iacute;duos/Industriais</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Ve&iacute;culos de Catering</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Ve&iacute;culos Utilit&aacute;rios</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Carros de Golf / Desportivos</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Comboios el&eacute;ctricos</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;Autocarros el&eacute;ctricos</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="3" cellspacing="3" style="width:95%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Modelo</strong></td>\r\n			<td>\r\n			<p><strong><strong>EG6043KR-00</strong></strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong><strong>EG6043KR-01</strong></strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Categoria</strong></td>\r\n			<td>\r\n			<p>L7e</p>\r\n			</td>\r\n			<td>\r\n			<p>L7e</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Passageiros</strong></td>\r\n			<td>\r\n			<p>4</p>\r\n			</td>\r\n			<td>\r\n			<p>2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Caixa de carga</strong></td>\r\n			<td>\r\n			<p>---</p>\r\n			</td>\r\n			<td>\r\n			<p>375 kg</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Trac&ccedil;&atilde;o</strong></td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n			<td>\r\n			<p>Posterior</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Autonomia</strong></td>\r\n			<td>\r\n			<p>70 km</p>\r\n			</td>\r\n			<td>\r\n			<p>70 km</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Baterias</strong></td>\r\n			<td>\r\n			<p>Trojan</p>\r\n			</td>\r\n			<td>\r\n			<p>Trojan</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Controlador</strong></td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Transmiss&atilde;o</strong></td>\r\n			<td>\r\n			<p>1 : 1</p>\r\n			</td>\r\n			<td>\r\n			<p>1 :1</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Velocidade</strong></td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Controlador</strong></td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Transmiss&atilde;o</strong></td>\r\n			<td>\r\n			<p>1 : 1</p>\r\n			</td>\r\n			<td>\r\n			<p>1 :1</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Velocidade</strong></td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Controlador</strong></td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n			<td>\r\n			<p>Curtis</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Transmiss&atilde;o</strong></td>\r\n			<td>\r\n			<p>1 : 1</p>\r\n			</td>\r\n			<td>\r\n			<p>1 :1</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Velocidade</strong></td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n			<td>\r\n			<p>40 km/h</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Trav&otilde;es</strong></td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n			<td>\r\n			<p>Hidr&aacute;ulicos</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Suspens&atilde;o</strong></td>\r\n			<td>\r\n			<p>Independente</p>\r\n			</td>\r\n			<td>\r\n			<p>Independente</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Pneus</strong></td>\r\n			<td>\r\n			<p>12&rsquo;&rsquo; com jantes liga leve</p>\r\n			</td>\r\n			<td>\r\n			<p>12&rsquo;&rsquo; com jantes liga leve</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', NULL, '', 'i74935651411db340b7bf5d9ae5eb6e2d.jpg', 0, '', 0),
(11, 7, 'teste1', '', NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, '', '', 0, '', 1),
(12, 7, 'teste 2', '', NULL, '', NULL, NULL, NULL, NULL, '', '', NULL, '', '', 0, '', 0),
(14, 21, 'teste', NULL, NULL, '', NULL, NULL, NULL, NULL, '<p>sdbsdfghsdhsgh</p>\r\n', NULL, NULL, '', 'ia39b6af231ce2eedd9c400a3304446df.jpg', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_portfolio_file`
--

CREATE TABLE IF NOT EXISTS `item_portfolio_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item_portfolio` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `nome_pt` varchar(100) DEFAULT NULL,
  `nome_es` varchar(100) DEFAULT NULL,
  `nome_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_item_portfolio` (`id_item_portfolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `item_portfolio_file`
--

INSERT INTO `item_portfolio_file` (`id`, `id_item_portfolio`, `file`, `nome_pt`, `nome_es`, `nome_en`) VALUES
(10, 7, 'pdf_teste.pdf', NULL, NULL, NULL),
(13, 6, 'pdf_teste.pdf', NULL, NULL, NULL),
(14, 7, 'pdf_teste.pdf', NULL, NULL, NULL),
(15, 11, 'pdf_teste.pdf', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_portfolio_img`
--

CREATE TABLE IF NOT EXISTS `item_portfolio_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item_portfolio` int(11) NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `nome_pt` varchar(100) DEFAULT NULL,
  `nome_es` varchar(100) DEFAULT NULL,
  `nome_en` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_item_portfolio` (`id_item_portfolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `item_portfolio_img`
--

INSERT INTO `item_portfolio_img` (`id`, `id_item_portfolio`, `imagem`, `nome_pt`, `nome_es`, `nome_en`) VALUES
(7, 5, 'c012ce37747c26a5b00e82e6a3f94d7d.jpg', 'Trator vermelho', '', ''),
(8, 5, 'c9d3d12908cd17678b531f021b79dfd5.jpg', NULL, NULL, NULL),
(20, 6, 'c24ffa780e949c7baa0e7116bed5fe05.jpg', 'sfagdgdgdg', 'sdfgdsg', NULL),
(21, 6, '61083aaffc1eec699a7cc12b6fcd8f73.jpg', 'sdgdsg', 'sdgdsg', NULL),
(22, 6, '77ea1a0d67c78bfa8cca9151125c7253.jpg', NULL, NULL, NULL),
(23, 7, 'bdcb2998f1c295ca4de7b571d5538edb.jpg', 'fdhfdhdfgh', '', NULL),
(24, 7, '9c1f77bdaf5120cdffa11b09679cb441.JPG', NULL, NULL, NULL),
(25, 7, '5853563b78183db7ac6fecd1a1a36069.JPG', NULL, NULL, NULL),
(26, 7, '11c732c7d96aa923df4b593b60c2713e.JPG', NULL, NULL, NULL),
(27, 8, '4905449f626d7e7dc24be655c7235647.jpg', NULL, NULL, NULL),
(28, 8, 'b2cf5e1178f87b0afe42b1195e8eb1f8.JPG', NULL, NULL, NULL),
(29, 9, 'ac40664b21b1e06b453b32fd8daef237.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_pt` varchar(50) NOT NULL,
  `titulo_en` varchar(50) DEFAULT NULL,
  `titulo_es` varchar(50) DEFAULT NULL,
  `titulo_fr` varchar(50) DEFAULT NULL,
  `posicao` varchar(50) NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  `pagina_id` int(11) DEFAULT NULL,
  `menu_main` int(11) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `visivel` int(11) NOT NULL,
  `target` varchar(50) NOT NULL,
  `imagem` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `titulo_pt`, `titulo_en`, `titulo_es`, `titulo_fr`, `posicao`, `link`, `pagina_id`, `menu_main`, `ordem`, `visivel`, `target`, `imagem`) VALUES
(1, 'empresa', NULL, 'empresa', NULL, 'top', '', 3, NULL, 1, 1, '_self', '0db2ab3ecde8a80618a7432f99d78fe4.JPG'),
(2, 'Máquinas e equipamentos', NULL, 'Máquinas e equipamentos', NULL, 'top', '', NULL, NULL, 2, 1, '_self', '37694c8587fa7f9e34bf98bb3ee76e57.JPG'),
(3, 'Peças e serviços', NULL, 'Peças e serviços', NULL, 'top', 'site/pecas', NULL, NULL, 3, 1, '_self', '97bd92d0787074ef96e0b8678317790f.jpg'),
(4, 'Eventos', NULL, 'Eventos', NULL, 'top', 'evento', NULL, NULL, 4, 1, '_self', '58197df11cc5bba64483a443b75f80bc.jpg'),
(5, 'Downloads', NULL, 'Downloads', NULL, 'top', 'downloads', NULL, NULL, 5, 1, '_self', '53fa9785fa7ce0858c1f2f87865d0b21.jpg'),
(6, 'contactos', NULL, 'contactos', NULL, 'top', '', 2, NULL, 6, 1, '_self', '9d30dbf08ae981187d87d66455059649.jpg'),
(7, 'história', NULL, 'história', NULL, 'top', '', 3, 1, 1, 1, '_self', '1c47f91120c777c8a8e8a729181f4c59.JPG'),
(8, 'qualidade e ambiente', NULL, 'qualidade e ambiente', NULL, 'top', '', 4, 1, 2, 1, '_self', 'fd0897562cd1dbfc7ad1349d35d07ff0.JPG'),
(9, 'agrocortes club', NULL, 'agrocortes club', NULL, 'top', '', 5, 1, 3, 1, '_self', '7edb7e46711204f2f33d3e817eb3a046.JPG'),
(10, 'empresas do grupo', NULL, 'empresas do grupo', NULL, 'top', '', 6, 1, 4, 1, '_self', '176269bd90cb468e3cc11c0590944818.JPG'),
(11, 'home', NULL, 'home', NULL, 'top', 'index.php', NULL, NULL, 0, 1, '_self', '94f193f9f8a123eb3afb7d9ad95e0077.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `noticia`
--

CREATE TABLE IF NOT EXISTS `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_pt` varchar(100) NOT NULL,
  `titulo_en` varchar(100) DEFAULT NULL,
  `titulo_fr` varchar(100) DEFAULT NULL,
  `titulo_es` varchar(100) DEFAULT NULL,
  `conteudo_pt` text NOT NULL,
  `conteudo_en` text,
  `conteudo_fr` text,
  `conteudo_es` text,
  `data` date NOT NULL,
  `data_fim` date NOT NULL,
  `imagem` varchar(200) NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `noticia`
--

INSERT INTO `noticia` (`id`, `titulo_pt`, `titulo_en`, `titulo_fr`, `titulo_es`, `conteudo_pt`, `conteudo_en`, `conteudo_fr`, `conteudo_es`, `data`, `data_fim`, `imagem`, `link`) VALUES
(1, 'Santarém', NULL, NULL, '', '<p>51&ordf; Feira Nacional de Agricultura<br />\r\n61&ordf; Feira do Ribatejo<br />\r\nSantar&eacute;m - CNEMA</p>\r\n\r\n<p>Uma presen&ccedil;a fiel. Uma parceria &uacute;nica no mercado. Uma gama completa de m&aacute;quinas e equipamentos. Uma visita imprescind&iacute;vel como sempre.</p>\r\n', NULL, NULL, '', '2014-06-07', '2014-06-15', 'c31fac0f03f7c8ffa352cee86fdcbe7e.gif', 'http://www.cnema.pt/noticias_detail.php?aID=3417'),
(2, 'AGRO BRAGA', NULL, NULL, 'AGRO BRAGA', '<p>47&ordm; Feira Internacional de Agricultura, Pecu&aacute;ria e Alimenta&ccedil;&atilde;o.</p>\r\n', NULL, NULL, '<p>47&ordm; Feira Internacional de Agricultura, Pecu&aacute;ria e Alimenta&ccedil;&atilde;o.</p>\r\n', '2014-03-27', '2014-03-30', 'e7e56b5a12709e008743fa3e3d0cba2d.jpg', ''),
(3, 'FIMA', NULL, NULL, 'FIMA', '<p>38&ordf; Feira Internacional de M&aacute;quinas Agr&iacute;colas<br />\r\nZaragoza, Espanha</p>\r\n\r\n<p>Convidamos os nossos clientes, amigos e p&uacute;blico em geral a visitar o stand da AGRICORTES na 38&ordf; edi&ccedil;&atilde;o da FIMA.<br />\r\nA AGRICORTES estar&aacute; como &eacute; habitual no Pavilh&atilde;o 6 com uma excelente amostra da gama de tractores CARRARO e TYM.<br />\r\nVisite-nos!&nbsp;</p>\r\n', NULL, NULL, '<p>38&ordf; Feira Internacional de M&aacute;quinas Agr&iacute;colas<br />\r\nZaragoza, Espanha</p>\r\n\r\n<p>Convidamos os nossos clientes, amigos e p&uacute;blico em geral a visitar o stand da AGRICORTES na 38&ordf; edi&ccedil;&atilde;o da FIMA.<br />\r\nA AGRICORTES estar&aacute; como &eacute; habitual no Pavilh&atilde;o 6 com uma excelente amostra da gama de tractores CARRARO e TYM.<br />\r\nVisite-nos!&nbsp;</p>\r\n', '2014-02-11', '2014-02-15', '3d5a2f5e64cb4bfdc0a61f7f513385b3.gif', 'http://www.feriazaragoza.es/fima_agricola.aspx'),
(4, 'Santarém', NULL, NULL, 'Santarém', '<p>50&ordf; Feira Nacional de Agricultura<br />\r\n60&ordf; Feira do Ribatejo<br />\r\nSantar&eacute;m - CNEMA</p>\r\n\r\n<p>Uma presen&ccedil;a fiel. Uma parceria &uacute;nica no mercado. Uma gama completa de m&aacute;quinas e equipamentos. Uma visita imprescind&iacute;vel.</p>\r\n', NULL, NULL, '<p>50&ordf; Feira Nacional de Agricultura<br />\r\n60&ordf; Feira do Ribatejo<br />\r\nSantar&eacute;m - CNEMA</p>\r\n\r\n<p>Uma presen&ccedil;a fiel. Uma parceria &uacute;nica no mercado. Uma gama completa de m&aacute;quinas e equipamentos. Uma visita imprescind&iacute;vel.</p>\r\n', '2013-06-08', '2013-06-16', '95ab84a3f80d966d128c1a69eddb16e6.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `pagina`
--

CREATE TABLE IF NOT EXISTS `pagina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_pt` varchar(100) NOT NULL,
  `titulo_en` varchar(100) DEFAULT NULL,
  `titulo_fr` varchar(100) DEFAULT NULL,
  `titulo_es` varchar(100) DEFAULT NULL,
  `conteudo_pt` text NOT NULL,
  `conteudo_en` text,
  `conteudo_fr` text,
  `conteudo_es` text,
  `imagem` varchar(100) DEFAULT NULL,
  `slug_pt` varchar(100) NOT NULL,
  `slug_es` varchar(100) NOT NULL,
  `slug_en` varchar(100) NOT NULL,
  `visivel` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pagina`
--

INSERT INTO `pagina` (`id`, `titulo_pt`, `titulo_en`, `titulo_fr`, `titulo_es`, `conteudo_pt`, `conteudo_en`, `conteudo_fr`, `conteudo_es`, `imagem`, `slug_pt`, `slug_es`, `slug_en`, `visivel`) VALUES
(2, 'contactos', NULL, NULL, 'contactos', '<table border="0" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="vertical-align:top">\r\n			<p><strong>PORTUGAL</strong></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>Sede</strong></p>\r\n\r\n			<p>AGRICORTES &ndash; Com&eacute;rcio de M&aacute;quinas e Equipamentos, SA</p>\r\n\r\n			<p>Avenida do Lis &ndash; Cortes, 2410-501 LEIRIA &ndash; PORTUGAL</p>\r\n\r\n			<p><strong>GPS</strong> N 39&ordm; 41&rsquo; 58,54&rsquo;&rsquo; W 8&ordm; 47&rsquo; 20,29&rsquo;&rsquo;</p>\r\n\r\n			<p><strong>T</strong> 244 819 110</p>\r\n\r\n			<p><strong>F</strong> 244 819 111</p>\r\n\r\n			<p><a href="mailto:agricortes@agricortes.com">agricortes@agricortes.com</a></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>Sucursais</strong></p>\r\n\r\n			<p>Pombal</p>\r\n\r\n			<p>Batalha</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>Filial Lisboa</strong></p>\r\n			INTERCORTES &ndash; Estudos, Projectos de Engenharia e Representa&ccedil;&otilde;es, Lda</td>\r\n			<td style="vertical-align:top">\r\n			<p><strong>ESPANHA</strong></p>\r\n\r\n			<p>AGRICORTES ESPA&Ntilde;A, SL</p>\r\n\r\n			<p>28924 Alcorc&oacute;n (MADRID) &ndash; ESPA&Ntilde;A</p>\r\n\r\n			<p><strong>T</strong> 916 210 620</p>\r\n\r\n			<p><strong>F</strong> 916 210 619</p>\r\n			<p><a href="mailto:ventas@agricortes.com">ventas@agricortes.com</a></p></td>\r\n			<td style="vertical-align:top">\r\n			<p><strong>ANGOLA</strong></p>\r\n\r\n			<p>AGRICORTES DE LUANDA &ndash; Desenvolvimento Agr&iacute;cola, Industrial e Urbano, Lda</p>\r\n\r\n			<p>Viana &ndash; LUANDA &ndash; ANGOLA</p>\r\n\r\n			<p><strong>M</strong> 933 203 108</p>\r\n			<p><a href="mailto:sales@agricortes.com">sales@agricortes.com</a></p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, NULL, '', '', 'contactos', 'contactos', '', 1),
(3, 'história', NULL, NULL, '', '<p><strong>1969</strong> - Constiui&ccedil;&atilde;o da AGRICORTES - Sociedade Comercial Agr&iacute;cola, Lda., com sede em Cortes, Leiria, com actividade na comercializa&ccedil;&atilde;o de m&aacute;quina e equipamentos para a agricultura e movimenta&ccedil;&atilde;o de terras.</p>\r\n\r\n<p><strong>1975</strong> - In&iacute;cio da importa&ccedil;&atilde;o e distribui&ccedil;&atilde;o directa de m&aacute;quinas agr&iacute;colas italianas.</p>\r\n\r\n<p><strong>1982</strong> - Distribui&ccedil;&atilde;o exclusiva dos tractores CARRARO para o mercado portugu&ecirc;s.</p>\r\n\r\n<p><strong>1987</strong> - Inaugura&ccedil;&atilde;o do Parque de Exposi&ccedil;&atilde;o permanente em Amieira - Batalha - Leiria. In&iacute;cio da actividade comercial grossista de Equipamentos de Movimenta&ccedil;&atilde;o de Cargas.</p>\r\n\r\n<p><strong>1988</strong> - In&iacute;cio da actividade comercial grossista de Equipamentos Florestais.</p>\r\n\r\n<p><strong>1990</strong> - In&iacute;cio da actividade comercial grossista de Equipamentos Ambientais.</p>\r\n\r\n<p><strong>1995</strong> - Amplia&ccedil;&atilde;o das infra-estruturas da sede com a cria&ccedil;&atilde;o de um novo armaz&eacute;m e sala de forma&ccedil;&atilde;o.</p>\r\n\r\n<p><strong>1997</strong> - Distribui&ccedil;&atilde;o exclusiva dos tractores TYM para o mercado portugu&ecirc;s.</p>\r\n\r\n<p><strong>1999</strong> - In&iacute;cio da actividade comercial grossista de M&aacute;quinas de Movimenta&ccedil;&atilde;o de Terras, Obras P&uacute;blicas e Constru&ccedil;&atilde;o Civil. Distribui&ccedil;&atilde;o exclusiva das m&aacute;quinas industriais VENIERI para o mercado portugu&ecirc;s. Distribui&ccedil;&atilde;o exclusiva dos tractores TYM para o mercado ib&eacute;rico. Constitui&ccedil;&atilde;o da AGRICORTES ESPA&Ntilde;A, S.L.</p>\r\n\r\n<p><strong>2000</strong> - Altera&ccedil;&atilde;o de pacto social e aumento de capital social para 3.000.000.</p>\r\n\r\n<p><strong>2002</strong> - Transforma&ccedil;&atilde;o em sociedade an&oacute;nima, com denomina&ccedil;&atilde;o social: AGRICORTES - Com&eacute;rcio de M&aacute;quinas e Equipamentos, SA.</p>\r\n\r\n<p><strong>2003</strong> - Distribui&ccedil;&atilde;o exclusiva dos tractores CARRARO para o mercado ib&eacute;rico. A AGRICORTES obt&eacute;m a certifica&ccedil;&atilde;o do seu Sistema de Gest&atilde;o da Qualidade segundo a norma NP EN ISO 9001.</p>\r\n\r\n<p><strong>2004</strong> - Distribui&ccedil;&atilde;o exclusiva das varredoras urbanas SICAS para o mercado portugu&ecirc;s.</p>\r\n\r\n<p><strong>2005</strong> - Distribui&ccedil;&atilde;o exclusiva dos tractores FARMTRAC e AGT para o mercado ib&eacute;rico.</p>\r\n\r\n<p><strong>2007</strong> - A AGRICORTES ESPA&Ntilde;A muda os seus escrit&oacute;rios comerciais para Madrid.</p>\r\n\r\n<p><strong>2008</strong> - Distribui&ccedil;&atilde;o exclusiva dos biotrituradores para biomassa JENZ e m&aacute;quinas compactas para a constru&ccedil;&atilde;o EUROCOMACH.</p>\r\n\r\n<p><strong>2009</strong> - A AGRICORTES atrav&eacute;s da sua participada INTERCORTES cria um escrit&oacute;rio de representa&ccedil;&atilde;o no Parque das Na&ccedil;&otilde;es em Lisboa. Distribui&ccedil;&atilde;o exclusiva dos ve&iacute;culos el&eacute;ctricos ALK&Egrave;.</p>\r\n\r\n<p><strong>2010</strong> - Constitui&ccedil;&atilde;o da AGRICORTES DE LUANDA - Desenvolvimento Agr&iacute;cola, Industrial e Urbano, Lda com sede em Luanda - Angola. &Eacute; implementado um Sistema de Gest&atilde;o do Ambiente segundo a norma NP EN ISO 14001 na sede da AGRICORTES (Portugal).</p>\r\n', NULL, NULL, '', '', 'historia', '', '', 1),
(4, 'qualidade e ambiente', NULL, NULL, 'qualidade e ambiente', '<p>qualidade e ambiente</p>\r\n', NULL, NULL, '', '', 'qualidade-e-ambiente', 'qualidade-e-ambiente', '', 1),
(5, 'agricortes club', NULL, NULL, 'agricortes club', '<ul>\r\n	<li>Objecto do Clube: promover os interesses dos associados e unir os associados &agrave; volta de um sentimento de perten&ccedil;a.</li>\r\n	<li>Associados do clube: clientes finais, utilizadores das m&aacute;quinas e equipamentos, da AGRICORTES .</li>\r\n	<li>Direitos dos Associados:\r\n	<ul>\r\n		<li>privil&eacute;gio no atendimento comercial e t&eacute;cnico;</li>\r\n		<li>atendimento imediato, aquando de uma reclama&ccedil;&atilde;o relativa a produtos e servi&ccedil;os da AGRICORTES</li>\r\n		<li>benef&iacute;cio a sorteios, promo&ccedil;&otilde;es e ofertas;</li>\r\n		<li>participa&ccedil;&atilde;o nos inqu&eacute;ritos de avalia&ccedil;&atilde;o de satisfa&ccedil;&atilde;o com opini&otilde;es;</li>\r\n		<li>receberem correspond&ecirc;ncia ass&iacute;dua, quer comercial quer do Clube;</li>\r\n	</ul>\r\n	</li>\r\n	<li>Deveres do Associados: promoverem a continuidade, divulga&ccedil;&atilde;o e bom-nome do Clube.</li>\r\n</ul>\r\n', NULL, NULL, '', '73ee976c2f47f5bce3a11ce3cad567fb.jpg', 'agricortes-club', 'agricortes-club', '', 1),
(6, 'empresas do grupo', NULL, NULL, 'empresas do grupo', '<p><strong>AGRICORTES ESPA&Ntilde;A, SL</strong></p>\r\n\r\n<p>Madrid</p>\r\n\r\n<p>Atividade: Tratores</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>AGRICORTES DE LUANDA, Lda</strong></p>\r\n\r\n<p>Luanda</p>\r\n\r\n<p>Atividade: M&aacute;quinas e Equipamentos</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>INTERCORTES &ndash; Estudos, Projectos de Engenharia e Representa&ccedil;&otilde;es, Lda</strong></p>\r\n\r\n<p>Actividade: Ve&iacute;culos utilit&aacute;rios el&eacute;tricos</p>\r\n\r\n<p>Parque das Na&ccedil;&otilde;es, 1990-019 LISBOA</p>\r\n\r\n<p>M 966 781 529</p>\r\n\r\n<p><a href="mailto:intercortes@agricortes.com">intercortes@agricortes.com</a></p>\r\n\r\n<p><a href="http://www.alke.pt/">www.alke.pt</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>SERVINATURA, Lda</strong></p>\r\n\r\n<p>Actividade: Servi&ccedil;os de Sa&uacute;de&nbsp;</p>\r\n\r\n<p><a href="http://www.pacosaude.pt">www.pacosaude.pt</a></p>\r\n', NULL, NULL, '', '', 'empresas-do-grupo', 'empresas-do-grupo', '', 1),
(7, 'popup', NULL, NULL, 'popup', '<p>AGRICORTES</p>\r\n', NULL, NULL, '', '48f3025f762b4d1965ee716f30ac1da6.JPG', 'popup', 'popup', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `telemovel` varchar(50) NOT NULL,
  `cod_postal` varchar(50) NOT NULL,
  `mensagem` text NOT NULL,
  `assunto` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`id`, `nome`, `empresa`, `telemovel`, `cod_postal`, `mensagem`, `assunto`, `email`, `data`) VALUES
(2, 'Norberto', 'teste', '876987', '865986', 'kjhglkhjlk', 'RETROESCAVADORAS PEQUENAS EUROCOMACH', 'mail@norberto.eu', '2014-05-22 11:16:37'),
(3, 'rgdsfgsdg', 'sdgsdg', '3425632', 'sagasg', 'adgfdg', 'TESTE 2', 'mail@norberto.eu', '2014-05-22 11:18:42');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(100) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `link` varchar(200) DEFAULT NULL,
  `titulo_pt` varchar(200) DEFAULT NULL,
  `titulo_en` varchar(200) DEFAULT NULL,
  `titulo_fr` varchar(200) DEFAULT NULL,
  `titulo_es` varchar(200) DEFAULT NULL,
  `subtitulo_pt` varchar(200) DEFAULT NULL,
  `subtitulo_en` varchar(200) DEFAULT NULL,
  `subtitulo_fr` varchar(200) DEFAULT NULL,
  `subtitulo_es` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `imagem`, `ordem`, `link`, `titulo_pt`, `titulo_en`, `titulo_fr`, `titulo_es`, `subtitulo_pt`, `subtitulo_en`, `subtitulo_fr`, `subtitulo_es`) VALUES
(5, 'fd1fdd380b9f770c1d3e0e0afa1280a1.jpg', 1, '', '', '', NULL, '', NULL, NULL, NULL, NULL),
(7, '5b3983ebdd6fcef952de2bea30ea8f0c.jpg', 2, '', '', NULL, NULL, '', NULL, NULL, NULL, NULL),
(8, '04ecb5843b421ed3a24edacb48a4de97.jpg', 3, '', '', NULL, NULL, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `roles` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `email`, `password`, `nome`, `last_login_time`, `create_time`, `create_user_id`, `update_time`, `update_user_id`, `roles`) VALUES
(1, 'norberto', 'mail@norberto.eu', '12345', 'Admin', '2014-06-02 15:45:40', '2013-02-04 23:14:52', 1, '2014-03-24 11:42:00', 1, 'admin'),
(2, 'teste', 'mail@norberto.eu2', '12345', 'Norberto', '2014-03-31 15:57:12', '2013-04-18 16:19:46', 1, '2013-11-30 16:28:09', 1, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `website`
--

CREATE TABLE IF NOT EXISTS `website` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_pt` varchar(200) NOT NULL,
  `titulo_en` varchar(200) DEFAULT NULL,
  `titulo_fr` varchar(200) DEFAULT NULL,
  `titulo_es` varchar(200) DEFAULT NULL,
  `description_pt` varchar(500) NOT NULL,
  `description_en` varchar(500) DEFAULT NULL,
  `description_fr` varchar(500) DEFAULT NULL,
  `description_es` varchar(500) DEFAULT NULL,
  `keywords_pt` varchar(500) NOT NULL,
  `keywords_en` varchar(500) DEFAULT NULL,
  `keywords_fr` varchar(500) DEFAULT NULL,
  `keywords_es` varchar(500) DEFAULT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `website`
--

INSERT INTO `website` (`id`, `titulo_pt`, `titulo_en`, `titulo_fr`, `titulo_es`, `description_pt`, `description_en`, `description_fr`, `description_es`, `keywords_pt`, `keywords_en`, `keywords_fr`, `keywords_es`, `url`) VALUES
(1, 'Agricortes', 'Agricortes', 'Agricortes', 'Agricortes', 'Cultivamos qualidade, com ela produzimos cultura', '', '', '', 'agricortes', '', '', '', 'http://localhost/virtualnet/agricortes');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
