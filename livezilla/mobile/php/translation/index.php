<?php
/****************************************************************************************
 * LiveZilla index.php
 *
 * Copyright 2013 LiveZilla GmbH
 * All rights reserved.
 * LiveZilla is a registered trademark.
 *
 ***************************************************************************************/

$langFileLocation = '.';
$LZLANG = Array();

if (isset($_GET['g_language'])) {
    $language = ($_GET['g_language'] != '') ? htmlentities($_GET['g_language']) : 'en';
    require ($langFileLocation . '/langmobileorig.php');
    $LZLANGEN = $LZLANG;
    requireDynamic ($langFileLocation . '/langmobile' . $language . '.php', $langFileLocation);
    

    $stringArray = Array();
    foreach ($LZLANGEN as $key => $value) {
        if (preg_match('/^mobile_/', $key) != 0) {
            if (array_key_exists($key, $LZLANG) && $LZLANG[$key] != null && $LZLANG[$key] != '') {
                array_push($stringArray, array('orig' => $value, $language => $LZLANG[$key]));
            } else {
                array_push($stringArray, array('orig' => $value, $language => $value));
            }
        }

    }

    exit(base64_encode(json_encode($stringArray)));

} else {
    die(json_encode('No get or post parameter given'));
}

function requireDynamic($_file,$_trustedFolder)
{
    global $CONFIG, $_CONFIG, $LZLANG, $LZLANGEN; // ++
    if(strpos($_file, "..") !== false)
        return;
    if(strpos(realpath($_file),realpath($_trustedFolder)) !== 0)
        return;
    if(file_exists($_file))
        require($_file);
}
?>
