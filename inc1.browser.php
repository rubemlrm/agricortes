<?php

/**
 * List files in a folder recursively with options.
 *
 * -- description about this --
 *
 *
 *
 * LICENSE: Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met: Redistributions of source code must retain the
 * above copyright notice, this list of conditions and the following
 * disclaimer. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * @category
 * @package     File_Browser
 * @author      Gabriel Dunne <gldunne[at]gmail[dot]com>
 * @copyright   2006 Gabriel Dunne
 * @version     CVS: $Id: file_browser.php,v #.## 2009/03/00 05:54:17 gdunne Exp $
 * @license     http://www.opensource.org/licenses/bsd-license.php
 * @link        http://quilime.com/library/php/file-browser/
 */


class File_Browser
{
    function File_Browser($settings = 0)
    {
        $this->settings = $settings;
    }    
}




$settings = array();

$settings['title']          = 'browse';
$settings['folders_on_top'] = true;
$settings['hide_file_ext']  = false;
$settings['hide_this_file'] = true;

_RUN();

?>
<html>

<head>

    <style type="text/css">

        body { font-family:Arial; margin:4em; font-size: 1em; }
        h1 { font-size:1em; margin-bottom:2em; }
        
        a       { color:#99aa00; text-decoration:none; }
        a:hover { background:#ddffdd; color:#663304; }

        li, ol { padding:0; margin:0; color:#bbb; }
        li { }        
            li.file   a { color:#553300; }
            li.folder a { color:#128800; }
            li.notice   { list-style-type:none; color:#944; }
            
        #parent { position:absolute; left:2em; display:block; width:2em; text-align:center; }
        
    </style>
    
    <title><?php echo $settings['title']; ?> <?php echo $p == "" ? "" : " : " . $p; ?></title>

</head>

<body>

    <?php if ($parent_dir) : ?>
    <a href="?p=<?php echo $parent_dir;?>" id="parent">&uarr;</a>
    <?php endif; ?>

    <h1>
        <a href="<?php echo $_SERVER['SCRIPT_NAME'];?>"><?php echo $settings['title']; ?></a>
        : 
        <?php echo breadcrumbs($p); ?>
    </h1>
    
    <ol>
        <?php if($fileList) : ?>
        
            <?php foreach($fileList as $f) : ?>
                
                <?  
                if($f['type'] == "Folder") {
                    $class = 'folder';
                    $href  = "?p=" . $p . $f['name'];
                    $name  = $f['name'];
                }
                else {
                    $class = 'file';
                    $href  = pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME) . '/' . $p . $f['name']; 
                    $name  = $settings['hide_file_ext'] ? removeExtension($f['name']) : $f['name'];
                }
                ?>
                
                <li class="<?php echo $class; ?>">
                    <a href="<?php echo $href; ?>"><?php echo $name; ?></a>
                </li>
                
            <?php endforeach; ?>
            
        <?php else : ?>
        
            <li class="notice">
                error: <strong>not found</strong>
            </li>
            
        <?php endif; ?>
    </ol>

</body>

</html>

<?php

/*********************************************
 FUNCTIONS
 *********************************************/

/**
 *  init
 */
function _RUN()
{
    global $p, $parent_dir, $fileList, $settings;
    
    // get current path from URL
    $p = isset($_GET['p']) ? $_GET['p'] : false;
    
    // clean path to make sure it's not hacked
    if($p && (strstr($p, '../') || strstr($p, '//') || $p == "./")) $p = "";
    
    // get parent directory of current path
    $parent_dir = $p ? pathinfo($p, PATHINFO_DIRNAME) . '/' : false;
    
    // create file array from path, if it's a legit folder
    $fileList = array();
    if(is_dir("./" . $p)) {
        $fileList  = fileArray("./" . $p);
        if($settings['folders_on_top']) {
            $folders = array();
            $files   = array();
            foreach($fileList as $f) {
                if($settings['hide_this_file'] && 
                   pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME) . '/' . $p . $f['name'] ==
                   pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME) . '/' . pathinfo(__FILE__, PATHINFO_BASENAME))
                    continue;
                if($f['type'] == 'Folder')  
                    $folders[] = $f;
                else
                    $files[] = $f;
            }
            $fileList = array_merge($folders, $files);
        }
    }
    else {
        $p == false;
    }
    
    return false;
}

/**
 *  file/folder array
 *
 *  @param String $p                the path
 *  @param String[] $extensions     extensions of files to show
 *  @param String[] $excludes       files to exclude
 *  @param Bool     $show_hidden    show hidden files
 *  @param Array[]                  the array of files
 *
 */
function fileArray($p, $extensions = Array(), $excludes = Array('.', '..'), $show_hidden = false) 
{
    $result = Array();
    $parsedResult = Array();
  
    if ($handle = opendir($p)) {
        while (false !== ($file = readdir($handle))) {
            if (!in_array($file, $excludes)) {
            
        	if(!$show_hidden)
	            if( $file{0} == ".") continue;
	
                // is a folder
                if(is_dir($p.$file)) {
                    $folderInfo  = Array (
                        'name'      => $file .'/',
                        'mtime'     => $mtime = filemtime($p . $file),
                        'type'      => 'Folder',
                        'size'      => null,
                        );  
            
                    if(in_array('Folder', $excludes)) continue;
                    if(in_array('Folder', $extensions)) {
                        $parsedResult[] = $folderInfo;
                        continue;
                    }
                    $result[] = $folderInfo;
                }
                // is a file
                else {
                    $fileInfo  = Array(
                        'name'      => $file,
                        'mtime'     => $mtime = filemtime($p . $file),
                        'type'      => $type  = pathinfo($file, PATHINFO_EXTENSION),
                        'size'      => filesize($p.$file),
                        );
                    if(in_array($type, $extensions)) {
                        $parsedResult[] = $fileInfo;
                        continue;
                    }
                    $result[] = $fileInfo;
                }
            }
        }
    }

//  if(sizeof($extensions) > 0)
//    return sortArray($parsedResult, 'A', 'N');
//  else
    return sort_array_of_arrays($result, 'name', 'name'); //sortArray($result, 'A', 'N');
}


/**
 *  sort array of arrays
 *  requires array of associative arrays -- not checked within array
 *
 *  @author : http://phosphorusandlime.blogspot.com/2005/12/php-sort-array-by-one-field-in-array.html
 *
 *  @param Array[] $ARRAY           (two dimensional array of arrays)
 *  @param String $sortby_index     index/column to be sorted on
 *  @param String $key_index        equivalent to primary key column in SQL 
 */
function sort_array_of_arrays($ARRAY, $sortby_index, $key_index) 
{
    $ORDERING = array();
    $SORTED = array();
    $_DATA = array();
    $_key = '';
    $_sort_col_val = '';
    $_i = 0;
    
    // get ordering array
    foreach ( $ARRAY as $_DATA ) {
        $_key = $_DATA[$key_index];
        $ORDERING[$_key] = $_DATA[$sortby_index];
    }
    
    // sort ordering array
    asort($ORDERING);
    
    // map ARRAY back to ordering array
    foreach ( $ORDERING as $_key => $_sort_col_val ) {
        // get index for ARRAY where ARRAY[][$key_index] == $_key
        foreach ($ARRAY as $_i => $_DATA) {
            if ( $_key == $_DATA[$key_index] ) {
                $SORTED[] = $ARRAY[$_i];
                continue;
            }
        }
    }    
    return $SORTED;
}


/**
 *  Format Size
 *  @param int $size    in bytes
 *  @param int $round   round value
 *
 */
function format_size($size, $round = 0) 
{
    $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    for ($i = 0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024;
    return round($size, $round) . $sizes[$i];
} 


/**
 *  remove file extension from string
 */
function removeExtension($strName) 
{
    $ext = strrchr($strName, '.');
    if($ext !== false)
        $strName = substr($strName, 0, -strlen($ext));
    return $strName;
} 

/**
 *  breadcrumbs
 *  @param String $path         path
 *  @param String $sep          separator
 *  @param String $path_var     path variable for the url
 *  @return String              
 */
function breadcrumbs($path, $sep = " / ", $path_var = "p")
{
    $pathParts = explode("/", $path);
    $pathCT = 0; 
    $br = "";
    foreach($pathParts as $pt) {
		$br .= '<a href="?' . $path_var . '=';
		for($i=0; $i <= $pathCT; $i++) {
			$br .= $pathParts[$i].'/'; 
		}
		$br .= '">'.$pt.'</a>'; 
		if($pathCT < sizeof($pathParts)-2)
		$br .= $sep; 
		$pathCT++;
    }
    return $br;
}

?>


